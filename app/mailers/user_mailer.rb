class UserMailer < ApplicationMailer
	default from: 'Lorryz <no-reply@lorryz.com>'

	def send_customer_info(customer)
    @customer = customer
    mail(to: "contact@lorryz.com", subject: "Lorryz - Customer Info")
  end

  def send_admin_credentials_to_super_admin superadmin_email,admin_id , password
    @admin = User.find_by_id(admin_id)
    @password = password
    mail(to: superadmin_email , subject: "Lorryz - New Admin Credentials") if superadmin_email.present?
  end

	def send_new_user_update(customer,customer_country)
    @customer = customer
    @customer_country=customer_country
    mail(to: "contact@lorryz.com", subject: "Lorryz - Customer Info")
  end

  def new_rating reciever

  end
end
