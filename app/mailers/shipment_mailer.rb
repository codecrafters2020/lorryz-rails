class ShipmentMailer < ApplicationMailer

	default from: 'Lorryz <no-reply@lorryz.com>'

	def send_automated_email(shipment)
		
		@shipment = Shipment.find_by_id(shipment)
		@company = Company.find_by_id(@shipment.company)
		@duration_shipment = 0;
if ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Offloading Point").present? && ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").present?
	start_time = {"date"=>  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).strftime("%H:%M") }
	start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
	tz = ISO3166::Country.new(@shipment.country.short_name).timezones.zone_identifiers.first
	start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
	@start_timestamp = start_time_csv.utc
	start_timestamp = start_time_csv.utc

	complete_time = {"date"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).strftime("%H:%M") }
	complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
	complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
	complete_timestamp = complete_time_csv.utc
	@complete_timestamp = complete_time_csv.utc

	
	
	durations = ( complete_timestamp -	start_time_csv)
                 
		seconds = (durations % 60)
		minutes = (durations / 60) % 60
		hours = (durations / (60 * 60))
		days = (hours) / 24

		hours= hours % 24
		@duration_shipment = format("%02d days %02d hours %02d minutes", days,hours, minutes)
	end

		if @company.send_automated_emails == true
			@to_email = AutomatedEmailConfiguration.where(company_id: @company.id ,pickup_city:	@shipment.pickup_city , drop_city:@shipment.drop_city  )
			if (@to_email.present?)
				@to_email_splitted = @to_email.first.email.split(";")
					else
				@to_email_splitted = @shipment.company.cargo_owner.email
			end
			if @to_email_splitted.present?
				mail(to: @to_email_splitted, subject: "Lorryz trip summary for completed shipment no.:  #{@shipment.try(:id)}")
			end
		end
		# mail(to: "contact@lorryz.com", subject: "Lorryz - Customer Info")
	  end
	def change_destination_mail_to_admin(shipment, old_destination, new_destination)
		@shipment = shipment
		@old_destination = old_destination
		@new_destination = new_destination
		mail(to: "contact@lorryz.com", subject: "Request received for change in destination for #{@shipment.try(:id)}")
	end

	def expired_cargo_company 
		#shipment_users

		subject = "Expired Cargo Company Owner" 
		mail(to: ["contact@lorryz.com"], subject: subject)
	end

	def expired_fleet_company 
		#shipment_users

		subject = "Expired Fleet Company Owner" 
		mail(to: ["contact@lorryz.com"], subject: subject)
	end

	def expired_cargo_individual 
		#shipment_users

		subject = "Expired Cargo Individual Owner" 
		mail(to: ["contact@lorryz.com"], subject: subject)
	end

	def expired_fleet_individual 
		#shipment_users

		subject = "Expired Fleet Individual Owner" 
		mail(to: ["contact@lorryz.com"], subject: subject)
	end



	def change_destination_approved_mail(shipment, old_destination)
		@shipment = shipment
		@old_destination = old_destination
		shipment_users
		subject = "Lorryz - Shipment Change Destination Request Approved"
		mail(to: [@cargo_owner_email, @fleet_owner_email], subject: subject) if @cargo_owner_email.present? or @fleet_owner_email.present?
		# mail(to: "sohail.khalil@virtual-force.com", subject: "Request received for change in destination for #{@shipment.try(:id)}")
	end


	def status_change(to,cc,message,subject,previous_status,current_status,id)

	@shipment = Shipment.find_by_id(id)

	@to = to.split(',')
	@cc = cc.split(',')
	@message=message
	@before_status=previous_status
	@after_status =current_status

		
		mail(to:@to ,cc: @cc, subject: subject) 
	end

	def shipment_request (destination,origin,name,contact_no,amount,country,vehicle)

		@name = name
		@contact_no =contact_no 
		@destination = destination
		@origin = origin
		@amount = amount
		@country = country
		@vehicle = vehicle
	
		subject = "New Customer Checking Rates"
		mail(to: ["contact@lorryz.com", "hashmi@lorryz.com"], subject: subject)
	end

	

	def change_destination_approved_mail(before, after,shipment_id)
		@before_status = before
		@after_status = after
		@shipment = Shipment.find(shipment_id)

		subject = "Lorryz - Shipment No : " + shipment_id.to_s + " status"
		mail(to: [@cargo_owner_email, @fleet_owner_email], subject: subject) if @cargo_owner_email.present? or @fleet_owner_email.present?
		# mail(to: "sohail.khalil@virtual-force.com", subject: "Request received for change in destination for #{@shipment.try(:id)}")
	end


	def shipment_canceled shipment_id
		@shipment = Shipment.find(shipment_id)
		shipment_users
		subject = "Lorryz - Shipment Cancelled"
		mail(to: ["contact@lorryz.com", @fleet_owner_email], subject: subject)
	end

	def shipment_rating shipment_id, receiver_id
		@shipment = Shipment.find(shipment_id)
		@email = User.find(receiver_id).email
		subject = "Lorryz - Your Shipment has been Rated."
		mail(to: @email , subject: subject)
	end

	def bidless_shipment shipment_id
		@shipment = Shipment.find(shipment_id)

		@online_users = find_all_online_users

		subject = "No bids received in 90s – Shipment No : " + shipment_id.to_s
		mail(to: ["contact@lorryz.com"], subject: subject)
	end

	def no_bids_accepted shipment_id
		@shipment = Shipment.find(shipment_id)
		#shipment_users
		subject = "Customer did not accept bids in 180s – Shipment No : #{shipment_id}"
		mail(to: ["contact@lorryz.com"], subject: subject)
	end

	def shipment_not_started shipment_id
		@shipment = Shipment.find(shipment_id)
		#shipment_users
		subject = "Driver did not start shipment in 120s – Shipment No : #{shipment_id} "
		mail(to: ["contact@lorryz.com"], subject: subject)
	end


	def shipment_started shipment_id
		@shipment = Shipment.find(shipment_id)
		#shipment_users
		subject = "Track ongoing shipment – Shipment No : " + shipment_id.to_s
		mail(to: ["contact@lorryz.com"], subject: subject)
	end

	def new_shipment shipment_id
		#shipment_users
		@shipment = Shipment.find(shipment_id)

		subject = "New Shipment has been posted – Shipment No : " + shipment_id.to_s 
		mail(to: ["contact@lorryz.com"], subject: subject)
	end

	private
	def shipment_users
		@fleet_owner_email = @shipment.fleet.fleet_owner.email rescue ""
		@cargo_owner_email = @shipment.company.cargo_owner.email rescue ""
	end

	private
	def find_all_online_users
		#shipment = Shipment.find(params[:shipment_id])
		online_users = User.individual_fleet_owners
		online_users.each do |user|
			unless (user.is_online == true && user.country_id == @shipment.country_id)
				online_users -= [user]
			end
		end
		return online_users
	end





end
