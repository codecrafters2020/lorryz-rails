class ModeratorController < ActionController::Base
	layout 'moderator'
  before_action :authenticate_user!
  before_action :check_if_moderator
  include UserConcern
  def check_if_moderator
    redirect_to root_path unless current_user and moderator?(current_user)
  end
 
end
