class ActivitiesController < AdminController
  layout "admin"
  before_action :check_if_super_admin
  def index
    # @activities_link = "active"
    # @activities = PublicActivity::Activity.order("created_at desc")#.where(owner_id: current_user.friend_ids, owner_type: "User")
    @activities = Audit.where("action is NOT NULL and action != ''").order(created_at: :DESC).page params[:page]

    respond_to do |format|
    	format.html
    	format.xlsx {
    		start_date = params[:start_date].present? ? params[:start_date].to_date.beginning_of_day : 100.years.ago.to_date
    		end_date = params[:end_date].present? ? params[:end_date].to_date.end_of_day : 100.years.from_now.to_date
    		@activities  = @activities.where(created_at: start_date..end_date)
    		render xlsx: 'xlsx_activities', filename: "xlsx_activities.xlsx", disposition: 'attachment'
    	}

	  end
	end
end
