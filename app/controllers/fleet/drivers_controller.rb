class Fleet::DriversController < FleetController
  before_action :check_if_individual
  before_action :set_vehicle, only: [:show]
  before_action :set_drivers_title , only: [:index , :edit , :new]
  before_action :set_attachment_params , only: [:create , :update]
  include UserConcern
  def index
    instance_variable_set :"@#{__method__}" , "active"
    @drivers = User.where(:company_id => current_user.company_id,role:User.role.find_value(:driver).value)
    # render 'index', status: :ok
  end
  def new
    instance_variable_set :"@#{__method__}" , "active"
    @driver = User.new
    @driver_information = @driver.build_driver_information
  end
  def create
    full_name = params[:user][:full_name]
    firstname = full_name.split(" ")[0] rescue ''
    lastname = full_name.split(" ").drop(1).join("") rescue ''
    mobile = make_phone_number current_user.country.id , params[:user][:login]
    # mobile = current_user.country.dialing_code + params[:user][:login]
    password = Devise.friendly_token(length = 8)
    @driver = User.new(:mobile => mobile,
                       :first_name=> firstname,
                       :last_name => lastname,
                       :password => password,
                       :password_confirmation => password,
                       :role => User.role.find_value(:driver).value,
                       :company_id => current_user.company_id,
                       :country_id => current_user.country_id
    )
    emirate_documents = params[:user][:driver_information][:emirate_documents]
    license_documents = params[:user][:driver_information][:license_documents]
    @driver.build_driver_information(
        national_id: params[:user][:national_id],
        expiry_date: params[:user][:expiry_date],
        license: params[:user][:license],
        license_expiry: params[:user][:license_expiry],
        avatar: params[:user][:driver_information][:avatar],
        emirate_documents: emirate_documents,
        license_documents: license_documents)

    if @driver.save
      @driver.driver_login_sms()
      redirect_to fleet_drivers_path
    else
      flash[:error] = @driver.errors.full_messages
      redirect_back(fallback_location: fleet_drivers_path)
    end
  end
  def edit
    instance_variable_set :"@#{__method__}" , "active"
    @driver = User.where(id: params[:id]).includes(:driver_information).first
    @driver.mobile = @driver.mobile[3..12]
  end
  def update
    full_name = params[:user][:full_name]
    firstname = full_name.split(" ")[0] rescue ''
    lastname = full_name.split(" ").drop(1).join("") rescue ''
    make_phone_number_on_update
    mobile = params[:user][:login]
    @driver = User.find_by_id(params[:id])
    if @driver
      driver = @driver.update_attributes(:mobile => mobile,
                                         :first_name=> firstname,
                                         :last_name => lastname
      )
      emirate_documents = params[:user][:driver_information][:emirate_documents]
      license_documents = params[:user][:driver_information][:license_documents]
      # emirate_documents = emirate_documents.present? ? emirate_documents : nil
      driver_information = @driver.driver_information
      driver_information.update_attributes!(
          national_id: params[:user][:national_id],
          expiry_date: params[:user][:expiry_date],
          license: params[:user][:license],
          license_expiry: params[:user][:license_expiry],
          avatar: params[:user][:driver_information][:avatar],
          emirate_documents: emirate_documents,
          license_documents: license_documents)
      if driver && driver_information
        redirect_to fleet_drivers_path
      else
        flash[:error] = @driver.errors.full_messages
        render :edit
      end
    else
      render json: {"error": "Driver not found"}  , status: :ok
    end
  end

  def destroy
    @driver = User.find_by_id(params[:id])
    unless @driver.vehicle.present?
      @driver.driver_information.destroy
      @driver.destroy
    end
    redirect_to fleet_drivers_path
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_vehicle
    @driver = User.find(params[:id])
  end
  def make_phone_number_on_update
    params[:user][:login].sub!(/^0/, "")
    params[:user][:login] = current_user.country.dialing_code+ params[:user][:login].strip
  end
  def set_attachment_params
    if params[:user][:driver_information][:avatar].present?
      avatar = params[:user][:driver_information][:avatar]
      begin
        params[:user][:driver_information][:avatar]  = eval(avatar).first if avatar.present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
    end
    if params[:user][:driver_information][:emirate_documents].present?
      begin
        params[:user][:driver_information][:emirate_documents] = eval(params[:user][:driver_information][:emirate_documents].first) if params[:user][:driver_information][:emirate_documents].present?
      rescue SyntaxError => e
        params[:user][:driver_information][:emirate_documents] = params[:user][:driver_information][:emirate_documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:user][:driver_information][:emirate_documents] = params[:user][:driver_information][:emirate_documents].blank? ? [] : params[:user][:driver_information][:emirate_documents]

    end
    if params[:user][:driver_information][:license_documents].present?
      begin
        params[:user][:driver_information][:license_documents] = eval(params[:user][:driver_information][:license_documents].first) if params[:user][:driver_information][:license_documents].present?
      rescue SyntaxError => e
        params[:user][:driver_information][:license_documents] = params[:user][:driver_information][:license_documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:user][:driver_information][:license_documents] = params[:user][:driver_information][:license_documents].blank? ? [] : params[:user][:driver_information][:license_documents]

    end
  end
end
