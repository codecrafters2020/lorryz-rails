class Fleet::ShipmentsController < FleetController
  before_action :get_shipment , only: [:quit_bid, :show,:cancel, :assign_vehicle,:update, :update_destination, :get_coordinates]
  # before_action :set_title , only: [:show , :ongoing , :completed , :active , :won]

  include ShipmentConcern

  def posted
    @new = "active"
    @title = "New"
    @shipments = Shipment.filter_country(current_user.country_id).fleet_posted(current_user.company_id,current_user)
    @shipments = @shipments.sort_by {|s| [s.try(:company).try(:featured) ? 0 : 1 , s.try(:company).try(:verified) ? 0 : 1 ]}
    respond_to_xls
  end

  def active
    @active = @title =  "active"
    @shipments = Shipment.filter_country(current_user.country_id).fleet_active(current_user.company_id).remove_not_interested(current_user)
    respond_to_xls
  end

  def won
    @won = "active"
    @title = "Won"
    @shipments = Shipment.where(state: ["accepted" ,"vehicle_assigned"], fleet_id: current_user.company_id)
    respond_to_xls
  end

  def ongoing
    @ongoing = "active"
    @title  = "Ongoing"
    

    @shipments = Shipment.where(state: ["ongoing", "loading", "en_route", "unloading"], fleet_id: current_user.company_id)
    
    
    @shipments = Shipment.fleet_individual_shipments(current_user.company_id).where(state: ["ongoing", "loading", "en_route", "unloading"]) if Shipment.fleet_individual_shipments(current_user.company_id).where(state: ["ongoing", "loading", "en_route", "unloading"]).present?

    @shipments = @shipments.page params[:page]  if @shipments.present?


    # @shipments = Shipment.where(state: [ "ongoing", "loading", "en_route", "unloading"], fleet_id: current_user.company_id)
    respond_to_xls
  end

  def completed
    @completed = "active"
    @title = "Completed"
    @shipments = Shipment.where(fleet_id: current_user.company_id).where('state =? OR (state=? AND cancel_by=?)', "completed",'cancel','fleet_owner')
    # @shipments = Shipment.where(state: ["completed", "cancel"], fleet_id: current_user.company_id)
    respond_to_xls
  end

  def get_coordinates
    # data = @shipment.shipment_vehicles.where(status: ShipmentVehicle::ONGOING_VEHICLES).map{|sv| {lat: sv.vehicle.latitude.to_f, lng: sv.vehicle.longitude.to_f, status: (sv.status.humanize rescue "") ,id: sv.id.to_s} }
    # render json: { coordinates:  data}, status: :ok
    render json: { coordinates:  get_shipment_coordinates(@shipment.id)}, status: :ok
  end

  def assign_vehicle
    respond_to do |format|
      format.js
    end
  end

  def cancel
    respond_to do |format|
      format.js
    end
  end

  def show
    redirect_to root_path, error: "Unauthorized" if @shipment.state != "posted" and @shipment.fleet_id !=  current_user.company_id && !@shipment.is_createdby_admin
    instance_variable_set :"@#{@shipment.state}" , "active"
  end

  def respond_to_xls
    respond_to do |format|
      format.html
      format.xlsx {
            render xlsx: 'xls_shipments', filename: "all_shipments.xlsx", disposition: 'attachment'
          }
    end
  end

  def quit_bid
    @shipment.bids.where(:company_id => current_user.company_id).destroy_all
    # Shipment.find(params[:id]).bids.where(:company_id => current_user.company_id).update_all(status: 'rejected')
    redirect_to active_fleet_shipments_path , notice: "bid quit successfully"
  end

  def update
    if @shipment.update(update_shipment_params)
      if params["shipment"]["vehicle_ids"].present?
        vehicle_ids= params["shipment"]["vehicle_ids"].reject { |c| c.empty? }
        if vehicle_ids.present?
          params["shipment"]["vehicle_ids"].reject { |c| c.empty? }.each do |id|
            shipment = Shipment.find(params["id"])
            vehicle = Vehicle.find(id)
          end
          if vehicle_ids.present? and @shipment.state != "vehicle_assigned"
            @shipment.update!(state_event: :vehicle_assigned) rescue (flash[:error] = @shipment.errors.full_messages)
          end
        end
      end  
      redirect_back(fallback_location: fleet_shipments_path) #, notice: "Shipment updated Successfully!"
    else
      flash[:error] = @shipment.errors.full_messages
      redirect_back(fallback_location: fleet_shipments_path) #, notice: "Shipment updated Successfully!"
    end
  end

  def not_interested
    current_user.not_interested_shipments.create(shipment_id: params[:id])
    flash[:notice] ="Shipment marked as not interested."
    # redirect_back(fallback_location: fleet_root_path)
    redirect_to posted_fleet_shipments_path
  end

  private

  def get_shipment
    shipment_country_access_control params[:id]
    @shipment = Shipment.find_by_id params[:id]
  end

  def shipment_params
    params.require(:shipment).permit(:cancel_reason, :cancel_by, :state_event ,:state, :amount, vehicle_ids: [])
  end
  def update_shipment_params
    params.require(:shipment).permit(:cancel_reason, :cancel_by ,:state, :state_event, :amount, vehicle_ids: [])
  end

end
