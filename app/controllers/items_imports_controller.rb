class ItemsImportsController < ApplicationController

  def new
    @items_import = ItemsImport.new
  end

  def create
    @items_import = ItemsImport.new(params[:items_import])
    if @items_import.save
      puts("\n\n\n\n\n\n\n\n #{current_user.role}")
      
      if current_user.moderator?
        redirect_to moderator_fleet_owners_path

      else         
        redirect_to admin_fleet_owners_path

      end
    else
      render :new
    end
  end
end