class Api::V1::Fleet::VehiclesController < Api::V1::ApiController
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy,:change_driver, :update_lat_lng]

  def index
    
    if vehicle_assignment?
      @vehicles = Vehicle.reterive_for_assignment(current_user)
    elsif vehicle_filter?
      @vehicles = Vehicle.search_by_filters(current_user,params)
    else 
      @vehicles = Vehicle.fetch_by_company(@user.company_id)
    end
    
    render "index" , status: :ok
  end

  def show
  end

  def new
    @vehicle = Vehicle.new
  end

  def edit
  end

  def vehicle_types
    
    render json: VehicleType.where(:country_id => @user.country_id), status: :ok
  end
  def create
    @vehicle =  @user.company.vehicles.create(vehicle_params)
    @vehicle.errors.blank? ? ( render "create" , status: :ok ) : (render json: { errors: @vehicle.errors.full_messages }, status: :bad_request)

  end

  def update
    if @vehicle.update(vehicle_params)
      render "create" , status: :ok 
    else
      render json: { errors: @vehicle.errors.full_messages }, status: :bad_request
    end
  end

  def update_lat_lng
    
    if @vehicle.latitude !=  params[:vehicle][:latitude] && @vehicle.longitude !=  params[:vehicle][:longitude] &&  params[:vehicle][:latitude] != 0

      @vehicle.update_attributes(latitude:params[:vehicle][:latitude] ,longitude: params[:vehicle][:longitude] ,coordinates_updated_at:DateTime.now())   

      if  ShipmentVehicle.where(vehicle_id: @vehicle.id).where.not(status: 6).last.present?
      @shipment =  Shipment.find_by_id(ShipmentVehicle.where(vehicle_id: @vehicle.id).where.not(status: 6).last.shipment_id)

      
      if  Geocoder::Calculations.distance_between([params[:vehicle][:latitude],params[:vehicle][:longitude]],[@shipment.pickup_lat,@shipment.pickup_lng],:units =>:km) < 0.5
        if @shipment.state == "ongoing" && (@shipment.status != "Enroute to Origin Border" && @shipment.status != "At Loading" && @shipment.status != "Enroute to Destination" && @shipment.status != "At Offloading Point" && @shipment.status != "Completed")
          
          @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id ).where.not(status: [3,4,5,6]).all
          @shipment_vehicles.each do |shipment_vehicle|
            shipment_vehicle.update(status: 3)
          
          end

          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng)
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng )  if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: @vehicle.id,state:  "loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng )  if  ShipmentActionDate.where(shipment_id: @shipment.id , vehicle_id: @vehicle.id,state:  "loading").blank?
                  
          @shipment.update_attributes(status: "At Loading")
        else
        end
      
      elsif @shipment.status == "At Loading"
        @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id ).where.not(status: 5).all

        @shipment_vehicles.each do |shipment_vehicle|
          shipment_vehicle.update(status: 4)
        
        end


         if @shipment.category  == "Cross_Border"
          @shipment.update_attributes(status: "Enroute to Origin Border")
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng)  if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: @vehicle.id,state:  "enroute", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id,  vehicle_id: @vehicle.id,state:"enroute").blank?

        else
          @shipment.update_attributes(status: "Enroute to Destination")
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Destination", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng)  if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: @vehicle.id,state:  "enroute", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng )  if  ShipmentActionDate.where(shipment_id: @shipment.id , vehicle_id: @vehicle.id,state:  "enroute").blank?

        end
    end
    
    if  Geocoder::Calculations.distance_between([params[:vehicle][:latitude],params[:vehicle][:longitude]],[@shipment.drop_lat,@shipment.drop_lng],:units =>:km) < 0.5 && (@shipment.status == "At Loading" || @shipment.status == "Enroute to Origin Border" || @shipment.status == "Enroute to Destination")
     
      @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id ).all
      @shipment_vehicles.each do |shipment_vehicle|
        shipment_vehicle.update(status: 5)
      
      end

      ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Offloading Point", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) 
      @shipment.update_attributes(status: "At Offloading Point")
     
      ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: @vehicle.id,state:  "unloading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng )if  ShipmentActionDate.where(shipment_id: @shipment.id , vehicle_id: @vehicle.id,state:  "unloading").blank?
      

    end

    if  Geocoder::Calculations.distance_between([params[:vehicle][:latitude],params[:vehicle][:longitude]],[@shipment.drop_lat,@shipment.drop_lng],:units =>:km) > 0.5 &&  (@shipment.status == "At Offloading Point")

      @shipment_vehicles= ShipmentVehicle.where(shipment_id: @shipment.id ).all
      @shipment_vehicles.each do |shipment_vehicle|
        shipment_vehicle.update(status: 6)
      
      end

      ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Completed", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) 
      @shipment.update_attributes(status: "Completed")
      @shipment.update_attributes(state_event: "completed")
      ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: @vehicle.id,state:  "finish", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng )if  ShipmentActionDate.where(shipment_id: @shipment.id , vehicle_id: @vehicle.id,state:  "finish").blank?
      ShipmentMailer.send_automated_email( @shipment.id).deliver_now
      end
    

  end

    Aws.config.update({
      region: "ap-south-1",
      credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')     
    })
    
    dynamodb = Aws::DynamoDB::Client.new
    table_name = 'vehicle_coordinates'
    vehicle_id = @vehicle.id
    lat = params[:vehicle][:latitude]
    lng =  params[:vehicle][:longitude]

    item = {
      vehicle_id: vehicle_id,
      created: DateTime.now.utc.to_s,
      lat: lat,
      lng: lng,
       
    }  
     params1 = {
        table_name: table_name,
        item: item
    }
    
    begin
        dynamodb.put_item(params1)
    
    rescue  Aws::DynamoDB::Errors::ServiceError => error
        puts "Unable to add item:"
        puts "#{error.message}"
    end
  end

  
    if @vehicle.update(vehicle_params)
      render json: { message: "lat lng updated" }, status: :ok
    else
      render json: { errors: @vehicle.errors.full_messages }, status: :bad_request
    end
  end

	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end
  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    @vehicle.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def change_driver
    @vehicle.user_id = params[:driver_id]
    if @vehicle.save
      render json: { success: "Driver Assigned to Vehicle Successfully" }, status: :ok
    else
      render json: { errors: @vehicle.errors.full_messages }, status: :bad_request
    end
  end

  def fetch_nearby
    @vehicles = Vehicle.within(8, :origin => [params[:lat],params[:lng]])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
      params.require(:vehicle).permit(:user_id,:id, :latitude, :longitude, :registration_number, :insurance_number, :vehicle_type_id, :company_id,:expiry_date,:authorization_letter,:available, :not_available_to,:not_available_from,{documents: []},:insurance_expiry_date)
    end

    def vehicle_assignment?
      params[:assignment].present?
    end

    def vehicle_filter?
      params[:state].present? && params[:state] != 'undefined'
    end
end
