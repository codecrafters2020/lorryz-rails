class Api::V1::Users::SessionsController < Devise::SessionsController
  require 'auth_token'
  include UserConcern
  # Disable CSRF protection
  skip_before_action :verify_authenticity_token
  skip_before_action :verify_signed_out_user 
  # Be sure to enable JSON.
  respond_to :html,:json
  
  # POST /resource/sign_in
  def create
    make_phone_number params
    @user = User.find_by_email_or_phone(params)    
    unless @user
      return render json: {error: 'Invalid credentials'}, status: :unauthorized
    end

    if @user and @user.try(:company).try(:blacklisted)
      render json: { error: 'Your account is being verified and you will be notified shortly by Lorryz Team. For any enquiries please send email to contact@lorryz.com or call 056 5466088' },status: :unauthorized
    else
      if @user.mobile_verified?
        if email_authentication?(params)
          if @user && @user.valid_password?(params[:user][:password])
            # debugger
            # warden.set_user(@user, scope: :user)
            # self.resource = warden.authenticate!(auth_options)
            # set_flash_message(:notice, :signed_in) if is_flashing_format?
            sign_in(@user, store: false)
            # yield resource if block_given?
            render json: @user
          else  
            render json: {error: 'Invalid Phone no / password'}, status: :unauthorized
          end
        elsif phone_authentication?(params)
            if @user && @user.valid_password?(params[:user][:password])
              render json: @user
            else
              render json: {error: 'Invalid Phone no / password'}, status: :unauthorized    
            end
        else
          render json: {error: 'Email is invalid'}, status: :unauthorized
        end
      else  
        render json: {error: 'Phone no is not verified'}, status: :unauthorized
      end
    end
  end

  def destroy
    unless all_signed_out?
      Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
      yield if block_given?
      render body: 'Logout Successfully', status: json_status(true)
    else
      render body: 'User Already Logged out', status: json_status(true)
    end
  end

  def require_no_authentication
  end

  private

  def make_phone_number params
    dialing_code = Country.find_by_dialing_code(params[:user][:country]).try(:dialing_code)
    
    # if dialing_code.present?
    #   params[:user][:email] = dialing_code + params[:user][:email].strip
    # end
  end

end
