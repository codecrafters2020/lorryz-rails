class Api::V1::Users::RegistrationsController < Devise::RegistrationsController
  # Disable CSRF protection
  prepend_before_action :authenticate_scope!, only: []
  skip_before_action :verify_authenticity_token
  include UserConcern
  # Be sure to enable JSON.
  respond_to :html, :json

  def create
    make_phone_number params
  	build_resource(sign_up_params)
  	resource_saved = resource.save
  	  yield resource if block_given?
      if resource_saved
        newuser= params[:user]
        newuser_country = Country.find_by_id(params[:user][:country_id] )
        UserMailer.send_new_user_update(newuser,  newuser_country.try(:name)).deliver
    	    if resource.active_for_authentication?
    	      sign_in(resource_name, resource, store: false)
            render json: resource
          return
  	    else
  	     expire_data_after_sign_in!
  	    end
  	 end
  	   render_resource_or_errors(resource)
  end



  def update
    make_phone_number params
    resource = User.find_by_id(params[:user][:id])

    resource_updated = resource.update_attributes(account_update_params)
    if resource_updated
      sign_in resource_name, resource, bypass: true
      resource.assgin_verification_code
      resource.send_verification_code
      render json: resource
      return
    end
    render_resource_or_errors(resource)
  end

  def destroy
    resource.destroy
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    yield resource if block_given?
    render nothing: true, status: json_status(resource.destroyed?)
  end

  def require_no_authentication
  end

  private

  def make_phone_number params
 
    country = Country.find_by_dialing_code(params[:user][:country])
    dialing_code = country.try(:dialing_code)
      
    if dialing_code.present?
      params[:user][:country_id] = country.id
      # params[:user][:mobile] = dialing_code + params[:user][:mobile].strip
    end

  end
 

  def resource_name
   :user
  end

end

