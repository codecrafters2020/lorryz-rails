class Api::V1::CompaniesController < Api::V1::ApiController
  

  def add_company_information
    update_information
  end
  
  def add_individual_information
    update_information
  end

  def update_information
    if password_update_request?
      updated = validate_and_update_password
      if updated == true 
        send "#{params[:action]}_detail" 
      else
       return updated
      end
    else
      send "#{params[:action]}_detail"
    end
  end

  def validate_and_update_password
    if password_update_request? 
      password_match? ? update_password : render_response("Confirm password does not match")
    end
  end
  def get_individual_information
    @info = @user.company.individual_information
    render :json => @info 
  end

  def get_company_information
    @info =  @user.company.company_information
    render :json => @info
  end

  private
    def company_params
      params.require(:company_informations).permit(:name,:address,:landline_no,:website_address,:license_number,:license_expiry_date,:secondary_contact_name,:secondary_mobile_no,:news_update,:password,:confirm_password,:current_password,:avatar,:tax_registration_no,{documents: []})
    end

    def individual_params
      params.require(:individual_informations).permit(:secondary_mobile_no,:landline_no,:national_id,:expiry_date,:news_update,:password,:confirm_password,:current_password,:tax_registration_no,:avatar,{documents: []})  
    end

  def password_valid?
    @user.valid_password?(request_params[:current_password])
  end

  def password_update_request?
    true if request_params[:password].present? or request_params[:confirm_password].present?
  end

  def password_match?
    request_params[:password] == request_params[:confirm_password] 
  end

  def render_response(msg)
    render json: {error: msg}   
  end

  def update_password
    if request_params[:password].length >= 8
      @user.password = request_params[:password]
      @user.save
    else
      render_response("Passwords must be at least 8 characters long")
    end
    
  end

  def add_company_information_detail
    @company = current_user.company
    update_tax_registration_no

    if company_information?
      @company.company_information.update(information_params)
      render_company_json(true)
    else
      @company.build_company_information(information_params)
      @company.save
     render_company_json()

    end
    
  end

  def add_individual_information_detail
    @company = current_user.company
    update_tax_registration_no
    if individual_information?
      @company.individual_information.update(information_params)
      render_individual_json(true)
    else
      @company.build_individual_information(information_params)
      @company.save
      render_individual_json()
    end
  end


  def update_tax_registration_no
    @user.company.update_attributes(tax_registration_no: request_params[:tax_registration_no])
  end

  def request_params
    params_type = { add_individual_information: 'individual_params', add_company_information: 'company_params' }
    eval("#{params_type[params[:action].to_sym]}")
  end

  def information_params
    request_params.except(:password,:current_password,:confirm_password,:tax_registration_no)
  end

  def company_information?
    @company.company_information.present?
  end

  def individual_information?
    @company.individual_information.present?
  end

  def render_company_json(update =  false)
    if update
      @msg = "Company Information updated successfully"
    else
      @msg = "Company Information added successfully"
    end

    render 'company_information'  

  end

  def render_individual_json(update = false)
    if update
      @msg = "Individual Information updated successfully"
    else
      @msg = "Company Information added successfully"
    end
    render 'individual_information'
  end

end
