class Api::V1::UsersController < Api::V1::ApiController
    require 'date'
		before_action :verify_jwt_token, except: [:reset_password]

    def send_phone_verify_code
       @user.update(params.permit(:mobile)) && (@user.assgin_verification_code; @user.send_verification_code)
   	end

   	def verify_phone_number
      if @user.verify_code?(params[:code])
      	render json: {verified: true}
      else
      	render json: {verified: false, error: "Code is invalid"}
      end
   	end


   	def resent_code
   		@user.send_verification_code
   		if @user.mobile_verified == "code_sent"
   			render json: {sent: true,success: "Code is sent on your Phone"}
   		else
   			render json: {sent: false,error: "We are unable to sent code. Please try later"}
   		end
   	end


   	def register_as_fleet_owner
   		if @user.update_column(:role, User.role.find_value(:fleet_owner).value)
            render json: {registerd_as_fleet_owner: true,success: "success"}
         else
            render json: {registerd_as_fleet_owner: true,error: "We are unable to register you as fleet owner"}
         end
   	end

    def register_as_cargo_owner
       if @user.update_column(:role, User.role.find_value(:cargo_owner).value)
          render json: {registerd_as_cargo_owner: true,success: "success"}
       else
          render json: {registerd_as_cargo_owner: true,error: "We are unable to register you as fleet owner"}
       end
    end

    def register_as_individual

      company = @user.register_as_individual

      if company
        render json: {registerd_as_fleet_owner: true,success: "success"}
      else
        render json: {registerd_as_fleet_owner: true,error: "We are unable to register you as fleet owner"}
      end
    end

    def register_as_company

      company = @user.register_as_company

      if company
        render json: {registerd_as_fleet_owner: true,success: "success"}
      else
        render json: {registerd_as_fleet_owner: true,error: "We are unable to register you as fleet owner"}
      end
    end

   def current_user_detail
     render json: @user
   end

    def db_device_id
        respond_to do |format|
            format.json { render json: {user_device_id: @user.device_id}, status: :ok }
        end
    end

   def change_online_status
    if(@user.device_id == params[:device_id])
        d = DateTime.now
        session = nil
        if(@user.is_online == true && params[:is_online] == false)
          if(@user.session_log_id?)
            session = SessionLog.find(@user.session_log_id)
            session.end_time = d
            session.save
          else
            session = SessionLog.new(:user_id => @user.id, :company_id => @user.company_id, :start_time => d, :end_time => d)
            session.save
          end
         elsif(@user.is_online == false && params[:is_online] == true)
          session = SessionLog.new(:user_id => @user.id, :company_id => @user.company_id, :start_time => d)
          session.save
         end
         session_id = (session != nil) ? session.id : nil
         if @user.update_attributes(:is_online=> params[:is_online], :session_log_id => session_id)
           render json: {
            status: :ok
           }
         else
           puts "@"*100, @user.errors.full_messages
           render json: {
            errors: "#{@user.errors}",
            status: 400
           }
         end
    end
   end





   #def schedule_change_online_status
   # @people = User.find(:all)
   # @people.each do |p|
   #   p.change_online_status(false)
   #end














   def reset_password
     @user = User.find_by_email_or_phone(params)
     if @user
        password = Devise.friendly_token(length = 8)
        @user.password = password
        @user.save
        @user.send_reset_password_code(password)
        render json: {success: "Password has been sent to your phone number"}
    else
      render json: {errors: "Can't find user."}
    end
   end

  def update
    if @user.update_attributes(user_params)
      render json: {
          status: :ok
      }
    else
      puts "@"*100, @user.errors.full_messages
      render json: {
          errors: "#{@user.errors}",
          status: 400
      }
    end
  end

  def destroy
    @user = User.find_by_id params[:id]
    render json: {
      status: :ok
    }
  end

  def notifications
    @user = User.find params[:id]
    @user.unread_notifications.update_all(viewed_flag: true)
    @notifications = @user.notifications.order(created_at: :desc).page params[:page]
    render 'notifications', status: :ok
  end

  def change_device_id
      @user = User.find params[:id]
      if(@user.device_id == params[:device_id])
          @user.update_attributes(device_id: "")
      end
      render json: {
          status: :ok
      }
  end

  private
  def user_params
    params.require(:user).permit(:id,:device_id,:os, :email, :mobile, :password, :password_confirmation, :current_password, :is_online)
  end
end
