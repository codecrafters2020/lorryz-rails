class Api::V1::CountriesController < Api::V1::ApiController
  
  before_action :verify_jwt_token, except: [:index]
  before_action :set_country, only: [:show, :edit, :locations, :vehicles,:primary_vehicles]
  def index
    @countries = Country.all.where(:status => true)
    render json: @countries, status: :ok
  end

  def locations
    @company = @user.company
  	@locations = @company.locations
  end

  def vehicles
	  #@vehicles = @country.vehicle_types.join(:avatar_attachment).order(:name)
    @vehicles = VehicleType.eager_load(:avatar_attachment).where(:country_id => @country.id).order(:id)
  end

  def primary_vehicles
	  #@vehicles = @country.vehicle_types.join(:avatar_attachment).order(:name)
    @vehicles = PrimaryVehicle.where(:country_id => @country.id).order(:id)
  end

  def secondary_vehicles 
    primary_vehicle = params[:id]
	  #@vehicles = @country.vehicle_types.join(:avatar_attachment).order(:name)
    @vehicles = VehicleType.eager_load(:avatar_attachment).where(:primary_category =>  params[:id]).order(:id)
  end

  private
  def set_country
    @country = Country.find_by_id(params[:id])
  end
end
