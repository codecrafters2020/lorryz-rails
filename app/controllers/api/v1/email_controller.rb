class Api::V1::EmailController < Api::V1::ApiController
  before_action :verify_jwt_token

  def send_shipment_email

    case params[:shipment_status]
      when 'started' then ShipmentMailer.shipment_started(params[:shipment_id]).deliver_now
      when 'not_started' then ShipmentMailer.shipment_not_started(params[:shipment_id]).deliver_now
      when 'no_bids_accepted' then ShipmentMailer.no_bids_accepted(params[:shipment_id]).deliver_now
      when 'no_bids' then ShipmentMailer.bidless_shipment(params[:shipment_id]).deliver_now
    end

    render json: {success: "Shipment updates successfully sent in email"}

  end


  private
  def set_shipment
    @shipment = Shipment.find(params[:id])
  end
end
