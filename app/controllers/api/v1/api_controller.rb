class Api::V1::ApiController < ApplicationController
	
	skip_before_action :verify_authenticity_token
	respond_to :json
	before_action :verify_jwt_token, except: [:apk_version]

	def apk_version
		# puts "\n\n\n\t\t colors is  #{APP_VERSION} \n\n"
		render json: {version: APP_VERSION,success: "APK Version sent"}

	end
end