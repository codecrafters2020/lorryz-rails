class CountriesController < ApplicationController
  # before_action :set_country, only: [:show, :edit, :locations, :vehicles]
  def index
    @countries = Country.all
    render json: @countries, status: :ok
  end


  def get_vehicles_by_country
  


    if params[:filter_by] && params[:type].present? 
      @country = Country.find_by_name(params[:filter_by])
      # binding.pry
      if @country
        @vehicle_types = @country.vehicle_types.order(:name)    
      else
        @vehicle_types = []
      end

      if @vehicle_types.present?
       x = params[:type].downcase  

       x = "CrossBorder" if x == 'cross_border'

       x = 'intra' if x == 'within_city'
       x = 'inter' if x == 'inter_city'
       @vehicle_types = @vehicle_types.where(x => true)
        end
     


    end
    render json: {vehicle_types: @vehicle_types}, status: :ok
  end

  def locations
    @locations = @country.locations
  end
  def location
    @location = Location.find(params[:location_id])
    render json: {pickup_location: @location.pickup_location,
                  pickup_building_name: @location.pickup_building_name,
                  pickup_lat: @location.pickup_lat,
                  pickup_lng: @location.pickup_lng,
                  pickup_city: @location.pickup_city,
                  drop_location: @location.drop_location,
                  drop_building_name: @location.drop_building_name,
                  drop_lat: @location.drop_lat,
                  drop_lng: @location.drop_lng,
                  drop_city: @location.drop_city,
                  rate: @location.rate,
                  loading_time: @location.loading_time  ,
                  unloading_time: @location.unloading_time,
                  vehicle_type_id: @location.vehicle_type_id}
  end
  def vehicles
    @vehicles = @country.vehicle_types
  end

  private
  def set_country
    @country = Country.find_by_id(params[:id])
  end
end
