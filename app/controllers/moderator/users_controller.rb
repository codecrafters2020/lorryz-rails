class Moderator::UsersController < ModeratorController
  layout 'moderator'

  include UserConcern

  before_action :check_if_moderator , only: [:new , :create ,:show]
  before_action :set_user , only: [:show,:change_password,:update]

  autocomplete :user, :fleet_owner_name,:column_name => 'first_name', :extra_data => [:last_name], :display_value => :full_name,:scopes => [:fleet_owners]
  autocomplete :user, :cargo_owner_name,:column_name => 'first_name', :extra_data => [:last_name], :display_value => :full_name,:scopes => [:cargo_owners]
  autocomplete :vehicle, :vehicle_name,:column_name => 'registration_number'

  def index
    @users = User.all.reverse
  end
  def change_password
 



    def update 

      @user.update_attributes(password: params[:user][:password])
  
      redirect_to moderator_users_path,notice: "password successfully updated"
  
  
    end 
  end
  def get_all_drivers
    @vehicles = Vehicle.all.filter_by_country_id(current_user.country.try(:id)) 
  #  @shipments = Shipment.includes(:vehicle_type,:shipment_vehicles).where(shipment_vehicles: {status: ShipmentVehicle::ONGOING_VEHICLES })
  render json: { coordinates:  get_driver_bulk_coordinates(@vehicles,'','','') }, status: :ok
end


  def get_all_drivers_filtered
    radius = 100 if  params[:radius].blank?
    radius = params[:radius] if  params[:radius].present?

    company = nil
    company = CompanyInformation.find_by_id(params[:cargo_company_id]).company_id if params[:cargo_company_id].present?

    @vehicles = Vehicle.filter_by_country_id(current_user.country.try(:id)) 


    @vehicles = @vehicles.filter_by_country_id(params[:country])  if params[:country].present?
    @vehicles = @vehicles.filter_by_imei(params[:imei])  if params[:imei].present?
    @vehicles = @vehicles.filter_by_vehicle(params[:vehicle_number])  if params[:vehicle_number].present?
    @vehicles = @vehicles.filter_by_shipment(params[:shipment_no])  if params[:shipment_no].present?
    @vehicles = @vehicles.filter_by_vehicletype(params[:vehicle_type])  if params[:vehicle_type].present?
    @vehicles = @vehicles.filter_by_customer(company)  if params[:cargo_company_id].present?
    @vehicles = @vehicles.filter_by_fleet(User.find_by_id(params[:company_fleet_id]).company_id)  if params[:company_fleet_id].present?
    @vehicles = @vehicles.within(radius, :origin => [params[:centre_lat],params[:centre_lng]])  if params[:centre_lat].present? && params[:centre_lng].present?

    
    if params[:status].present?
    @vehicles = @vehicles.filter_by_status_shipment(params[:status])     if params[:status] !="Free"
   end
   
     render json: { coordinates:  get_driver_bulk_coordinates(@vehicles,params[:shipment_no],company,params[:company_fleet_id]),result:"abc" }, status: :ok
 end
  
  def set_user
    @user = User.find(params[:id])
  end
  def show

    respond_to do |format|
      format.html
     format.xlsx {
            start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
            end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
            @vehicle  = @user.vehicle if  @user.vehicle.present?
            
          Aws.config.update({
            region: "ap-south-1",
            credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
            })
            dynamodb = Aws::DynamoDB::Client.new
            table_name = 'vehicle_coordinates'
          
            zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")
            vehicle_id=0
           
          
            if  start_date.present? && end_date.present? 
            start_time = {"date"=>  start_date.strftime("%d/%m/%y") , "time"=>  Time.now.strftime("%H:%M") }
            start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
            tz = ISO3166::Country.new(current_user.country.short_name).timezones.zone_identifiers.first
            start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
            start_timestamp = start_time_csv.utc
            complete_time = {"date"=> end_date.strftime("%d/%m/%y") ,  "time"=> Time.now.strftime("%H:%M") }
            complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
            complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
            complete_timestamp = complete_time_csv.utc
            vehicle_id  = @user.vehicle.id if  @user.vehicle.present?
                 
            params1 = {
          
              table_name: table_name,
              key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
              expression_attribute_names: {
                "#created" => "created",
                "#vehicle_id" => "vehicle_id"
              },
              expression_attribute_values: {
              ":vehicle_id" => vehicle_id,
              ":start_time" =>   start_timestamp.to_s,
              ":end_time" => complete_timestamp.to_s
              }
            }
            if start_timestamp < complete_timestamp
             @resp = dynamodb.query(params1) 
            end
	# url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{ship.pickup_lat.to_f},#{ship.pickup_lng.to_f}&destinations=#{ship.drop_lat.to_f},#{ship.drop_lng.to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
	# 	response = HTTParty.get(url)

  if @resp.count > 1
  url =  "https://maps.googleapis.com/maps/api/geocode/json?latlng=#{@resp.items.first['lat']},#{@resp.items.first['lng']}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
          
      response = HTTParty.get(url)
      
      @start_location=response['results'].last['formatted_address']


  url =  "https://maps.googleapis.com/maps/api/geocode/json?latlng=#{@resp.items.last['lat']},#{@resp.items.last['lng']}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
          
  response = HTTParty.get(url)
  
  @end_location=response['results'].last['formatted_address']
end
            render xlsx: 'moderator/users/index_fleet', filename: "Fleet_Report.xlsx", disposition: 'attachment', stream: true

          end
        
            
         }

    end
  end

  

end
