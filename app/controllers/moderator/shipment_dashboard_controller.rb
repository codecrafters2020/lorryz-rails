class Moderator::ShipmentDashboardController < ModeratorController
  before_action :authenticate_user!
  before_action :check_if_moderator

  before_action :set_shipment_posted_link



  layout 'moderator'


  def shipment_posted
    # @gps = GpsVehicle
  
    params[:query] ||=   {}
  
    if params[:query].present?
    @shipments = Shipment.unscoped {  Shipment.search(params[:query]).where.not('state = ? OR state = ? ', "completed",'cancel').where(:country_id => current_user.country.try(:id)).paginate(:page=>params[:page],per_page:30).order(pickup_date: :desc)}

    else
    @shipments =Shipment.unscoped {  Shipment.where.not('state = ? OR state = ? ', "completed",'cancel').where(:country_id => current_user.country.try(:id)).paginate(:page=>params[:page],per_page:30).order(pickup_date: :desc)}
    end
    @ongoing ='Ongoing shipments : ' + @shipments.where('state =? ', "ongoing").count.to_s
    @posted ='Posted shipments : ' + @shipments.where('state =? ', "posted").count.to_s
    @upcoming= 'Upcoming shipments : ' +@shipments.where('state =? OR state=? ', "accepted",'vehicle_assigned').count.to_s
  end


 
  private


  def set_shipment_posted_link
    @shipment_dashboard_link = "active"
  end

end
