class Moderator::CompanyInformationController < ModeratorController
    autocomplete :company_information, :name, :extra_data => [:company_id]

end