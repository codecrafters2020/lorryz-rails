class Moderator::InstantRatesController < ModeratorController
    before_action :authenticate_user!
    before_action :check_if_moderator
    before_action :set_instant_rate , only: [:edit, :update,:destroy]
    before_action :set_instant_rate_link , only: [:edit, :update , :new , :index ]
    layout 'moderator'
    def index
      @instant_rates = InstantRate.all
      respond_to do |format|
        format.html
        format.xlsx {
          start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
          end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
          @instant_rate = InstantRate.where(:pickup_country => current_user.country.try(:name))
        }
      end
    end
    def new
      @instant_rate = InstantRate.new
    end
    def edit
    end
    def create
      @instant_rate = InstantRate.new(instant_rates_params)
      print instant_rates_params
      if @instant_rate.save
        redirect_to moderator_instant_rates_path , notice: "Instant Rate Created Successfully!"
      else
        format.html { render :new }
      end
    end
    def update
      if @instant_rate.update(instant_rates_params)
        redirect_to moderator_instant_rates_path , notice: "instant_rates Updated Successfully!"
      else
        format.html { render :edit}
      end
    end
    def destroy
      if @instant_rates.destroy
        redirect_to moderator_instant_rates_path, notice: "instant_rates destroyed successfully"
      else
        redirect_back(fallback_location: moderator_instant_rates_path)
      end
    end
    private
    def set_instant_rate
        @instant_rate = InstantRate.find(params[:id])
      end
    def instant_rates_params
      params.require(:instant_rate).permit(:pickup_city, :pickup_country,:to_country, :to_city, :vehicle_type_id, :amount)
    end
    def set_instant_rate_link
      @instant_rate_link = "active"
    end
  end
  