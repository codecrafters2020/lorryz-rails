class Moderator::GpsVehicleController < ModeratorController
  before_action :authenticate_user!
  before_action :check_if_moderator
  before_action :set_country , only: [:edit, :create,:index , :new,  :update, :destroy]
  before_action :set_gps , only: [:edit,  :update, :destroy]

  before_action :set_vehicle_driver, only: [:edit , :new, :create , :update]
  before_action :set_gps_link ,only: [:edit,  :index , :new]
  layout 'moderator'
  def index
    params[:query] ||=   {}
    params[:query][:country_id]=current_user.country.try(:id)
    @gps = GpsVehicle.search(params[:query])
    @total = GpsVehicle.where(:country_id => current_user.country.try(:id)).count
    @active_gps = Vehicle.vehicle_owners.fleet_gps_owners_country(current_user.country.try(:id)).count + Vehicle.vehicle_owners.fleet_admin_gps_owners_country(current_user.country.try(:id)).count
    @inactive_gps =@total -  @active_gps
    @active_fleet = User.busy_fleet_owners.individual_fleet_owners 
    @active_fleet_admin = User.busy_fleet_individual_owners.individual_fleet_owners 

    @active_fleets =   @active_fleet.select(:company_id)
    @active_fleet_admins =@active_fleet_admin.select(:company_id)

    @total = "Total GPS: " +@total.to_s
    @active_gps = "Busy GPS: "+ @active_gps.to_s
    @inactive_gps = "Free GPS:" +@inactive_gps.to_s

    
    respond_to do |format|
      format.html
      format.xlsx {
        render xlsx: 'xlsx_GPS', filename: "all_gps.xlsx", disposition: 'attachment'
      }
    end
  end
  def new
    @moderator_gps = GpsVehicle.new
  end
  def create
    set_gps_params
    @moderator_gps = GpsVehicle.new(gps_params)
    if @moderator_gps.save
      redirect_to moderator_gps_vehicle_index_path , notice: "GPS created successfully!"
    else
      render :new
    end
  end
  def edit
  end
  def update
    set_gps_params
    if @moderator_gps.update(gps_edit_params)
      redirect_to moderator_gps_vehicle_index_path , notice: "Country updated successfully!"
    else
      render :edit
    end
  end
  def destroy
    if @admin_country.destroy
      redirect_to admin_countries_path , notice: "Country destroyed successfully"
    else
      redirect_back(fallback_location: admin_countries_path)
    end
  end
  private
  def gps_params
     if params[:gps_vehicle][:vehicle_id].present?
     @user = User.find_by_id params[:gps_vehicle][:vehicle_id]
     @vehicle =@user.vehicle
     params[:gps_vehicle][:vehicle_id] =@vehicle.id
    else
      params[:gps_vehicle][:vehicle_id] = ''
     end

     params.require(:gps_vehicle).permit(:imei, :supplier,:manufacturer ,:country_id, :vehicle_id, :status)
  end
  def gps_edit_params

    if params[:gps_vehicle][:vehicle_id].present?
    @user = User.find_by_id params[:gps_vehicle][:vehicle_id]
    @vehicle =@user.vehicle
     params[:gps_vehicle][:vehicle_id] =@vehicle.id
    else
      params[:gps_vehicle][:vehicle_id] = ''
     end
    params.require(:gps_vehicle).permit(:vehicle_id, :status)
 end
  def set_country
    @countries = Country.all.where(:id => current_user.country.try(:id))
  end
  def set_gps_link
    @gps_link = "active"
  end
  

  def set_gps
    @moderator_gps = GpsVehicle.find(params[:id])
  end

  def set_gps_params
    id = params[:gps_vehicle][:id]
  end

  def set_vehicle_driver
    @users = User.individual_fleet_owners.where(:country_id => current_user.country.try(:id))
  end

end
