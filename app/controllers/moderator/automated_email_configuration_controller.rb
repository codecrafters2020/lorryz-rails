class Moderator::AutomatedEmailConfigurationController < ModeratorController
    before_action :authenticate_user!
    before_action :check_if_moderator
    before_action :set_automated_email , only: [ :new,:destroy]
    before_action :set_automated_edit_email , only: [ :edit,:update]

    layout 'moderator'
    def index
      @company_id = params[:company_id]
      @automated_email = AutomatedEmailConfiguration.where(company_id:@company_id)
     

    end
    def new
      @automated_email = AutomatedEmailConfiguration.new
    end
    def edit
    end
    def create
      @automated_email = AutomatedEmailConfiguration.new(automated_email_params)
      print automated_email_params
      if @automated_email.save
        redirect_to moderator_automated_email_configuration_index_path(company_id: params[:automated_email_configuration][:company_id]) , notice: "Instant Rate Created Successfully!"
      else
        format.html { render :new }
      end
    end
    def update
      if @automated_email.update(automated_email_params)
         redirect_to moderator_automated_email_configuration_index_path(company_id: @automated_email.company_id) , notice: " updated successfully!"
        # redirect_to admin_automated_email_configuration_index_path(params[:automated_email_configuration][:company_id]) , notice: "instant_rates Updated Successfully!"
      else
        format.html { render :edit}
      end
    end
    def destroy
      if @automated_email.destroy
        redirect_to admin_instant_rates_path, notice: "instant_rates destroyed successfully"
      else
        redirect_back(fallback_location: admin_instant_rates_path)
      end
    end
    private
    def set_automated_email
        @automated_email = AutomatedEmailConfiguration.where(company_id:params[:company_id])
        @company =params[:company_id]
      end

      def set_automated_edit_email
        @automated_email = AutomatedEmailConfiguration.find(params[:id])
        @company =@automated_email.company_id
      end
    def automated_email_params
      params.require(:automated_email_configuration).permit(:pickup_city, :pickup_country,:drop_country, :drop_city,:dropoff_location,:pickup_location, :email, :company_id)
    end
  
  end
  