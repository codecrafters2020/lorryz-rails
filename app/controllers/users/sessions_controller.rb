# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  prepend_before_action :require_no_authentication, only: [:new, :create]
  prepend_before_action :allow_params_authentication!, only: :create
  prepend_before_action :verify_signed_out_user, only: :destroy
  prepend_before_action(only: [:create, :destroy]) { request.env["devise.skip_timeout"] = true }

  include UserConcern
  # GET /resource/sign_in
  def new
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    yield resource if block_given?
    respond_with(resource, serialize_options(resource))
  end

  # POST /resource/sign_in
  def create
    # make_phone_number request.params     #devise use request.params for warden authentication
    request.params[:user][:login] = make_phone_number request.params[:user][:country_id] , request.params[:user][:login] unless email_authentication_web? params
    self.resource = warden.authenticate!(auth_options)


    if resource.try(:company).try(:blacklisted)
      signout_blacklisted
    elsif resource.role == "driver"
      signout_driver
    # elsif  (resource.role == "fleet_owner" && resource.company.company_type == "individual")
    #   signout_driver
    else
      if self.resource.mobile_verified? or resource.admin?
        set_flash_message!(:notice, :signed_in)
        sign_in(resource_name, resource)
        yield resource if block_given?
        if admin_or_superadmin?(resource)
          redirect_to admin_onlinedrivers_path
          return
        end
        if moderator?(resource)
          puts "\n\n\n\t\t moderator role is #{resource.role} \n\n"

          redirect_to moderator_onlinedrivers_path
          return
        end
        company = resource.company
        if company.present?
          if company.company_type == "individual"
            if company.profile.present? 
              redirect_to dashboard_index_path
            else
              redirect_to get_individual_information_companies_path
            end
          elsif company.company_type == "company"
            if company.profile.present? 
              redirect_to dashboard_index_path
            else
              redirect_to get_company_information_companies_path
            end
          end
        end
        # respond_with resource, location: after_sign_in_path_for(resource)
      else
        #warden.authenticate somehow sign in user. so needs to sign him out untill he verifies his code.
        signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
        redirect_to users_mobile_verification_path(unverified_user_id: resource.id)
      end
    end
  end

  # DELETE /resource/sign_out
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message! :notice, :signed_out if signed_out
    yield if block_given?
    respond_to_on_destroy
  end

  protected
  # def make_phone_number params
  #   params[:user][:login].sub!(/^0/, "")
  #   if !email_authentication_web?(params)
  #     dialing_code = Country.find_by_id(params[:user][:country_id]).try(:dialing_code)
  #     if dialing_code.present?
  #       params[:user][:login] = dialing_code + params[:user][:login].strip
  #     end
  #   end
  # end
  def sign_in_params
    devise_parameter_sanitizer.sanitize(:sign_in)
  end

  def serialize_options(resource)
    methods = resource_class.authentication_keys.dup
    methods = methods.keys if methods.is_a?(Hash)
    methods << :password if resource.respond_to?(:password)
    { methods: methods, only: [:password] }
  end

  def auth_options
    { scope: resource_name, recall: "#{controller_path}#new" }
  end

  def translation_scope
    'devise.sessions'
  end

  private

  # Check if there is no signed in user before doing the sign out.
  #
  # If there is no signed in user, it will set the flash message and redirect
  # to the after_sign_out path.
  def verify_signed_out_user
    if all_signed_out?
      set_flash_message! :notice, :already_signed_out

      respond_to_on_destroy
    end
  end

  def all_signed_out?
    users = Devise.mappings.keys.map { |s| warden.user(scope: s, run_callbacks: false) }

    users.all?(&:blank?)
  end

  def respond_to_on_destroy
    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to after_sign_out_path_for(resource_name) }
    end
  end
end
