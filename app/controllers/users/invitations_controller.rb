class Users::InvitationsController < DeviseController
  layout 'admin', :except => [:edit , :update]
  before_action :check_if_admin, only: [:new , :create]
  prepend_before_action :authenticate_inviter!, :only => [:new, :create]
  prepend_before_action :has_invitations_left?, :only => [:create]
  prepend_before_action :require_no_authentication, :only => [:edit, :update, :destroy]
  prepend_before_action :resource_from_invitation_token, :only => [:edit, :destroy]

  if respond_to? :helper_method
    helper_method :after_sign_in_path_for
  end

  include UserConcern
  # GET /resource/invitation/new
  def new
    @users_link= "active"
    self.resource = resource_class.new
    @company = resource.build_company
    render :new
  end

  # POST /resource/invitation
  def create
    @users_link= "active"
    self.resource = invite_resource
    resource_invited = resource.errors.empty?
    yield resource if block_given?

    if resource_invited
      if is_flashing_format? && self.resource.invitation_sent_at
        set_flash_message :notice, :send_instructions, :email => self.resource.email
      end
      if self.method(:after_invite_path_for).arity == 1
        redirect_to new_user_invitation_url
      else
        redirect_to new_user_invitation_url
      end
    else
      respond_with_navigational(resource) { render :new }
    end
  end

  # GET /resource/invitation/accept?invitation_token=abcdef
  def edit
    @company = resource.build_company
    set_minimum_password_length
    resource.invitation_token = params[:invitation_token]
    render :edit , layout: "application"
  end

  # PUT /resource/invitation
  def update
    raw_invitation_token = update_resource_params[:invitation_token]
    country_id = params[:user][:country_id]
    initial_mobile_number = params[:user][:mobile]
    params[:user][:mobile] = make_phone_number country_id , params[:user][:mobile]
    phone_number_valid = phone_number_valid?( country_id,  params[:user][:mobile] )
    self.resource = accept_resource
    set_resource_values resource , params[:user]
    if phone_number_valid
      yield resource if block_given?
      invitation_accepted = resource.errors.empty?
      if invitation_accepted
        if Devise.allow_insecure_sign_in_after_accept
          flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
          set_flash_message :notice, flash_message if is_flashing_format?
          redirect_to users_mobile_verification_path(unverified_user_id: resource)
        else
          set_flash_message :notice, :updated_not_active if is_flashing_format?
          respond_with resource, :location => new_session_path(resource_name)
        end
      else
        resource.invitation_token = raw_invitation_token
        respond_with_navigational(resource){ render :edit }
      end
    else
      # flash[:notice] = resource.errors.full_messages
      unless phone_number_valid
        params[:user][:mobile] = initial_mobile_number
        flash[:notice] = "Invalid Phone Number"
      end
      @company = Company.new
      render :edit
    end
  end

  # GET /resource/invitation/remove?invitation_token=abcdef
  def destroy
    resource.destroy
    set_flash_message :notice, :invitation_removed if is_flashing_format?
    redirect_to after_sign_out_path_for(resource_name)
  end

  protected

  def invite_resource(&block)
    resource_class.invite!(invite_params, current_inviter, &block)
  end

  def accept_resource
    resource_class.accept_invitation!(update_resource_params)
  end

  def current_inviter
    authenticate_inviter!
  end

  def has_invitations_left?
    unless current_inviter.nil? || current_inviter.has_invitations_left?
      self.resource = resource_class.new
      set_flash_message :alert, :no_invitations_remaining if is_flashing_format?
      respond_with_navigational(resource) { render :new }
    end
  end

  def resource_from_invitation_token
    unless params[:invitation_token] && self.resource = resource_class.find_by_invitation_token(params[:invitation_token], true)
      set_flash_message(:alert, :invitation_token_invalid) if is_flashing_format?
      redirect_to after_sign_out_path_for(resource_name)
    end
  end

  def invite_params
    devise_parameter_sanitizer.sanitize(:invite)
  end

  def update_resource_params
    devise_parameter_sanitizer.sanitize(:accept_invitation)
  end

  def translation_scope
    'devise.invitations'
  end

  def check_if_admin
    redirect_to root_path unless current_user and admin_or_superadmin?(current_user)
  end

  def set_resource_values resource , user_params
    resource.email = user_params[:email]
    resource.first_name = user_params[:first_name]
    resource.last_name= user_params[:last_name]
    resource.country_id = user_params[:country_id]
    resource.mobile = user_params[:mobile]
    resource.role = user_params[:role]
    resource.password = user_params[:password]
    if resource.save
      if params[:user][:company_attributes][:company_type] == "Individual"
        resource.register_as_individual
      elsif params[:user][:company_attributes][:company_type] == "Company"
        resource.register_as_company
      end
    end
  end
end
