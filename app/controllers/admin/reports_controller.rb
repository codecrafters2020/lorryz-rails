class Range
    def intersection(other)
     return nil if (self.max < other.begin or other.max < self.begin)
     [self.begin, other.begin].max..[self.max, other.max].min
    end
     alias_method :&, :intersection
end
class Admin::ReportsController < AdminController
    def get_online_report(start_date, end_date, user_id)

        sessions = SessionLog.where(user_id: params[:user_id],created_at: start_date..end_date).where.not(end_time: [nil])
        pk_tz_hrs = 5
        date_ranges = (start_date..end_date).to_a.map {|d| (d+(8-pk_tz_hrs)/24.0..d+(23-pk_tz_hrs)/24.0)}
        i=0
        j=0
        @heading = "Total Time"
        gr = {}
        while i < date_ranges.length && j < sessions.length do
          end_time = (sessions[j].end_time) ? (sessions[j].end_time) : DateTime.now
          #logger.info(end_time)
          intersection = (sessions[j].start_time..(end_time)) & date_ranges[i]
          #logger.info(intersection)
          if intersection
            old_val = gr[date_ranges[i].min.strftime("%Y-%m-%d")]
            old_val = old_val ? old_val : 0
            gr[date_ranges[i].min.strftime("%Y-%m-%d")] = old_val + (intersection.max.to_time-intersection.min.to_time)
            if intersection.max == date_ranges[i].max
              i = i + 1
            else
              j = j + 1
            end
          else
            if date_ranges[i].max <= sessions[j].start_time
              i = i + 1
            else
              j = j + 1
            end
          end
        end
        @report = gr
        show_value = lambda { |v|
            (v.floor/3600.floor).to_s + ' h ' + (v.floor%3600/60).to_s + ' m'
        }
        return {report: @report, heading: @heading, show_value: show_value}
    end

    def get_online_report_list(start_date, end_date, user_id)

        sessions = SessionLog.where(user_id: params[:user_id],created_at: start_date..end_date).where.not(end_time: [nil])
        pk_tz_hrs = 5
        date_ranges = (start_date..end_date).to_a.map {|d| (d+(8-pk_tz_hrs)/24.0..d+(23-pk_tz_hrs)/24.0)}
        i=0
        j=0
        @heading = "Time"
        gr = {}
        while i < date_ranges.length && j < sessions.length do
          end_time = (sessions[j].end_time) ? (sessions[j].end_time) : DateTime.now
          #logger.info(end_time)
          intersection = (sessions[j].start_time..(end_time)) & date_ranges[i]
          #logger.info(intersection)
          if intersection
            old_val = gr[date_ranges[i].min.strftime("%Y-%m-%d")]
            old_val = old_val ? old_val : []
            gr[date_ranges[i].min.strftime("%Y-%m-%d")] = old_val
            gr[date_ranges[i].min.strftime("%Y-%m-%d")].push(intersection)
            if intersection.max == date_ranges[i].max
              i = i + 1
            else
              j = j + 1
            end
          else
            if date_ranges[i].max <= sessions[j].start_time
              i = i + 1
            else
              j = j + 1
            end
          end
        end
        @report = gr

        return {report: @report, heading: @heading, show_value_template: "online_status"}
    end

    def bids_placed(start_date, end_date, user)
        company_id = user.company_id
        bids = Bid.where(company_id: company_id, created_at: start_date..end_date).group("date_trunc('day', bids.created_at)").count
        @heading = "Bids Placed"
        gr = {}
        (start_date..end_date).to_a.each { |date|
            gr[date.strftime("%Y-%m-%d")] = 0
        }

        bids.each{ |date, value|
            gr[date.strftime("%Y-%m-%d")] = value
        }

        @report = gr
        @start_date = start_date.strftime("%Y-%m-%d")
        @end_date = end_date.strftime("%Y-%m-%d")
        return {report: @report, heading: @heading}
    end

    def bids_won(start_date, end_date, user)
        company_id = user.company_id
        bids = Bid.where(company_id: company_id, status: :accepted, updated_at: start_date..end_date).group("date_trunc('day', bids.updated_at)").count
        @heading = "Won"
        gr = {}
        (start_date..end_date).to_a.each { |date|
            gr[date.strftime("%Y-%m-%d")] = 0
        }

        bids.each{ |date, value|
            gr[date.strftime("%Y-%m-%d")] = value
        }

        @report = gr
        @start_date = start_date.strftime("%Y-%m-%d")
        @end_date = end_date.strftime("%Y-%m-%d")
        return {report: @report, heading: @heading}
    end

    def shipments_completed(start_date, end_date, user)
        company_id = user.company_id
        bids = Shipment.unscoped.where(fleet_id: company_id, state: "completed", updated_at: start_date..end_date).group("date_trunc('day', shipments.updated_at)").count
        @heading = "Completed"
        gr = {}
        (start_date..end_date).to_a.each { |date|
            gr[date.strftime("%Y-%m-%d")] = 0
        }

        bids.each{ |date, value|
            gr[date.strftime("%Y-%m-%d")] = value
        }

        @report = gr
        @start_date = start_date.strftime("%Y-%m-%d")
        @end_date = end_date.strftime("%Y-%m-%d")
        return {report: @report, heading: @heading}
    end

    def shipments_recieved(start_date, end_date, user)
        company_id = user.company_id
        bids = ShortListedBidder.unscoped.where(company_id: company_id, created_at: start_date..end_date).group("date_trunc('day', created_at)").count
        @heading = "Recieved"
        gr = {}
        (start_date..end_date).to_a.each { |date|
            gr[date.strftime("%Y-%m-%d")] = 0
        }

        bids.each{ |date, value|
            gr[date.strftime("%Y-%m-%d")] = value
        }

        @report = gr
        @start_date = start_date.strftime("%Y-%m-%d")
        @end_date = end_date.strftime("%Y-%m-%d")
        return {report: @report, heading: @heading}
    end


    def index
        start_date = params[:start_date].present? ? params[:start_date].to_datetime : Date.today.at_beginning_of_month.to_datetime
        end_date = params[:end_date].present? ? params[:end_date].to_datetime : Date.today.to_datetime
        end_date = end_date + 1
        user_id = params[:user_id]
        @dates = []
        @user_id = user_id
        @reports = []
        if(@user_id)
            begin
            user = User.find(@user_id)
            rescue ActiveRecord::RecordNotFound
            end
            if(user)
              @dates = (start_date..(end_date - 1)).to_a.map{|d| d.strftime("%Y-%m-%d")}
              @reports = [
                  get_online_report(start_date,end_date,user_id),
                  get_online_report_list(start_date,end_date,user_id),
                  bids_placed(start_date,end_date,user),
                  bids_won(start_date,end_date,user),
                  shipments_completed(start_date,end_date,user),
                  shipments_recieved(start_date,end_date,user),
              ]
            end
        end
        @start_date = start_date.strftime("%Y-%m-%d")
        @end_date = (end_date - 1).strftime("%Y-%m-%d")
        respond_to do |format|
            format.html { render :index }
        end
    end
end
