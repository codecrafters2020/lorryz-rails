class Admin::AdminNotificationController < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin
  before_action :set_notification , only: [:edit, :update,:destroy]
  before_action :set_notification_link , only: [:edit, :update , :new , :index ]
  layout 'admin'
  def index
    @notifcation = AdminNotification.all
    respond_to do |format|
      format.html
    
    end
  end
  def new
    @notification = AdminNotification.new
  end
  def edit
  end
  def create
    @notification = AdminNotification.new(notification_params)
    if @notification.save
      receivers = User.all
      receivers = receivers.where(:role => @notification.receivers )  if !@notification.receivers.all?    
        receivers.each do |receiver|
        # dispathcer = SendSMS.new
        # message = dispathcer.message(user.mobile,"Reminder! Don't forget to pick shipment # #{ self.id } on #{self.pickup_time}")
        Notification.create(
            notifiable_id: @notification.id,
            notifiable_type: "AdminNotification",
            body: @notification.text,
            user_mentioned_id: receiver.id)
        PushNotification.send_notification(
          receiver.os,
            receiver.device_id,
            "",
            @notification.id,
            @notification.text,
            nil,
            ) if receiver.device_id
      end









      
      redirect_to admin_admin_notification_index_path , notice: "Notification Created Successfully!"
    else
      format.html { render :new }
    end
  end
 
  private
  def set_notification_type
    @notification = AdminNotification.find(params[:id])
  end
  def notification_params
    params.require(:admin_notification).permit(:text,:receivers)
  end
  def set_notification_link
    @notification_link = "active"
  end



end
