class Admin::PrimaryVehicleController < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin
  before_action :set_country , only: [:edit, :create,:index , :new,  :update, :destroy]
  before_action :set_pvehicles, only: [:edit,  :update, :destroy]

  before_action :set_pvehicles_link ,only: [:edit,:update,  :index , :new]
  layout 'admin'
  def index
    params[:query] ||=   {}
  
    @pvehicles = PrimaryVehicle.all
   
  end
  def new
    @pvehicles = PrimaryVehicle.new
  end

  def create
    set_pvehicles_params
    set_attachment_params
    @pvehicles = PrimaryVehicle.new(pvehicles_params)
    if @pvehicles.save
      redirect_to admin_primary_vehicle_index_path , notice: "Primary vehiclescreated successfully!"
    else
      render :new
    end
  end
  def edit
  end
  def update
    set_pvehicles_params
    set_attachment_params
    if @pvehicles.update(pvehicles_params)
      redirect_to admin_primary_vehicle_index_path , notice: "Country updated successfully!"
    else
      render :edit
    end
  end
 
  private
  def pvehicles_params
   
     params.require(:primary_vehicle).permit(:name ,:country_id,image_url: [])
  end
  def pvehicles_edit_params
    
      params.require(:primary_vehicle).permit(:name ,:country_id,image_url: [])
    end
  def set_country
    @countries = Country.all
  end
  def set_pvehicles_link
    @pvehicles_link = "active"
  end
  

  def set_pvehicles
    @pvehicles = PrimaryVehicle.find(params[:id])
  end

  def set_pvehicles_params
    id = params[:primary_vehicle][:id]
  end

  def set_attachment_params

  


    if params[:primary_vehicle][:image_url].present?

      begin
        params[:primary_vehicle][:image_url]= eval(params[:primary_vehicle][:image_url].first) 
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end  end
      
    end
      
    

  

end
