class Admin::EnquiriesController < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin
  before_action :set_enquries_link ,only: [:edit,  :index , :new]

  layout 'admin'
  def index
    @enquiries = Shipmentestimation.all
    if params[:query].present?

    @enquiries = @enquiries.where(amount: [nil, ""]) if params[:query][:category] == 'Unique'
    @enquiries = @enquiries.where.not(amount: [nil, ""])if params[:query][:category] == 'Existing'
    end

    respond_to do |format|
      format.html
    
    end
   
end

def convert_to_shipment_country_timezone country, datetime
  tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
  return datetime.in_time_zone(tz) rescue (return datetime)
end
def set_enquries_link
  @enquries_link = "active"
end
end
