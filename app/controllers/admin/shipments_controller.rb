class Admin::ShipmentsController < AdminController
  layout 'admin'
 

  before_action :set_shipment , only: [:show ,:update,:get_coordinates]
  before_action :set_shipment_link
  before_action :set_vehicle_type, only: [:edit , :new, :create , :update]
  before_action :set_vendor, only: [:edit , :new, :create , :update,:assign_vehicle]

  before_action :set_company_type, only: [:edit , :new, :create , :update]
  before_action :get_shipment , only: [:quit_bid, :show,:cancel, :assign_cost, :assign_vehicle,:update, :update_destination, :get_coordinates ,:send_completed_email]
  before_action :set_individual_type, only: [:edit , :new, :create , :update]
  before_action :set_pickup_time, only: [:create, :update]
  skip_before_action :verify_authenticity_token, :only => [:status_email]

  include ShipmentConcern
  
   require "aws-sdk"
  require 's3_presigner'
  def presign_upload

    render json: UploadPresigner.presign("/users/avatar/", params[:filename], limit: 1.megabyte)
  end

  def index
    params[:query] ||=   {}
  
    if params[:query].present?
      
    @shipments = Shipment.unscoped { Shipment.search(params[:query]).paginate(:page=>params[:page],per_page:100).order(id: :desc)}

    else
  
    @shipments = Shipment.unscoped {  Shipment.paginate(:page=>params[:page],per_page:100).order(id: :desc)}
    end
    respond_to do |format|
      format.html
     format.xlsx {
            start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
            end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
            @shipments  = Shipment.where(pickup_date: start_date..end_date) #if params[:start_date].present? and params[:end_date].present?
            render xlsx: 'admin/index', filename: "all_shipments.xlsx", disposition: 'attachment', stream: true
         }
    end
  end


  def new
    instance_variable_set :"@#{__method__}" , "active"
    @title = "Upcoming"
    @shipment = Shipment.new
    @individual = true
    shipment = cookies[:shipment] ? eval(cookies[:shipment]) : nil

    @shipment.no_of_vehicles =1;
    if shipment

      shipment = eval(cookies[:shipment])
      @shipment = Shipment.new(pickup_location: shipment["pickup_location"],drop_location: shipment["drop_location"],
                              vehicle_type_id: shipment["vehicle_type_id"],no_of_vehicles: shipment["no_of_vehicles"],pickup_building_name: shipment["pickup_building_name"],drop_building_name: shipment["drop_building_name"], is_pickup_now: @individual)
      cookies[:shipment] = nil
    end
  end



  def get_shipment
    # shipment_country_access_control params[:id]
    @shipment = Shipment.find_by_id params[:id]
  end

  def get_coordinates
    respond_to do |format|
      if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").present? &&   ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Offloading Point").present?

        start_time = {"date"=>  @shipment.shipment_action_dates.where(state: "Enroute to Destination").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  @shipment.shipment_action_dates.where(state: "Enroute to Destination").try(:last).try(:performed_at_time).strftime("%H:%M") }
        start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
        complete_time = {"date"=>  @shipment.shipment_action_dates.where(state: "At Offloading Point").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  @shipment.shipment_action_dates.where(state: "At Offloading Point").try(:last).try(:performed_at_time).strftime("%H:%M") }
        complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
             
     durations = complete_time_csv.to_time  - start_time_csv.to_time
     
        seconds = (durations % 60)
        minutes = (durations / 60) % 60
        hours = (durations / (60 * 60))
        days = (hours) / 24

        hours= hours % 24
        @durations = format("%02d days %02d hours %02d minutes", days,hours, minutes)

      end
      format.html
     format.xlsx {
      Aws.config.update({
        region: "ap-south-1",
        credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
      })
      dynamodb = Aws::DynamoDB::Client.new
      table_name = 'vehicle_coordinates'
  
      zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")
  
  
  
      if  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).present?  && @shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at_time).present?
        start_time = {"date"=>  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).strftime("%H:%M") }
        start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
        tz = ISO3166::Country.new(@shipment.country.short_name).timezones.zone_identifiers.first
        start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
        start_timestamp = start_time_csv.utc
        complete_time = {"date"=>  @shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  @shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at_time).strftime("%H:%M") }
        complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
        complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
        complete_timestamp = complete_time_csv.utc
        vehicle_id  = @shipment.shipment_fleets.last.company.vehicles.first.try(:id) if @shipment.shipment_fleets.present? && @shipment.shipment_vehicles.blank?
        vehicle_id = @shipment.shipment_vehicles.first.try(:vehicle_id)  if @shipment.shipment_vehicles.present?
        @vehicle = Vehicle.find_by_id(vehicle_id)  if vehicle_id.present?
        @start_time_csv = start_time_csv
        @complete_time_csv = complete_time_csv
      
        params1 = {
  
          table_name: table_name,
          key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
          expression_attribute_names: {
              "#created" => "created",
              "#vehicle_id" => "vehicle_id"
          },
          expression_attribute_values: {
            ":vehicle_id" => vehicle_id,
            ":start_time" =>   start_timestamp.to_s,
            ":end_time" => complete_timestamp.to_s
          }
        }
        @resp =[]
        if start_timestamp < complete_timestamp
  
         @resp = dynamodb.query(params1)
          end

         
     
  if @resp.count > 1
    url =  "https://maps.googleapis.com/maps/api/geocode/json?latlng=#{@resp.items.first['lat']},#{@resp.items.first['lng']}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
            
        response = HTTParty.get(url)
        
        @start_location=response['results'].last['formatted_address']
  
  
    url =  "https://maps.googleapis.com/maps/api/geocode/json?latlng=#{@resp.items.last['lat']},#{@resp.items.last['lng']}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
            
    response = HTTParty.get(url)
    
    @end_location=response['results'].last['formatted_address']


  end

  render xlsx: 'admin/shipments/index_shipment', filename: "Shipment_Report.xlsx", disposition: 'attachment', stream: true

   
end
           
    }
  end

end

  def create
    set_attachment_params
     @shipment = Shipment.new(cargo_shipment_params)
     set_contract_fields if cargo_shipment_params[:location_id].present?
        
     if @shipment.cancel_by.present?
        @shipment.company_id =@shipment.cancel_by
        @shipment.cancel_by=nil
        @shipment.payment_option="credit_period"
        @shipment.is_pickup_now = false
      else
         @shipment.payment_option="before_pick_up"
         @shipment.is_pickup_now = true
      end
     

    
 
     @shipment.unloading_time = @shipment.loading_time;


    if !cargo_shipment_params[:pickup_building_name].present?
      @shipment.pickup_building_name = "not specified"
    end


    if params[:shipment][:drop_location].blank?
      @shipment.is_fixed = false;
    else
      @shipment.is_fixed = true;

        end
      @shipment.customer_net_amount =  @shipment.amount
      @shipment.is_createdby_admin = true;
      @shipment.fleet_id = User.find_by_id(current_user.id).company_id;
      @shipment.country_id=@shipment.try(:vehicle_type).country.try(:id)
      @shipment.amount =   @shipment.amount  *  @shipment.no_of_vehicles 
      @shipment.weight =   @shipment.weight  *  @shipment.no_of_vehicles 
      
      
      
      
 
      if @shipment.company_id.blank?
        flash[:error] ="Customer cannot be blank."

        redirect_to new_admin_shipment_path


      else
    respond_to do |format|

      if @shipment.save
         format.html { redirect_to admin_shipments_path, notice: 'Shipment was successfully created.' }
        else
        flash[:error] = @shipment.errors.full_messages
        format.html { render :new }
      end
    end
  end
    
  end

  def send_completed_email
    ShipmentMailer.send_automated_email( @shipment.id).deliver_now 
   redirect_to admin_shipment_path(@shipment.id), notice: 'Email  successfully sent.' 

  end
  def assign_vehicle


#     lat = @shipment.pickup_lat
# 		lon =  @shipment.pickup_lng

#     	url = "https://maps.googleapis.com/maps/api/geocode/json?units=imperial&latlng=#{lat.to_f},#{lon.to_f}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
#     response = HTTParty.get(url)
    
#     if  response['results'].first['address_components'].present?
#     address = response['results'].first['address_components']
#     address.each do |addr|

#       if addr['types'].first == 'country'
#         @pickup_country = addr['long_name']
#       end
#       if addr['types'].first == 'locality'
#         @pickup_city= addr['long_name']
#       end
      



#     end
#   end

#   lat = @shipment.drop_lat
#   lon =  @shipment.drop_lng

#     url = "https://maps.googleapis.com/maps/api/geocode/json?units=imperial&latlng=#{lat.to_f},#{lon.to_f}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
#   response = HTTParty.get(url)
  
#   if  response['results'].first['address_components'].present?
#   address = response['results'].first['address_components']
#   address.each do |addr|

#     if addr['types'].first == 'country'
#       @drop_country = addr['long_name']
#     end
#     if addr['types'].first == 'locality'
#       @drop_city= addr['long_name']
#     end
    



#   end

# end



#   puts "\n\n\n\t\t pickup_city is  #{@pickup_city} \n\n"
#   puts "\n\n\n\t\t pickup_country is  #{@pickup_country} \n\n"

#   puts "\n\n\n\t\t pickup_city is  #{@pickup_city} \n\n"
#   puts "\n\n\n\t\t pickup_country is  #{@pickup_country} \n\n"

    # Shipment.all.order(:id).each do |ship|

    #     if ship.category == 'Inter_City'
    #   ship.populate_category
    #        puts "\n\n\n\t\t colors is  #{ship.id} \n\n"
    #     end
    # end
  #     puts "\n\n\n\t\t colors is  #{ship.id} \n\n"


  #     lat = ship.pickup_lat
	# 	lon = ship.pickup_lng
	# 	lat_lon = "#{lat},#{lon}"
	# 	response = Geocoder.search(lat_lon).first

	# 	lat1 = ship.drop_lat
	# 	lon1 = ship.drop_lng
	# 	lat_lon1 = "#{lat1},#{lon1}"
	# 	response1 = Geocoder.search(lat_lon1).first


	# 	if (response.country == response1.country )
	# 		if (response.city == response1.city )
  #       ship.update_attributes(
	# 				category: "Intra_City"
	# 		)
	# 		elsif  (response.city != response1.city )
	# 			ship.update_attributes(
	# 				category: "Inter_City"
	# 	         	)
	# 		end
	# 	elsif (response.country != response1.country)
	# 		ship.update_attributes(
	# 			category: "Cross_Border"
	# 	)
  #   end
  # end

  # 	url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{ship.pickup_lat.to_f},#{ship.pickup_lng.to_f}&destinations=#{ship.drop_lat.to_f},#{ship.drop_lng.to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
	# 	response = HTTParty.get(url)
	# 	distance = response["rows"].first["elements"].first["distance"].blank? ? 0 : (response["rows"].first["elements"].first["distance"]["value"].to_f / 1000).round(2)

  #   ship.update_attributes(distance_between_endpoints:distance)
  # end
  case @shipment.state 
  when "vehicle_assigned","accepted","ongoing"  
@fleets = User.individual_fleet_owners.where(:country_id => @shipment.try(:vehicle_type).country.try(:id)).filter_by_nearest(@shipment.pickup_lat,@shipment.pickup_lng)  - User.individual_fleet_owners.busy_fleet_owners - User.individual_fleet_owners.busy_fleet_individual_owners  + User.shipment_fleet_individual_owners(@shipment.id)
when "initial","posted", "completed","cancel" 
  @fleets = User.individual_fleet_owners.where(:country_id => @shipment.try(:vehicle_type).country.try(:id)).filter_by_nearest(@shipment.pickup_lat,@shipment.pickup_lng)  - User.individual_fleet_owners.busy_fleet_owners - User.individual_fleet_owners.busy_fleet_individual_owners  + User.shipment_fleet_individual_owners(@shipment.id)
end

@fleets.each { |fleet| fleet.pickup_lat = @shipment.pickup_lat , fleet.pickup_lng = @shipment.pickup_lng}



  end

  # def assign_cost

  #   url = "http://103.9.23.45/TrakkerServices/Api/Home/GetVRNLastLocation/SameerUserID1/SameerPassword1/03353509134"
  #   response = HTTParty.get(url)
    

  #  end


   def assign_cost

    @additional_cost = 0 ;
    @additional_cost = @shipment.additional_rate  + @additional_cost if @shipment.additional_rate.present?
    @additional_cost =  @shipment.additional_rate1 + @additional_cost if @shipment.additional_rate1.present?
    @additional_cost =  @shipment.additional_rate2 + @additional_cost if @shipment.additional_rate2.present?
    @additional_cost =  @shipment.additional_rate3 + @additional_cost if @shipment.additional_rate3.present?
    @additional_cost =  @shipment.additional_rate4 + @additional_cost if @shipment.additional_rate4.present?
    @additional_cost =  @shipment.additional_rate5 +  @additional_cost if @shipment.additional_rate5.present?
    @additional_cost =  @shipment.additional_rate6 + @additional_cost if @shipment.additional_rate6.present?
  
  # @ss =  User.expired_fleet_users(Date.today()).individual_fleet_owners

  # @ss.each do |charge1| 

  # ShipmentMailer.expired_cargo_company.deliver_now
  # ShipmentMailer.expired_cargo_individual.deliver_now
  # ShipmentMailer.expired_fleet_company.deliver_now
  # ShipmentMailer.expired_fleet_individual.deliver_now

  # end
   @shipment_fleets = ShipmentFleet.all.where(shipment_id: @shipment.id ).order(:id)
   i = 1;
  #  @shipment.payable=  @shipment.fleet_net_rate if  @shipment.fleet_net_rate.present?
  @shipment.payable =0;
  
  @shipment.payable =  @shipment.advance_amount.to_i  if @shipment.advance_amount.present?
   @shipment.payable=@shipment.fleet_net_rate -  @shipment.payable if @shipment.fleet_net_rate.present?
      @shipment_fleets.each do |charge| 

    
    if i == 1
      @fleet_rate1 = charge.fleet_cost
      
    end
    if i == 2
      @fleet_rate2 = charge.fleet_cost
      

    end
    if i == 3
      @fleet_rate3 = charge.fleet_cost
     

    end
    if i == 4
      @fleet_rate4 = charge.fleet_cost
          end
    if i ==5
      @fleet_rate5 = charge.fleet_cost
        end
    if i == 6
      @fleet_rate6 = charge.fleet_cost
       end
    if i == 7
      @fleet_rate7 = charge.fleet_cost
       end
    if i == 8
      @fleet_rate8 = charge.fleet_cost
       end
    if i == 9
      @fleet_rate9 = charge.fleet_cost
        end
    if i == 10
      @fleet_rate10 = charge.fleet_cost
        end
    if i == 11
      @fleet_rate11 = charge.fleet_cost
        end
    if i == 12
      @fleet_rate12 = charge.fleet_cost
     end
    if i == 13
      @fleet_rate13 = charge.fleet_cost
      end
    if i == 14
      @fleet_rate14 = charge.fleet_cost
       end
    if i == 15
      @fleet_rate15 = charge.fleet_cost
      end
    if i == 16
      @fleet_rate16 = charge.fleet_cost
      end
    if i == 17
      @fleet_rate17 = charge.fleet_cost
         end
    if i == 18
      @fleet_rate18 = charge.fleet_cost
     end
    if i == 19
      @fleet_rate19 = charge.fleet_cost
       end
    if i == 20
      @fleet_rate20 = charge.fleet_cost
       end
    if i == 21
      @fleet_rate21 = charge.fleet_cost
    end
    if i ==22
      @fleet_rate22 = charge.fleet_cost
     end
    if i == 23
      @fleet_rate = charge.fleet_cost
     
    end
    if i == 24
      @fleet_rate24 = charge.fleet_cost
     
    end
    if i == 25
      @fleet_rate25= charge.fleet_cost
    
    end
   
   i+=1;
  end

    respond_to do |format|
      format.js
    end
  end



  def set_cargo_shipment
    shipment_country_access_control(params[:id])
    @shipment = Shipment.find(params[:id])
  end

  def cargo_shipment_params
   params.require(:shipment).permit( :cargo_value,:cargo_description_url,:reverse_trip,:balance_paid_date,:category, :invoice_comments,:VAT_applicable, :status,:weight,:vendor,:cancel_reason, :cancel_by,:amount_per_vehicle ,:state_event, :state, :id,:company_id, :country_id, :pickup_location, :pickup_date,:fleet_net_rate, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :payment_option, :pickup_building_name , :drop_building_name,:accepted_rate_currency,:amount,:is_createdby_admin,:balance_paid,:balance_paid_date)
  end

  def dropoff_location
    end

  def image_select
     
    @vehicle =""
    @pickup_time = ""
    @pickup_building_name = ""
    @drop_building_name = ""
    @pickup_date = ""
    @loading_time = ""
    @unloading_time = ""
    @vehicle_type_id = ""
    @no_of_vehicles = ""
    @cargo_description =""
    @cargo_packing_type = ""
    @payment_option = ""
    @address =""
    @drop_address =""
    @latitude = ""
    @longitude = ""
    @dropoff_lat= ""
    @dropoff_long= ""
    @city= ""
    @dcity=""
  
   
    @vehicle =params[:vehicle_id]
    @pickup_time = params[:pickup_time]
    @pickup_building_name = params[:pickup_building_name]
    @drop_building_name = params[:drop_building_name]
    @pickup_date = params[:pickup_date]
    @loading_time = params[:loading_time]
    @unloading_time = params[:unloading_time]
    @vehicle_type_id = params[:vehicle_type_id]
    @no_of_vehicles = params[:no_of_vehicles]
    @cargo_description = params[:cargo_description]
    @cargo_packing_type = params[:cargo_packing_type]
    @payment_option = params[:payment_option]
    @address =params[:address]
    @drop_address =params[:drop_address]
    @latitude = params[:latitude]
    @longitude = params[:longitude]
    @dropoff_lat= params[:dropoff_lat]
    @dropoff_long= params[:dropoff_long]
    @city= params[:city]
    @dcity= params[:dcity]
  
  
    redirect_to new_cargo_shipment_path(:vehicle=>@vehicle ,:pickup_building_name => @pickup_building_name,:drop_building_name => @drop_building_name,:pickup_date => @pickup_date,:loading_time => @loading_time,:unloading_time => @unloading_time,:no_of_vehicles => @no_of_vehicles,:cargo_description => @cargo_description,:cargo_packing_type => @cargo_packing_type,:payment_option => @payment_option,:address => @address,:drop_address => @drop_address,:latitude => @latitude,:longitude => @longitude,:dropoff_lat => @dropoff_lat,:dropoff_long => @dropoff_long,:city => @city,:dcity => @dcity)
     end


  def pickup_location
  end
  
  def format_date_fields
    params[:shipment][:pickup_date] = Date.strptime(params[:shipment][:pickup_date], "%d/%m/%Y")
  end

  def set_pickup_time
    if params[:shipment][:vehicle_type_id].present?
    @vehicle = VehicleType.find_by_id(params[:shipment][:vehicle_type_id])
    @country = Country.find_by_id(@vehicle.country)

    params[:shipment][:pickup_time] = set_time_zone_admin(params[:shipment][:pickup_time],@country) if params[:shipment][:pickup_time].present?
    end
  end

  def set_vehicle_type
    @vehicle_types = VehicleType.all.order(code: :desc)
  end

  def set_vendor
    @vendors = User.vendor.order(:country_id)
  end

  def set_company_type
    @company_types = CompanyInformation.cargo_company.where.not(name: [""]).order(:name)
  end

  def set_individual_type   
    @individual_types = User.individual_cargo_owners.order(:first_name)
  end

  def edit
    @shipment = Shipment.find params[:id]
    @vehicle_types = VehicleType.all.order(code: :desc)
  end


  def edit_history
    @activities = Audit.all.order(created_at: :DESC).where(auditable_id:  params[:id] ,auditable_type: "Shipment")

  end

  def email
    @shipment = Shipment.find params[:id]
    @vehicle_types = VehicleType.all.order(code: :desc)

   
  end

  def get_bulk_coordinates
    @shipments = Shipment.includes(:vehicle_type,:shipment_vehicles).where(shipment_vehicles: {status: ShipmentVehicle::ONGOING_VEHICLES })
    render json: { coordinates:  get_shipment_bulk_coordinates(@shipments) }, status: :ok
  end


  def status_email

    
    @to = params[:shipment][:to]
    @cc = params[:shipment][:cc]
    @message = params[:shipment][:message]
    @subject = params[:shipment][:subject]
    @previous_status = params[:shipment][:previous_status]
    @current_status =params[:shipment][:current_status]
    @id = params[:shipment][:id]

   @shipment= Shipment.find_by_id(@id)
    ShipmentMailer.status_change(@to,@cc,@message,@subject,@previous_status,@current_status,@id).deliver_now
    @shipment.update_attributes(status: params["shipment"]["current_status"])


    redirect_to edit_admin_shipment_path(@shipment)

  
  end

  def cancel
    Shipment.find(params[:id]).update_attributes!(state_event: :cancel , cancel_by: current_user.role,status: "cancel")
    redirect_to admin_shipments_path
    flash[:error]= "Shipment was canceled successfully"
  end
    def update


      set_attachment_params
     
    respond_to do |format|
      

      if params["shipment"]["status"].present?
        if params["shipment"]["status"] == "completed" || params["shipment"]["status"] == "At Loading"
  
              shipment =@shipment
  
         
          Aws.config.update({
            region: "ap-south-1",
            credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
            })
            dynamodb = Aws::DynamoDB::Client.new
            table_name = 'vehicle_coordinates'
          
            zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")
          
          
          
            if  shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).present? && shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).present?  && shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at).present? && shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at_time).present?
            start_time = {"date"=>  shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).strftime("%H:%M") }
            start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
            tz = ISO3166::Country.new(shipment.country.short_name).timezones.zone_identifiers.first
            start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
            start_timestamp = start_time_csv.utc
            complete_time = {"date"=>  shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  shipment.shipment_action_dates.where(state: "completed").try(:last).try(:performed_at_time).strftime("%H:%M") }
            complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
            complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
            complete_timestamp = complete_time_csv.utc
            vehicle_id  = shipment.shipment_fleets.last.company.vehicles.first.try(:id) if shipment.shipment_fleets.present? && shipment.shipment_vehicles.blank?
                vehicle_id = shipment.shipment_vehicles.first.try(:vehicle_id)  if shipment.shipment_vehicles.present?
                 
            params1 = {
          
              table_name: table_name,
              key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
              expression_attribute_names: {
                "#created" => "created",
                "#vehicle_id" => "vehicle_id"
              },
              expression_attribute_values: {
              ":vehicle_id" => vehicle_id,
              ":start_time" =>   start_timestamp.to_s,
              ":end_time" => complete_timestamp.to_s
              }
            }
            if start_timestamp < complete_timestamp && vehicle_id.present?

             @resp = dynamodb.query(params1) 
            end
              
            coords1 =[]
            snappedCoordinates=[]
            position=[]
            if @resp.present?
            if @resp.items.present?
              
              str = ""
           
                  @distance = 0
                  i =0 ;    j =100;
                  @resp.items.each do |user|
      
                  @distance = @distance + Geocoder::Calculations.distance_between([@temp_lat , @temp_lng],[user["lat"],user["lng"]],:units =>:km)  if   @temp_lat.present?
                  @temp_lat = user["lat"]
                  @temp_lng= user["lng"]
      
                    if i < j+100 || i != @resp.items.count
                      str = str + user["lat"].to_s + "," + user["lng"].to_s 
                          if i != j-1  && i != @resp.items.count-1
                            str = str+"|"
                           end
                       i +=1 
                      end
                    if i == j  || i== @resp.items.count
                      url ="https://roads.googleapis.com/v1/snapToRoads?path=#{str}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"            
                response = HTTParty.get(url)
           
                        if response["snappedPoints"].present?
                          response["snappedPoints"].each do |snappoint|
                             coords1.push({lat:  snappoint['location']['latitude']  , lng:  snappoint['location']['longitude']  });
                              end
          
                          end
                       str=""
                       j= i + 100
                     end
              end

              puts "\n\n\n\t\t @resp.items.count > 25 #{@resp.items.count > 25} \n\n"


              if @resp.items.count > 25
              
              distance_between_snappedpoints = coords1.count / 25
              distance_between_snappedpoints = distance_between_snappedpoints.to_i
           
              itr = 0 
              i =0 ;
              while itr < 25  do
               
             
               position.push({ lat:coords1[i][:lat]  , lng:  coords1[i][:lng] })
               i = i+distance_between_snappedpoints
                itr +=1
           
               end
              else
                itr1 =0 
                i =0 
                while itr1 <  @resp.items.count  do
               
             
                  position.push({ lat:coords1[i][:lat]  , lng:  coords1[i][:lng] })
                   itr1 +=1
              
                  end

              end


         
            
            end
          end
        end
        
        @shipment.update_attributes(
          # distance_between_endpoints:@distance,
          routes:position)
        
        end
      end
  
  

      @expired_fleets = 0;
      if params["shipment"]["vendor"].present?
        @shipment.update_attributes(vendor: params["shipment"]["vendor"][1])
      end
      if params["shipment"]["company_idss"].present?
      if params["shipment"]["company_idss"].count > 1
        params["shipment"]["company_ids"] = params["shipment"]["company_idss"]
      end
    end


      if params["shipment"]["company_ids"].present?

      i = 0;
              ShipmentVehicle.where(shipment_id: @shipment.id).destroy_all

      params[:shipment][:company_ids].each do |company|
        if i == 1 
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
            if(@expired.length == 0)

              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_1] )
             ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
                if params["shipment"]["customer_net_amount"]
                    #assign cost part
                else
                  #assign driver notification
                  PushNotification.send_notification(
                    @fleet.os,
                    @fleet.device_id,
                    "",
                    "",
                    "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                    nil,
                   ) if @fleet.device_id &&  @fleet.os!= "ios"
      
                   Notification.create(notifiable_id: @fleet.id, 
                     notifiable_type: "Shipment", 
                     body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                     user_mentioned_id: @fleet.id)  
                end
                
            else
              flash[:error] = " Fleet is expired cannot assign"
              format.html { redirect_to admin_shipments_path, notice: 'Shipment was successfully updated.' }

            end

          end
          if i == 2
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
             @expired =  User.filter_by_expired_fleet(company,Date.today())
             @fleet = User.find_by_company_id(company)

             @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_2] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
                  if params["shipment"]["customer_net_amount"]
                    #assign cost part
                else
                  #assign driver notification
                  PushNotification.send_notification(
                    @fleet.os,
                    @fleet.device_id,
                    "",
                    "",
                    "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                    nil,
                    ) if @fleet.device_id &&  @fleet.os!= "ios"

                    Notification.create(notifiable_id: @fleet.id, 
                      notifiable_type: "Shipment", 
                      body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                      user_mentioned_id: @fleet.id)
                    
          
                  
                end
      
          else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 3
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_3] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 4
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_4] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i ==5
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_5] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 6
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_6] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
              else
                #assign driver notification
                PushNotification.send_notification(
                  @fleet.os,
                  @fleet.device_id,
                  "",
                  "",
                  "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                  nil,
                  ) if @fleet.device_id &&  @fleet.os!= "ios"

                  Notification.create(notifiable_id: @fleet.id, 
                    notifiable_type: "Shipment", 
                    body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                    user_mentioned_id: @fleet.id)  
              end
            
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 7
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_7] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 8
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_8] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 9
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_9] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 10
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_10] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 11
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_11] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 12
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_12] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 13
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_13] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 14
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_14] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 15
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_15] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 16
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_16] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 17
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_17] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
               
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 18
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_18] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 19
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_19] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 20
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_20] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 21
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)
 
            @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_21] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i ==22
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

             @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_22] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 23
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_23] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 24
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_24] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
         
      
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
          if i == 25
            @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
            @fleet = User.find_by_company_id(company)

            @expired =  User.filter_by_expired_fleet(company,Date.today()) 
            @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
            if(@expired.length == 0)
              ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_25] )
              ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

              if params["shipment"]["customer_net_amount"]
                #assign cost part
            else
              #assign driver notification
              PushNotification.send_notification(
                @fleet.os,
                @fleet.device_id,
                "",
                "",
                "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
                nil,
               ) if @fleet.device_id &&  @fleet.os!= "ios"
  
               Notification.create(notifiable_id: @fleet.id, 
                 notifiable_type: "Shipment", 
                 body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
                 user_mentioned_id: @fleet.id)
                
      
              
            end
        


              
            else
            flash[:error] = " Fleet is expired cannot assign"
          end
          end
         
         i+=1;
        end
      end
      if params[:shipment][:pickup_date].present?
      if (@shipment.pickup_date.to_date !=  params[:shipment][:pickup_date].to_date)
      params[:shipment][:created_at] = params[:shipment][:pickup_date]
      end 
     end 

     if(@expired_fleets == 0) 
 
      if @shipment.update!(shipment_params)
        if ShipmentVehicle.where(shipment_id: @shipment.id ).present?
        ShipmentVehicle.where(shipment_id: @shipment.id ).update_all(status: 1 ) if ShipmentVehicle.where(shipment_id: @shipment.id ).last.status.blank? 
        end
        if @shipment.state == "completed"
          if  params[:balance_paid]
            @shipment.update_attributes(balance_paid:  params[:balance_paid], balance_paid_date:  Date.strptime(params[:shipment][:balance_paid_date],'%d/%m/%Y') ,paid_to_fleet:  true )
          else
           @shipment.update_attributes(balance_paid:  params[:balance_paid], balance_paid_date:  nil ,paid_to_fleet: true  )
          end
        else
          if  params[:balance_paid]
            @shipment.update_attributes(balance_paid:  params[:balance_paid], balance_paid_date:  Date.strptime(params[:shipment][:balance_paid_date],'%d/%m/%Y') ,paid_to_fleet:  params[:balance_paid] )
          else
           @shipment.update_attributes(balance_paid:  params[:balance_paid], balance_paid_date:  nil ,paid_to_fleet:  params[:balance_paid] )
          end
  
        end
        

        if  params[:shipment][:balance_paid_date].present?
          @shipment.update_attributes( balance_paid_date:  Date.strptime(params[:shipment][:balance_paid_date],'%m/%d/%Y') )

        end

      if params["shipment"]["amount"].present?
        @additional_cost = 0 ;
        @additional_cost = @shipment.additional_rate  + @additional_cost if @shipment.additional_rate.present?
        @additional_cost =  @shipment.additional_rate1 + @additional_cost if @shipment.additional_rate1.present?
        @additional_cost =  @shipment.additional_rate2 + @additional_cost if @shipment.additional_rate2.present?
        @additional_cost =  @shipment.additional_rate3 + @additional_cost if @shipment.additional_rate3.present?
        @additional_cost =  @shipment.additional_rate4 + @additional_cost if @shipment.additional_rate4.present?
        @additional_cost =  @shipment.additional_rate5 +  @additional_cost if @shipment.additional_rate5.present?
        @additional_cost =  @shipment.additional_rate6 + @additional_cost if @shipment.additional_rate6.present?
  
        @additional_cost  =   @additional_cost + params["shipment"]["amount"].to_i
        # @shipment.update_attributes(customer_net_amount: @additional_cost)

        
  
  #  
  end
  
      end
         if params["shipment"]["company_ids"].present?
          company_ids= params["shipment"]["company_ids"].reject { |c| c.empty? }
           if company_ids.present?
          params["shipment"]["company_ids"].reject { |c| c.empty? }.each do |id|
            shipment = Shipment.find(params["id"])
          end
          if company_ids.present? and @shipment.state == "posted"
            @shipment.update!(state_event: :accepted) rescue (flash[:error] = @shipment.errors.full_messages)
          end  
          # if company_ids.present? and @shipment.state == "accepted"
          #   @shipment.update_attributes(state_event: "vehicle_assigned")
          # end  
          # if company_ids.present? and @shipment.state == "vehicle_assigned"
          #   @shipment.update_attributes(state_event: "ongoing")
          # end  
             
     end
    end

    # if params["shipment"]["status"].present? and @shipment.state == "vehicle_assigned"             
    #   @shipment.update_attributes(state_event: "ongoing")     
    # end 
    if params["shipment"]["pickup_lat"].present? ||  params["shipment"]["pickup_lng"].present?
    @shipment.populate_category
    end

    if params["shipment"]["category"].present? 
      @shipment.update_attributes(category: params["shipment"]["category"])
      # ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
      end

    

    if params["shipment"]["status"].present?
     

      if params["shipment"]["status"].present? && @shipment.state !=  "ongoing"
        @shipment.update_attributes(state_event: "ongoing")
        @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id , status: 1).all
        @shipment_vehicles.each do |shipment_vehicle|
          shipment_vehicle.update(status: 2)
        
        end
        end

      if params["shipment"]["status"] == "Start"
        @shipment.update_attributes(state_event: "ongoing")
        @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id , status: 1).all
        @shipment_vehicles.each do |shipment_vehicle|
          shipment_vehicle.update(status: 2)
        
        end
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
      end
       
      if params["shipment"]["status"] == "At Loading"
        @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id ).all
        @shipment_vehicles.each do |shipment_vehicle|
          shipment_vehicle.update(status: 3)
        end
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
      end
       
        if params["shipment"]["status"] == "Enroute to Destination"
          @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id).all
          @shipment_vehicles.each do |shipment_vehicle|
            shipment_vehicle.update(status: 4)
          end
         
        
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").blank?

        if @shipment.category == "Cross_Border"
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Origin Border").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Transit Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Transit Border").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Cleared From Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state:"Cleared From Border").blank?

        end
      end

      if params["shipment"]["status"] == "Enroute to Origin Border"
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").blank?
      end
      if params["shipment"]["status"] == "At Origin Border"
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
      end
      if params["shipment"]["status"] == "At Transit Border" || params["shipment"]["status"] == "At Destination Border"|| params["shipment"]["status"] == "Cleared From Border"
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Origin Border").blank?
        ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
      end


  
     






        if params["shipment"]["status"] == "At Offloading Point"
       
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Destination", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").blank?
  
          @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id).all
          @shipment_vehicles.each do |shipment_vehicle|
            shipment_vehicle.update(status: 5)
          end
         

          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.drop_lat ,lng:@shipment.drop_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
          if @shipment.category == "Cross_Border"
            ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").blank?
            ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Origin Border").blank?
            ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Transit Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Transit Border").blank?
            ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Cleared From Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state:"Cleared From Border").blank?
  
          end
        end

        
        if params["shipment"]["status"] == "At Destination Border" || params["shipment"]["status"] == "Break Down"  || params["shipment"]["status"] == "Enroute to Destination" || params["shipment"]["status"] == "Cleared From Border" 

          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.drop_lat ,lng:@shipment.drop_lng) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
          


        end





      if params["shipment"]["status"] == "completed"
        if @shipment.state != "completed"
          @shipment.update_attributes(state_event: "completed")
          
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Destination", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.pickup_lat ,lng:@shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Offloading Point", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: @shipment.drop_lat ,lng:@shipment.drop_lng ) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Offloading Point").blank?
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "completed", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"),lat: @shipment.drop_lat ,lng:@shipment.drop_lng)  

          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "complete", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"),lat: @shipment.drop_lat ,lng:@shipment.drop_lng)  
        
          end
          @shipment_vehicles = ShipmentVehicle.where(shipment_id: @shipment.id).all
          @shipment_vehicles.each do |shipment_vehicle|
            shipment_vehicle.update(status: 6)
          end
         
        end
    
         
     end   
 
 
        @shipment.recalculate_shipment_payments if params[:shipment][:state_event] == "completed" || params[:shipment][:discount]
        
        @shipment.set_contract_fields

        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").try(:last).update_attributes(performed_at: Date.strptime( params[:shipment][:start].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:start_time],lat: params[:shipment][:start_lat],lng: params[:shipment][:start_lng])                                                               if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").present? &&  params[:shipment][:start].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:loading].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:loading_action_time],lat: params[:shipment][:loading_lat],lng: params[:shipment][:loading_lng] )                                                 if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").present?  &&  params[:shipment][:loading].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:enroute_origin].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:enroute_origin_time],lat: params[:shipment][:enroute_origin_lat],lng: params[:shipment][:enroute_origin_lng] )                            if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").present?  &&  params[:shipment][:enroute_origin].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Origin Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_origin].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_origin_time],lat: params[:shipment][:at_origin_lat],lng: params[:shipment][:at_origin_lng] )                                              if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Origin Border").present?  &&  params[:shipment][:at_origin].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Transit Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_transit].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_transit_time] ,lat: params[:shipment][:at_transit_lat],lng: params[:shipment][:at_transit_lng])                                           if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Transit Border").present?  &&  params[:shipment][:at_transit].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Destination Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_destination].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_destination_time],lat: params[:shipment][:at_destination_lat],lng: params[:shipment][:at_destination_lng] )                               if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Destination Border").present? &&  params[:shipment][:at_destination].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Cleared From Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:cleared_border].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:cleared_border_time],lat: params[:shipment][:cleared_border_lat],lng: params[:shipment][:cleared_border_lng] )                                 if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Cleared From Border").present? &&  params[:shipment][:cleared_border].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:enroute_dest].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:enroute_dest_time],lat: params[:shipment][:enroute_dest_lat],lng: params[:shipment][:enroute_dest_lng] )                                  if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").present? &&  params[:shipment][:enroute_dest].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Offloading Point").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_offloading].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_offloading_time],lat: params[:shipment][:at_offloading_lat],lng: params[:shipment][:at_offloading_lng] )                                       if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Offloading Point").present? &&  params[:shipment][:at_offloading].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Break Down").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:breakdown].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:breakdown_time] ,lat: params[:shipment][:breakdown_lat],lng: params[:shipment][:breakdown_lng])                                                   if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Break Down").present? &&  params[:shipment][:breakdown].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "completed").try(:last).update_attributes(performed_at: Date.strptime( params[:shipment][:completed].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:completed_time] ,lat: params[:shipment][:completed_lat],lng: params[:shipment][:completed_lng])                                                   if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "completed").present? &&  params[:shipment][:completed].present?
        redirection_path = params[:query].permit! if params[:query].present? 
        redirection_path = params[:query] if params[:query].blank? 
        

    
        format.html { redirect_to admin_shipments_path( query:redirection_path), notice: 'Shipment was successfully updated.' }
  
      else
        format.html { render :index }
      end
    end
  end

  def show

    # Aws.config.update({
    #   region: "ap-south-1",
    #   credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
    # })
    # dynamodb = Aws::DynamoDB::Client.new
    # table_name = 'vehicle_coordinates'

    # zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")



    # if  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).present?  && @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).present?
    #   start_time = {"date"=>  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  @shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).strftime("%H:%M") }
    #   start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
    #   tz = ISO3166::Country.new(@shipment.country.short_name).timezones.zone_identifiers.first
    #   start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
    #   start_timestamp = start_time_csv.utc
    #   complete_time = {"date"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).strftime("%H:%M") }
    #   complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
    #   complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
    #   complete_timestamp = complete_time_csv.utc
    #   vehicle_id  = @shipment.shipment_fleets.last.company.vehicles.first.try(:id) if @shipment.shipment_fleets.present? && @shipment.shipment_vehicles.blank?
		# 	vehicle_id = @shipment.shipment_vehicles.first.try(:vehicle_id)  if @shipment.shipment_vehicles.present?
			   
    #   params = {

    #     table_name: table_name,
    #     key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
    #     expression_attribute_names: {
    #         "#created" => "created",
    #         "#vehicle_id" => "vehicle_id"
    #     },
    #     expression_attribute_values: {
    #       ":vehicle_id" => vehicle_id,
    #       ":start_time" =>   start_timestamp.to_s,
    #       ":end_time" => complete_timestamp.to_s
    #     }
    #   }
     
    #   if start_timestamp < complete_timestamp
    #     @resp = dynamodb.query(params)
  
    #     end
  
    #   end

  end

  
  def cargo_invoice
        InvoiceMailer.send_cargo_invoice_in_email(:id => params[:format] ).deliver_now 
  end

	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end
	
  private

  def shipment_params
    params.require(:shipment).permit( :cargo_value,:cargo_description_url,:reverse_trip,:balance_paid_date,:category,:invoice_comments,:additional_cost_vat,:accepted_rate_currency,:additional_cost_1_vat,:additional_cost_2_vat,:additional_cost_3_vat,:additional_cost_4_vat,:additional_cost_5_vat,:additional_cost_6_vat,:advance_amount,:advance_paid_date,:advance_paid_to,:VAT_applicable,:final_paid_to,:status,:weight,:vendor,:drop_building_name, :pickup_building_name, :completed_by, :discount, :cancel_reason, :cancel_by,:amount_per_vehicle ,:state_event, :state,:fleet_net_rate, :id,:company_id, :country_id, :pickup_location, :pickup_date, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :payment_option ,:created_at,:fleet_income,:fleet_individual_rate,:lorryz_share_amount,:fleet_rate_currency,:additional_type,:additional_rate,:additional_rate_desc,:additional_type1,:additional_rate1,:additional_rate_desc1,:additional_type2,:additional_rate2,:additional_rate_desc2,:additional_type3,:additional_rate3,:additional_rate_desc3,:additional_type4,:additional_rate4,:additional_rate_desc4,:additional_type5,:additional_rate5,:additional_rate_desc5,:additional_type6,:additional_rate6,:additional_rate_desc6,:additional_cost_ctc,:additional_cost_ctf,:additional_cost1_ctc,:additional_cost1_ctf,:additional_cost2_ctc,:additional_cost2_ctf,:additional_cost3_ctc,:additional_cost3_ctf,:additional_cost4_ctc,:additional_cost4_ctf,:additional_cost5_ctc,:additional_cost5_ctf,:additional_cost6_ctc,:additional_cost6_ctf,:balance_paid,:balance_paid_date,:customer_net_amount,company_ids: [],final_voucher: [],documents: [] ,advance_voucher: [])
  end

  def set_shipment
    @shipment = Shipment.find(params[:id])
  end

  def set_shipment_link
    @shipment_link = "active"
  end




  def set_attachment_params

    if params[:shipment][:documents].present?
      begin
        params[:shipment][:documents] = eval(params[:shipment][:documents].first) 
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end  end




    if params[:shipment][:final_voucher].present?

      begin
        params[:shipment][:final_voucher] = eval(params[:shipment][:final_voucher].first) 
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end  end
    
    if params[:shipment][:advance_voucher].present?

      begin
        params[:shipment][:advance_voucher] = eval(params[:shipment][:advance_voucher].first) 
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end  end
    
  end

end


