class Admin::CompanyInformationController < AdminController
    autocomplete :company_information, :name, :extra_data => [:company_id]

end