class Admin::FinancialReportController < AdminController
  before_action :set_fc_link

  def show
 
  end
  def index
    country_id = current_user.country.id
    start_date =  Date.today()
    end_date = Date.today() - 30.day
    params[:query] ||=   {}
  
    if params[:query].present?
      
      country_id =params[:query] [:country_id]  if params[:query] [:country_id].present?
      end_date = params[:query][:from_date] if params[:query] [:from_date].present?
       start_date=params[:query][:to_date] if params[:query] [:to_date].present?
    end
    @country_id=country_id
    @start_date=start_date
    @end_date=end_date
    @cargo =User.cargo_owners.where(country_id: country_id)
    @total_indiduals =ShipmentFleet.filter_by_country(country_id).select('distinct(shipment_fleets.company_id)')
    @total_indi= @total_indiduals.count
    @total_indi_active =ShipmentFleet.filter_by_country(country_id).shipment_latest(start_date,end_date).select('distinct(shipment_fleets.company_id)').count
    @customers =User.shipmnent_cargo_owners(start_date,end_date,country_id).select('distinct(users.company_id)')
    @shipment =Shipment.where('country_id =? and shipments.pickup_date <=? and shipments.pickup_date >=? ',country_id,start_date,end_date)
    @repeated_trip =@shipment.filtered_shipment(start_date,end_date,country_id)
  puts "\n\n\n\t\t\n\n\n\t\t start_date is  #{@start_date} \n\n"


    respond_to do |f|
      f.html
    end
  end


  def set_fc_link
    @fc_link = "active"
  end
end
