class Admin::ShipmentDashboardController < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin 
  before_action :set_company, only: [:mark_verified,:mark_featured,:mark_blacklisted,:mark_safe_for_cash_on_delivery]
  before_action :set_user, only: [:edit_user, :update_user,:show_user]
  
  before_action :set_shipment_posted_link



  layout 'admin'
  def shipment_posted
    @countries = Country.all
    # @gps = GpsVehicle
  
    params[:query] ||=   {}
  
    if params[:query].present?
    @shipments = Shipment.unscoped {  Shipment.search(params[:query]).where.not('state = ? OR state = ? ', "completed",'cancel').paginate(:page=>params[:page],per_page:30).order(pickup_date: :desc)}

    else
    @shipments =Shipment.unscoped {  Shipment.where.not('state = ? OR state = ? ', "completed",'cancel').paginate(:page=>params[:page],per_page:30).order(pickup_date: :desc)}
    end
    @ongoing ='Ongoing shipments : ' + @shipments.where('state =? ', "ongoing").count.to_s
    @posted ='Posted shipments : ' + @shipments.where('state =? ', "posted").count.to_s
    @upcoming= 'Upcoming shipments : ' +@shipments.where('state =? OR state=? ', "accepted",'vehicle_assigned').count.to_s
 
  end

 
  private

  def set_user
    @user = User.find(params[:user_id])
    set_fleet_owners_link if @user.fleet_owner?
    set_cargo_owners_link if @user.cargo_owner?
  end

  def set_shipment_posted_link
    @shipment_dashboard_link = "active"
  end

end
