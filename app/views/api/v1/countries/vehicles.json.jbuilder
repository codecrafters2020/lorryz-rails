json.array! @vehicles do |vehicle|
  json.id vehicle.id
  json.name vehicle.name
  json.image vehicle.image_url.first  if vehicle.image_url.present?
  # if vehicle.avatar.attached?
  #   json.image url_for(vehicle.avatar)
  # end
end
