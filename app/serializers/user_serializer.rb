class UserSerializer < ActiveModel::Serializer
  attributes :id,:rated_count,:notification_count,:auth_token,:information,:process,:first_name,:last_name,:email,:country_id,:mobile,:company_id,:role,:mobile_verified,:vehicle_shipments, :is_max_period_amount_overdue,:average_rating, :credit_days,:is_online


  belongs_to :company, optional: true
  belongs_to :country, optional: true
  has_one :vehicle, optional: true
  # has_many :shipments

  def average_rating
    if object.company
      object.company.average_rating 
    else
      5
    end
  end
  def is_max_period_amount_overdue
    object.company.is_max_period_amount_overdue? if object.company
  end

  def credit_days
    object.company.credit_days if object.company
  end

  def auth_token
  	AuthToken.issue_token({ user_id: object.id }) if object
  end

  def process
  	object.country.process if object.country
  end

  def vehicle_shipments
    object.vehicle.shipment_vehicles if object.vehicle
  end

  def information
    if object && object.company 
      object.company.individual? ? object.company.individual_information : object.company.company_information
    end
  end

  def notification_count
    object.unread_notifications.count
  end

  def rated_count
    object.company ? (object.company.received_ratings.count ? object.company.received_ratings.count : 0) : 0
  end
end

