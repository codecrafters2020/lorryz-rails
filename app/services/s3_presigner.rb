class UploadPresigner
  def self.presign(prefix, filename, limit: limit)

    extname = File.extname(filename)
    filename = "#{SecureRandom.uuid}#{extname}"
    upload_key = Pathname.new(prefix).join(filename).to_s

    creds = Aws::Credentials.new(Rails.application.secrets.aws_access_key_id, Rails.application.secrets.aws_secret_access_key)
    s3 = Aws::S3::Resource.new(region: Rails.application.secrets.aws_region, credentials: creds)
    obj = s3.bucket('lorry-dev').object(upload_key)

    params = { acl: 'public-read' }
    params[:content_length] = limit if limit
    # binding.pry
    {
      presigned_url: obj.presigned_url(:put, params),
      public_url: obj.public_url
    }
  end
end