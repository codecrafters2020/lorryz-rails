class ItemsImport
    include ActiveModel::Model
    require 'roo'
    validates_presence_of :country, :vehicle_type_id
    attr_accessor :file
  
    def initialize(attributes={})
      attributes.each { |name, value| send("#{name}=", value) }
    end
  
    def persisted?
      false
    end
  
    def open_spreadsheet
      case File.extname(file.original_filename)
      when ".csv" then Csv.new(file.path, nil, :ignore)
      when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
  
    def load_imported_items
      spreadsheet = open_spreadsheet
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        country = row["Country"]
        first_name = row["First Name"]
        last_name = row["Last Name"]
        veh_type = row["Vehicle Type"]
       
        
          vehicle_type_id = VehicleType.where(name: veh_type).first.id if VehicleType.where(name: veh_type).first.present?
          country_id = Country.where(name: country).first.id   if Country.where(name: country).first.present?  
        
            initial_mobile_number = row["Mobile No"]
            mobile_no =  row["Mobile No"] 
            
            password ="12345678"
            @admin_fleet = User.new()
            @admin_fleet.role = :fleet_owner
            @admin_fleet.first_name = first_name
            @admin_fleet.last_name = last_name
            @admin_fleet.password = password
            @admin_fleet.mobile = mobile_no
            @admin_fleet.country_id = country_id
            if row["Email"].present?
              @admin_fleet.email = row["Email"]
            else
            end
            if @admin_fleet.save
            @admin_fleet.register_as_individual 
            @admin_fleet.update(mobile_verified: "verified", verification_code: "",terms_accepted: TRUE)
            
            # UserMailer.send_admin_credentials_to_super_admin(current_user.email, @admin_user.id , @password).deliver
            
            @vehicle = @admin_fleet.company.vehicles.create()
            @vehicle.company_id = @admin_fleet.company_id
            @vehicle.registration_number = row["Vehicle Registration No"]
            @vehicle.vehicle_type_id = vehicle_type_id
            @vehicle.user_id = @admin_fleet.id
            if @vehicle.save
            @vehicle
            else
              @admin_fleet.company.destroy
              @admin_fleet.destroy
              @vehicle


            end

      
        else
            @admin_fleet
        end
        


      end
    end
  
    def imported_items
      @imported_items ||= load_imported_items
    end
  
    def save
        
        if imported_items.map(&:valid?).all? 
          imported_items.each(&:save!)
          true
        else
          imported_items.each_with_index do |item, index|
            item.errors.full_messages.each do |msg|
              errors.add :base, "Row #{index + 1}: #{msg}"
            end
          end
          false
        end
      end
    
  end