class AdminNotification < ApplicationRecord
    extend Enumerize

    enumerize :receivers, in: {
        :all => 1,
        :cargo_owner_individuals => 2,
        :cargo_owner => 3,
        :fleet_owner => 4,
       }


end
