class CompanyRating < ApplicationRecord
	audited
	belongs_to :shipment

	belongs_to :giver, foreign_key: :giver_id, class_name: 'Company', optional: true
	belongs_to :receiver, foreign_key: :receiver_id, class_name: 'Company', optional: true
	after_create :send_push_notification

	def fetch_company_owner
		receiver.cargo? ? receiver.cargo_owner : receiver.fleet_owner
	end


	def send_push_notification
		
		User.send_notification_to_receiver(self.shipment,self.fetch_company_owner) if self.fetch_company_owner.present?  
	end

end