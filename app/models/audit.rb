 class Audit < Audited::Audit


 	def action_update?
 		action == 'update'
 	end

 	def action_create?
 		action == 'create'
 	end

 	def action_delete?
 		action == 'delete'
 	end

 	def user_role
 		user.role.humanize rescue ''
 	end

 	def user_name
 		user.first_name.humanize rescue ''
 	end

 	def action_name
		if action == 'update'

			'Updated'
		elsif action == 'create'
			'Created'
		elsif action == 'delete'
			'Deleted'
		end	
 	end

	 def changed_attributes
		

 		if shipment_vehicle?
 			audited_changes["status"] ? update_vehicle_status : audited_changes["status"]
 		elsif bid?
 				audited_changes["status"] ? update_bid_status : audited_changes["status"]
 		end
 		audited_changes
 	end

 	def shipment_vehicle?
 		auditable_type == "ShipmentVehicle"
 	end
	 def shipment?
 		auditable_type == "Shipment"
 	end

 	def user?
 		auditable_type == "User"
 	end

 	def update_vehicle_status
 		audited_changes["status"][1] = ShipmentVehicle.status.find_value(audited_changes["status"][1]) if ShipmentVehicle.status.find_value(audited_changes["status"][1]).present?
 	end

 	def update_bid_status
 		audited_changes["status"].kind_of?(Array) ? toggle_bid_value : audited_changes["status"]
 	end

 	def toggle_bid_value
 		audited_changes["status"][1] = Bid.status.find_value(audited_changes["status"][1])
 	end

 	def bid?
 		auditable_type == "Bid"
 	end

end 