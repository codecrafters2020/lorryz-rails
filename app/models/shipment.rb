class Shipment < ApplicationRecord
	audited
	paginates_per 5
	after_validation :update_errors
	extend Enumerize
	attr_accessor :completed_by
	has_many :bids
	has_many :change_destination_requests, dependent: :destroy
	has_many :shipment_vehicles
	has_many :shipment_fleets
	has_many :shipment_action_dates
	has_many :vehicles, :through => :shipment_vehicles
	has_many :companies, :through => :shipment_fleets

	has_many :drivers, through: :vehicles
	belongs_to :location, optional: true
	belongs_to :vehicle_type , optional: true
	belongs_to :country, optional: true
	belongs_to :company, optional: true
	belongs_to :fleet, foreign_key: :fleet_id, class_name: 'Company', optional: true
	scope :filter_country, -> (country_id) { where(:country_id => country_id) }
	scope :fleet_active, -> (company_id) {includes(:bids).where(bids: {company_id: company_id,status: [:open]}, state: "posted")}
    scope :fleet_individual_shipments, ->(fleet_id){joins("INNER JOIN shipment_fleets on shipments.id = shipment_fleets.shipment_id").merge(ShipmentFleet.fleet_individual_shipments(fleet_id))}
	scope :join_users, -> { joins("LEFT JOIN users on users.company_id = companies.id") }
	scope :filter_by_user, -> (user_id) { where('shipments.company_id =?',user_id) }
	scope :filter_by_fleet, -> (fleet_id) { where('shipments.fleet_id =?',fleet_id)}
	scope :filter_by_country_id, -> (country_id) { where('shipments.country_id =?',country_id)}
	scope :filter_by_state, -> (state) { where('shipments.state =?',state)}
	scope :filter_by_pickup_date, -> (date) { where('shipments.pickup_date >=?',date)}
	scope :filter_by_drop_date, -> (date) { where('shipments.pickup_date <= ?',date)}
	scope :filter_by_pick_drop_date, -> (startdate,enddate) { where('shipments.pickup_date >=? and shipments.pickup_date <=? ',startdate,enddate)}
    scope :filter_by_truck, ->(truck_no){joins("INNER JOIN shipment_vehicles on shipments.id = shipment_vehicles.shipment_id").where("shipment_vehicles.vehicle_id =?",truck_no)}
	
	scope :filter_by_truck_assigned, ->(){joins("INNER JOIN shipment_fleets on shipments.id = shipment_fleets.shipment_id")}

	scope :filter_by_truck_not_assigned, ->(){	left_outer_joins(:shipment_fleets).where( shipment_fleets: { id: nil } )}

	left_outer_joins(:shipment_fleets).where( shipment_fleets: { id: nil } )

	scope :shipment_ongoing, ->{where(shipments: { state: ["completed","cancel","vehicle_assigned","accepted","ongoing"]})}
	scope :shipment_post, ->{where(shipments: { state: ["posted","initial"]})}

	scope :filter_by_shipment_no, -> (shipment_no) { where('shipments.id =?',shipment_no)}
	scope :is_active, ->{where(is_expired: false)}
	scope :filter_active_shipment, -> (date) { where('shipments.created_at  >=?',date)}
	scope :filter_inactive_shipment, -> (date) { where('shipments.created_at  <?',date)}

	scope :shipment_active, ->{where(shipments: { state: ["vehicle_assigned","accepted","ongoing"]})}
	scope :shipment_active_dashboard, ->{where(shipments: { state: ["vehicle_assigned","ongoing"]})}
	scope :shipment_latest, ->(date) { where('shipments.pickup_date >= ?',date)}
	scope :shipment_latest_filtered, ->(date) { where('pickup_date >= ?',date)}
	scope :shipment_active_dashboard_filter, ->{where(shipments: { state: ["accepted","posted","vehicle_assigned","ongoing"]})}

	scope :shipment_inactive, ->{where(shipments: { state: ["completed","cancel"]})}

	scope :customer_shipments, -> (customer_id) { where(company_id:customer_id, paid_by_cargo: false ,state: ["completed","cancel"] )}

	scope :cargo_company, -> { where("companies.category=?",Company.category.find_value(:cargo).value.to_s) }
	scope :fleet_company, -> { where("companies.category=?",Company.category.find_value(:fleet).value.to_s) }
	scope :cargo_pending_payments, -> (user) { where(state: "completed", company_id: user.company_id, paid_by_cargo: false).sum(:customer_net_amount)}
	scope :cargo_pending_payments_ageing, -> (user,ed,sd) { where(state: "completed", company_id: user.company_id, paid_by_cargo: false).where('shipments.invoice_date <? and shipments.invoice_date >?',ed,sd).sum(:customer_net_amount) }

    scope :filter_by_active_shipments_users, -> (company_id,date) { joins("INNER JOIN users on users.company_id = shipments.company_id").where("users.company_id =? and shipments.created_at >? ",company_id,date)}

	scope :country_pending_payments, -> (country) { where(state: ["completed","cancel"], country_id: country, paid_by_cargo: false).sum(:customer_net_amount)}
	scope :country_pending_payments_ageing, -> (country,ed,sd) { where(state: ["completed","cancel"], country_id: country, paid_by_cargo: false).where('shipments.invoice_date <? and shipments.invoice_date >?',ed,sd).sum(:customer_net_amount) }
	scope :total_payments, ->  { where(state: ["completed"]).sum(:customer_net_amount)}
	scope :total_payments_fleet, ->  { where(state: ["completed"]).sum(:fleet_net_rate)}
	scope :cargo_payments, -> (user) { where(state: "completed", company_id: user.company_id).sum(:customer_net_amount)}
	scope :fleet_payments, -> (user) { where(state: "completed", company_id: user.company_id).sum(:fleet_net_rate)}
	scope :sum_no_of_vehicles, ->  {sum(:no_of_vehicles)}
	scope :fleet_active_shipments, -> (company_id) { where( state: ["ongoing"], fleet_id: company_id)}


    scope :filtered_shipment, ->(start_date,end_date,country_id){joins("INNER JOIN shipment_fleets on shipments.id = shipment_fleets.shipment_id").merge(ShipmentFleet.where(' shipments.pickup_date <=? and shipments.pickup_date >=? and shipments.country_id =? and shipment_fleets.company_id IN (SELECT company_id FROM shipment_fleets GROUP BY shipment_fleets.company_id HAVING COUNT(company_id) > 1)',start_date,end_date,country_id))}


	scope :fleet_pending_payments, -> (user) {where("state = ? AND fleet_id = ? AND (paid_to_fleet = ? OR lorryz_comission_received =?)","completed", user.company_id, false, false).sum(:amount) }
	 after_initialize :set_initial_status
	has_many :company_ratings

	default_scope { order(updated_at: :desc,created_at: :desc) }
	before_save :create_process
	after_commit :create_distance
	before_save :check_if_contratual

	acts_as_paranoid


	attr_accessor :fleet_charges_1
	attr_accessor :fleet_charges_2
	attr_accessor :fleet_charges_3
	attr_accessor :fleet_charges_4
	attr_accessor :fleet_charges_5
	attr_accessor :fleet_charges_6
	attr_accessor :fleet_charges_7
	attr_accessor :fleet_charges_8
	attr_accessor :fleet_charges_9
	attr_accessor :fleet_charges_10
	attr_accessor :fleet_charges_11
	attr_accessor :fleet_charges_12
	attr_accessor :fleet_charges_13
	attr_accessor :fleet_charges_14
	attr_accessor :fleet_charges_15
	attr_accessor :fleet_charges_16
	attr_accessor :fleet_charges_17
	attr_accessor :fleet_charges_18
	attr_accessor :fleet_charges_19
	attr_accessor :fleet_charges_20
	attr_accessor :fleet_charges_21
	attr_accessor :fleet_charges_22
	attr_accessor :fleet_charges_23
	attr_accessor :fleet_charges_24
	attr_accessor :fleet_charges_25
	

	attr_accessor :payable
	attr_accessor :start
	attr_accessor :loading
	attr_accessor :enroute_origin
	attr_accessor :at_origin
	attr_accessor :at_transit
	attr_accessor :at_destination
	attr_accessor :cleared_border
	attr_accessor :enroute_dest
	attr_accessor :at_offloading
	attr_accessor :breakdown
	attr_accessor :completed
	attr_accessor :start_time
	attr_accessor :loading_action_time
	attr_accessor :enroute_origin_time
	attr_accessor :at_origin_time
	attr_accessor :at_transit_time
	attr_accessor :at_destination_time
	attr_accessor :cleared_border_time
	attr_accessor :enroute_dest_time
	attr_accessor :at_offloading_time
	attr_accessor :breakdown_time
	attr_accessor :completed_time


	attr_accessor :start_lat
	attr_accessor :loading_lat
	attr_accessor :enroute_origin_lat
	attr_accessor :at_origin_lat
	attr_accessor :at_transit_lat
	attr_accessor :at_destination_lat
	attr_accessor :cleared_border_lat
	attr_accessor :enroute_dest_lat
	attr_accessor :at_offloading_lat
	attr_accessor :breakdown_lat
	attr_accessor :completed_lat
	attr_accessor :start_lng
	attr_accessor :loading_lng
	attr_accessor :enroute_origin_lng
	attr_accessor :at_origin_lng
	attr_accessor :at_transit_lng
	attr_accessor :at_destination_lng
	attr_accessor :cleared_border_lng
	attr_accessor :enroute_dest_lng
	attr_accessor :at_offloading_lng
	attr_accessor :breakdown_lng
	attr_accessor :completed_lng
	





	NO_OF_VEHICLES = (1..25).to_a

	CARGO_PACKING_TYPES =  ["Palletized","Containerized","Sacks","Boxes","Drums","Loose Cargo","Other"]

	FLEET_CANCEL_REASON = ["My customer canceled the shipment" , "No Bids received for this shipment", "Bids Received are expensive than market rates", "Other reason"]

	validates_presence_of   :unloading_time, :loading_time, :pickup_date, :pickup_location,:pickup_time,:vehicle_type_id, :no_of_vehicles,:cargo_description,:cargo_packing_type,:payment_option

	state_machine :state, initial: :posted do
		event :posted do
			transition [:initial, :accepted, :vehicle_assigned] => :posted
		end

		event :accepted do
			transition [:posted,:vehicle_assigned] => :accepted
		end

		event :vehicle_assigned do
			transition :accepted => :vehicle_assigned
		end

		event :ongoing do
			transition [:accepted,:vehicle_assigned,:posted] => :ongoing
		end

		event :completed do
			transition [:ongoing, :vehicle_assigned,:accepted] => :completed
		end

		event :cancel do
			transition [:accepted, :posted, :vehicle_assigned,:ongoing] => :cancel
		end

		state :posted do
			validate :validate_pickup_date_time, on: :create
		end

		state :accepted do

		end

		state :vehicle_assigned do
			validate { |shipment| (self.vehicle_ids.count != self.no_of_vehicles and self.vehicle_ids.count != 0) && self.errors[:vehicles] << "must be equal to #{self.no_of_vehicles}" }
		end

		state :initial
		state :vehicle_assigned
		state :ongoing
		state :completed
		state :cancel
		state :completed

		after_transition [:initial] => :posted do |shipment, transition|
			shipment.create_action_date("posted")
			shipment.populate_category
			if !shipment.is_createdby_admin 
			shipment.shipment_posted_notifications
			shipment.shipment_posted_notifications_moderator
			end

			


		end

		after_transition [:posted] => :accepted do |shipment, transition|
			NotInterestedShipment.where(shipment_id: shipment.id).delete_all
			shipment.create_action_date("accepted")
			shipment.update_shipment_payments
		end

		after_transition [:accepted] => :vehicle_assigned do |shipment, transition|
			shipment.create_action_date("vehicle_assigned")
			shipment.shipment_vehicles.update_all(status: "booked") 
		if  shipment.fleet.present?
			if shipment.fleet.company_type == "company"
				shipment.vehicles.each do |vehicle|
					vehicle.driver.send_assigned_vehicle_notification(shipment,vehicle)
				end
			end
		end	
		end

		after_transition [:initial,:accepted, :posted, :vehicle_assigned] => :ongoing do |shipment, transition|
			shipment.create_action_date("ongoing")
			shipment.generate_tacking_code
			shipment.create_vehicle_action_date

		end
		
		before_transition [:accepted, :posted, :vehicle_assigned] => :cancel do |shipment, transition|
			shipment.create_action_date("cancel")
			shipment.calculate_cancel_penalty
			# shipment.update(invoice_date: Date.today)
			shipment.calculate_cancel_amount_tax
			shipment.cancel_shipment_notifications
		end

		before_transition accepted: :posted do |shipment, transition|
			# shipment.create_action_date("fleet_posted")
			shipment.calculate_cancel_penalty
			shipment.calculate_cancel_amount_tax
			shipment.reverse_bids
			shipment.cancel_shipment_notifications
		end

		before_transition vehicle_assigned: :posted do |shipment, transition|
			shipment.calculate_cancel_penalty
			shipment.calculate_cancel_amount_tax
			shipment.reverse_bids
			shipment.cancel_shipment_notifications
		end

		#Trello Task Assigned by Sanjeev on 3/11/2021
		after_transition [:ongoing, :vehicle_assigned,:accepted] => :completed do |shipment|
			
			shipment.update_attributes(
				status: "completed", paid_to_fleet: true, paid_by_cargo: true, lorryz_comission_received: true
		)
		ShipmentVehicle.where(shipment_id: shipment.id ).update_all(status: 6 )
		ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: shipment.id , state: "Start").blank?
		ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: shipment.id , state: "At Loading").blank?
		ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "Enroute to Destination", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: shipment.id , state: "Enroute to Destination").blank?
		ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "At Offloading Point", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.drop_lat ,lng:shipment.drop_lng ) if  ShipmentActionDate.where(shipment_id: shipment.id , state: "At Offloading Point").blank?
		ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "completed", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now.utc).strftime("%I:%M %p"),lat: shipment.drop_lat ,lng:shipment.drop_lng)  
		ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "complete", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now.utc).strftime("%I:%M %p"),lat: shipment.drop_lat ,lng:shipment.drop_lng)  
	    if shipment.category == "Cross_Border"
            ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "Enroute to Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: shipment.id , state: "Enroute to Origin Border").blank?
            ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "At Origin Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng ) if  ShipmentActionDate.where(shipment_id: shipment.id , state: "At Origin Border").blank?
            ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "At Transit Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: shipment.id , state: "At Transit Border").blank?
            ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "Cleared From Border", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng) if  ShipmentActionDate.where(shipment_id: shipment.id , state:"Cleared From Border").blank?
  
          end


		   
		Aws.config.update({
			region: "ap-south-1",
			credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
		  })
		  dynamodb = Aws::DynamoDB::Client.new
		  table_name = 'vehicle_coordinates'
	  
		  zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")
	  
	  
	  
		  if  shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).present? && shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).present?  && shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).present? && shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).present?
			start_time = {"date"=>  shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  shipment.shipment_action_dates.where(state: "At Loading").try(:last).try(:performed_at_time).strftime("%H:%M") }
			start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
			tz = ISO3166::Country.new(shipment.country.short_name).timezones.zone_identifiers.first
			start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
			start_timestamp = start_time_csv.utc
			complete_time = {"date"=>  shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).strftime("%H:%M") }
			complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
			complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
			complete_timestamp = complete_time_csv.utc
			vehicle_id  = shipment.shipment_fleets.last.company.vehicles.first.try(:id) if shipment.shipment_fleets.present? && shipment.shipment_vehicles.blank?
				  vehicle_id = shipment.shipment_vehicles.first.try(:vehicle_id)  if shipment.shipment_vehicles.present?
					 
			params1 = {
	  
			  table_name: table_name,
			  key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
			  expression_attribute_names: {
				  "#created" => "created",
				  "#vehicle_id" => "vehicle_id"
			  },
			  expression_attribute_values: {
				":vehicle_id" => vehicle_id,
				":start_time" =>   start_timestamp.to_s,
				":end_time" => complete_timestamp.to_s
			  }
			}
			if start_timestamp < complete_timestamp
			 @resp = dynamodb.query(params1) 
			end
				

			if @resp.present?
			if @resp.items.present?
			  
				str = ""
				coords1 =[]
				snappedCoordinates=[]
				position=[]
					  @distance = 0
					  i =0 ;    j =100;
					  @resp.items.each do |user|

						@distance = @distance + Geocoder::Calculations.distance_between([@temp_lat , @temp_lng],[user["lat"],user["lng"]],:units =>:km)  if   @temp_lat.present?
						@temp_lat = user["lat"]
						@temp_lng= user["lng"]

							if i < j+100 || i != @resp.items.count
								str = str + user["lat"].to_s + "," + user["lng"].to_s 
									  if i != j-1  && i != @resp.items.count-1
										  str = str+"|"
									   end
								 i +=1 
							  end
							if i == j  || i== @resp.items.count
								url ="https://roads.googleapis.com/v1/snapToRoads?path=#{str}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"            
					             response = HTTParty.get(url)
	   
									if response["snappedPoints"].present?
										response["snappedPoints"].each do |snappoint|
											 coords1.push({lat:  snappoint['location']['latitude']  , lng:  snappoint['location']['longitude']  });
												end
		
									  end
								 str=""
								 j= i + 100
							 end
			  end
			  
			  distance_between_snappedpoints = coords1.count / 25
			  distance_between_snappedpoints = distance_between_snappedpoints.to_i
	   
			  itr = 0 
			  i =0 ;
			  while itr < 25  do
			   
				if coords1[i].present?
			   position.push({ lat:coords1[i][:lat]  , lng:  coords1[i][:lng] })
				end
			   i = i+distance_between_snappedpoints
					itr +=1
	   
			   end
			
			
			 	
				shipment.update_attributes(
					status: "completed",
					#  distance_between_endpoints:@distance,
					routes:position)

			
			
			end
		end
	  



	end


		
			shipment.recalculate_shipment_payments
			shipment.update_shipment_vehicle_status if shipment.completed_by == "admin"
			shipment.rate_shipment_notifications
			shipment.invoice_to_fleet
		end
	end
	STATES = %w(posted accepted vehicle_assigned start loading en_route unloading completed cancel ongoing)

	def set_initial_status 

		self.state ||= :initial 	
	end

	def display_date
		return self.invoice_date if ["completed", "cancel"].include?  self.state
		# return self.pickup_date
	end

	def create_vehicle_action_date
		self.shipment_vehicles.each do |sv|
			# ShipmentActionDate.create(shipment_id: self.id, vehicle_id: sv.id,state: "ongoing", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(self.country ,Time.now.utc).strftime("%I:%M %p"),lat: sv.vehicle.latitude ,lng:sv.vehicle.longitude)   if ShipmentActionDate.where(shipment_id: self.id, state: "ongoing",vehicle_id: sv.id).blank?

		end
	end

	def populate_category

		
		lat = self.pickup_lat
		lon =  self.pickup_lng

    	url = "https://maps.googleapis.com/maps/api/geocode/json?units=imperial&latlng=#{lat.to_f},#{lon.to_f}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
    response = HTTParty.get(url)
    if response['results'].present?
    if  response['results'].first['address_components'].present?
    address = response['results'].first['address_components']
    address.each do |addr|

      if addr['types'].first == 'country'
        @pickup_country = addr['long_name']
      end
      if addr['types'].first == 'locality'
        @pickup_city= addr['long_name']



    end


  end

  lat = self.drop_lat
  lon =  self.drop_lng

    url = "https://maps.googleapis.com/maps/api/geocode/json?units=imperial&latlng=#{lat.to_f},#{lon.to_f}&sensor=true&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
  response = HTTParty.get(url)
  if  response['results'].present?
  if  response['results'].first['address_components'].present?
  address = response['results'].first['address_components']
  address.each do |addr|

    if addr['types'].first == 'country'
      @drop_country = addr['long_name']
    end
    if addr['types'].first == 'locality'
      @drop_city= addr['long_name']
    end
    
end

end
 
  if @pickup_city.present? && @drop_city.present?
 

  if @pickup_city == @drop_city
    self.update_attributes(
      category: "Intra_City"
 )


  elsif @drop_country == @pickup_country
    self.update_attributes(
      category: "Inter_City"
 )
elsif @drop_country != @pickup_country
  self.update_attributes(
    category: "Cross_Border"
)
end
else
  self.update_attributes(
    category: "Intra_City"
)
end end
else
	self.update_attributes(
		category: "Intra_City"
	)	
end
end

	end

	def open_bids
		bids.where(status: :open)
	end

	STATES.each do |state|
		scope state, -> {where(state: state).order(updated_at: :desc,created_at: :desc)}
	end

	#it return cancel_penalty if state == cancel and amount otherwise
	def shipment_amount
		return self.cancel_penalty if self.state == "cancel"
		self.amount
	end

	def recalculate_shipment_payments
		# self.update(invoice_date: Date.today)
		if self.completed?
			self.update_attributes(
					detention: recalculate_shipment_detention,
					amount: recalculate_shipment_amount - discount
			)
			update_shipment_payments
		elsif self.cancel?
			calculate_cancel_amount_tax
		end
	end

	def set_contract_fields
		if self.location_id.present?

			location = Location.find(self.location_id)
			self.pickup_location = location.pickup_location
			self.drop_location = location.drop_location
			self.loading_time = location.loading_time
			self.unloading_time= location.unloading_time
			self.vehicle_type_id= location.vehicle_type_id
			self.save

		end
	end

	def recalculate_shipment_detention
		if process == "prorate"
			actual_prorate_waiting_charges
		else
			actual_bidding_waiting_charges
		end
	end

	def recalculate_shipment_amount
		if process == "prorate"
			actual_prorate_amount
		else
			actual_bidding_amount
		end
	end

	def actual_waiting_charges_per_vehicle_for_invoice vehicle_id
		if process == "prorate"
			actual_waiting_charges_per_vehicle vehicle_id
		else
			actual_waiting_charges_per_vehicle_bidding vehicle_id
		end
	end

	def calculate_cancel_amount_tax
		self.update_attributes(
				cancel_penalty: discounted_cancel_penalty,
				amount_tax: tax_cal_cancel_penalty
		)
	end

	def discounted_cancel_penalty
		cancel_penalty - discount rescue 0
	end

	def update_shipment_payments
		self.update_attributes(
				# lorryz_share_amount: lorryz_share_amount_cal,
				# fleet_income: fleet_income_cal,
				amount_tax: tax_cal,
				lorryz_detention_share_amount: lorryz_detention_share_amount_cal,
				fleet_detention_income: fleet_detention_income_cal,
				detention_tax: detention_tax_cal
		)
	end

	def tax_cal_cancel_penalty
		if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
			(country.fleet_owner_income_tax) * (discounted_cancel_penalty / 100)
		elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
			(country.lorryz_share_tax) * (discounted_cancel_penalty / 100)
		else
			0
		end
	end

	def lorryz_share_amount_cal
		tax_invoice_lorryz_share_cal
	end

	def tax_invoice_lorryz_share_cal
		(country.commision * amount)/100
	end

	def non_tax_invoice_lorryz_share_cal
		if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
			((country.commision)/100) * (amount - tax_cal)
		elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
			(((country.commision)/100) * (amount)) - tax_cal
		end
	end

	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end

	def lorryz_share_tax_amount
		if country.lorryz_share_tax and country.lorryz_share_tax > 0
     
			lorryz_share_amount =0 if lorryz_share_amount.blank?
			country.lorryz_share_tax * (lorryz_share_amount) / (100 + country.lorryz_share_tax)
		end
	end

	def lorryz_share_tax_amount_on_cancel_panelty
		if country.lorryz_share_tax and country.lorryz_share_tax > 0
			cancel_penalty =0 if cancel_penalty.blank?
			country.lorryz_share_tax * (cancel_penalty) / (100 + country.lorryz_share_tax)
		end
	end

	def cancel_penalty_after_tax_deduction
		cancel_penalty - lorryz_share_tax_amount_on_cancel_panelty if !cancel_penalty.blank?
	end

	def lorryz_share_after_tax_deduction
		0
		# lorryz_share_amount - lorryz_share_tax_amount
	end

	def fleet_income_cal
		tax_invoice_fleet_income_cal
	end

	def tax_invoice_fleet_income_cal
		amount - tax_invoice_lorryz_share_cal
	end

	def non_tax_invoice_fleet_income_cal
		if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
			((100 - country.commision)/100) * (amount - tax_cal)
		elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
			((100 - country.commision)/100) * (amount)
		end
	end

	def tax_cal
	# 	@additional_cost = 0 ;
    # @additional_cost = self.additional_rate  + @additional_cost if self.additional_cost_vat
    # @additional_cost =self.additional_rate1 + @additional_cost if self.additional_cost_1_vat
    # @additional_cost =  self.additional_rate2 + @additional_cost if self.additional_cost_2_vat
    # @additional_cost =  self.additional_rate3 + @additional_cost if self.additional_cost_3_vat
    # @additional_cost =self.additional_rate4 + @additional_cost if self.additional_cost_4_vat
    # @additional_cost =  self.additional_rate5 +  @additional_cost if self.additional_cost_5_vat
    # @additional_cost = self.additional_rate6 + @additional_cost if self.additional_cost_6_vat
  

		if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0 and self.customer_net_amount.present?
			# (country.fleet_owner_income_tax * amount) / (100 + country.fleet_owner_income_tax)
          (self.customer_net_amount) * (country.fleet_owner_income_tax / 100)
		elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
			(country.lorryz_share_tax * (((country.commision)/100) * (amount))) / (100 + country.lorryz_share_tax)
		elsif country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0 and self.amount.present?
			# (country.fleet_owner_income_tax * amount) / (100 + country.fleet_owner_income_tax)
			(self.amount) * (country.fleet_owner_income_tax / 100)

			
		else
			0
		end
	end

	def detention_tax_cal
		val = 0
		if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
			val = (country.fleet_owner_income_tax * detention) / (100 + country.fleet_owner_income_tax)
		elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
			val = (country.lorryz_share_tax * detention) / (100 + country.lorryz_share_tax)
		end
		val > 0 ? val : 0
	end

	def lorryz_detention_share_amount_cal
		tax_invoice_lorryz_detention_share_amount_cal
	end

	def tax_invoice_lorryz_detention_share_amount_cal
		(country.commision * detention)/100
	end

	def non_tax_invoice_lorryz_detention_share_amount_cal
		val = 0
		if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
			val = ((country.commision)/100) * (detention - detention_tax_cal)
		elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
			val = (((country.commision)/100) * (detention)) - detention_tax_cal
		end
		val > 0 ? val : 0
	end

	def fleet_detention_income_cal
		tax_invoice_fleet_detention_income_cal
	end

	def tax_invoice_fleet_detention_income_cal
		detention - tax_invoice_lorryz_detention_share_amount_cal
	end

	def non_tax_invoice_fleet_detention_income_cal
		val = 0
		if country.fleet_owner_income_tax and country.fleet_owner_income_tax > 0
			val = ((100 - country.commision)/100) * (detention - detention_tax_cal)
		elsif country.lorryz_share_tax and country.lorryz_share_tax > 0
			val = ((100 - country.commision)/100) * (detention)
		end
		val > 0 ? val : 0
	end

	def create_action_date(state)
		ShipmentActionDate.create(shipment_id: self.id, state: state, performed_at: DateTime.now) if ShipmentActionDate.where(shipment_id: self.id, state: state).blank?
	end

	def create_process
		if posted?
			process = self.country.try(:process)
			self.process = process if process
			calculate_prorate_amount if process == "prorate"
		end
	end

	def create_distance
		self.update_column(:distance_between_endpoints, distance_between_start_and_end_point) if posted?
	end

	def check_if_contratual
		if posted? && location && amount != (location.rate * no_of_vehicles)
			amount = location.rate * no_of_vehicles
		end
	end

	def actual_prorate_amount
		total = 0
		self.shipment_vehicles.each do |sv|
			total += actual_prorate_formula_cal(sv.vehicle_id)
		end
		total
	end

	def actual_prorate_formula_cal(vehicle_id)
		vehicle = self.vehicle_type
		distance = distance_between_start_and_end_point

		km_charges = (distance - vehicle.free_kms) * vehicle.rate_per_km
		amount = vehicle.base_rate + (km_charges > 0 ? km_charges : 0) + (actual_waiting_charges_per_vehicle(vehicle_id) > 0 ? actual_waiting_charges_per_vehicle(vehicle_id) : 0)
	end

	def distance_between_start_and_end_point
		# Haversine.distance(pickup_lat.to_f, pickup_lng.to_f, drop_lat.to_f, drop_lng.to_f).to_kilometers.round(2)
		url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{pickup_lat.to_f},#{pickup_lng.to_f}&destinations=#{drop_lat.to_f},#{drop_lng.to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
		response = HTTParty.get(url)
		#logger.info(response)
		distance = response["rows"].first["elements"].first["distance"].blank? ? 0 : (response["rows"].first["elements"].first["distance"]["value"].to_f / 1000).round(2)
	end

	def actual_waiting_charges_per_vehicle(vehicle_id)
		vehicle = self.vehicle_type
		total = ((actual_load_unload_days(vehicle_id) * 24) - vehicle.load_unload_free_hours) * vehicle.waiting_charges
		total > 0 ? total : 0
	end

	def calculate_prorate_amount
		amount = prorate_formula
		self.amount_per_vehicle = amount
		self.amount = amount * no_of_vehicles
		self.detention_per_vehicle = prorate_waiting_charges
		self.detention = prorate_waiting_charges * no_of_vehicles
	end

	def prorate_formula
		vehicle = self.vehicle_type
		distance = Haversine.distance(pickup_lat.to_f, pickup_lng.to_f, drop_lat.to_f, drop_lng.to_f).to_kilometers.round(2)

		km_charges = (distance - vehicle.free_kms) * vehicle.rate_per_km
		amount = vehicle.base_rate + (km_charges > 0 ? km_charges : 0) + (prorate_waiting_charges)
	end

	def prorate_waiting_charges
		vehicle = self.vehicle_type
		waiting_charges = ((loading_time || 0) + (unloading_time || 0) - vehicle.load_unload_free_hours) * vehicle.waiting_charges
		waiting_charges > 0 ? waiting_charges : 0
	end

	def actual_prorate_waiting_charges
		vehicle = self.vehicle_type
		waiting_charges = 0
		self.shipment_vehicles.each do |sv|
			waiting_charges += actual_waiting_charges_per_vehicle(sv.vehicle_id)
		end
		waiting_charges > 0 ? waiting_charges : 0
	end

	def actual_bidding_amount
		total = amount || 0
		self.shipment_vehicles.each do |sv|
			total += actual_waiting_charges_per_vehicle_bidding(sv.vehicle_id)
		end
		total
	end

	def actual_bidding_waiting_charges
		vehicle = self.vehicle_type
		waiting_charges = 0
		self.shipment_vehicles.each do |sv|
			waiting_charges += actual_waiting_charges_per_vehicle_bidding(sv.vehicle_id)
		end
		waiting_charges > 0 ? waiting_charges : 0
	end

	def actual_waiting_charges_per_vehicle_bidding vehicle_id
		total = detention_days(vehicle_id) * detention_per_vehicle
		total > 0 ? total : 0
	end

	def calculate_cancel_penalty
		if cancel_by == "superadmin" || "moderator"

		else
			cancel_by == "cargo_owner" ? cargo_cancel_penalty : fleet_cancel_penalty
		end

	end

	def cargo_cancel_penalty
		if self.posted?
			self.update_attribute(:cancel_penalty, 0)
		elsif self.accepted? || self.vehicle_assigned?
			if country.penalty_free_hours > ( pickup_time - Time.zone.now)/3600
				self.update_attribute(:cancel_penalty, vehicle_type.cargo_penalty_amount * no_of_vehicles)
			else
				self.update_attribute(:cancel_penalty, 0)
			end
		end
	end

	def cargo_cancel_penalty_charges
		vehicle_type.cargo_penalty_amount
	end

	def fleet_cancel_penalty_charges
		if country.fleet_penalty_free_hours && country.fleet_penalty_free_hours > ( pickup_time - Time.zone.now)/3600
			return vehicle_type.fleet_penalty_amount
		else
			return 0
		end
	end

	def cargo_cancel_penalty_charges_within_24hours
		vehicle_type.cargo_penalty_amount * no_of_vehicles
	end

	def fleet_cancel_penalty_charges_within_24hours
		if country.fleet_penalty_free_hours && country.fleet_penalty_free_hours > ( pickup_time - Time.zone.now)/3600
			return vehicle_type.fleet_penalty_amount * no_of_vehicles
		else
			return 0
		end
	end

	def fleet_cancel_penalty
		self.bids.delete_all
		dup_shipment = self.dup

		if country.fleet_penalty_free_hours > ( pickup_time - Time.zone.now)/3600
			dup_shipment.cancel_penalty = vehicle_type.fleet_penalty_amount * no_of_vehicles
		else
			dup_shipment.cancel_penalty = 0
		end

		dup_shipment.company_id = ""
		dup_shipment.state = "cancel"

		# dup_shipment.save(validate: false)
		dup_shipment.create_action_date("cancel")
	end

	def reverse_bids
		accepted = bids.accepted
		rejected = bids.rejected
		accepted.update_all(status: :rejected)
		rejected.update_all(status: :open)
	end

	def self.fleet_posted(company_id,user)
		# binding.pry
		shipments = Shipment.posted.remove_not_interested(user).filter_country(user.country_id).select { |shipment|  shipment.bids.pluck(:company_id).exclude?(company_id) }
	end

	def self.remove_not_interested(user)
		where.not(id: user.not_interested_shipments.pluck(:shipment_id))
	end

	def shipment_posted_notifications_moderator

		
		moderator = User.moderator.where(:country_id => self.country_id)
		moderator.each do |mod|
			if mod.company_id.present?
			Notification.create!(notifiable_id: mod.id,
		  notifiable_type: "User",
		  body: "New shipment posted from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}",
		  user_mentioned_id: mod.id)
			end
		end
		
	end

	def shipment_posted_notifications
		prefix = ""
		if (self.is_pickup_now)
			prefix = "Pickup Now:"
		end
		shipmentCompany = Company.find(self.company_id)
		# if(shipmentCompany.company_type == 'individual')
			users = User.fleet_owners
		# else
		# 	users = User.company_fleet_owners
		# end
		#users = self.is_pickup_now ? User.individual_fleet_owners: User.fleet_owners
		#logger.info("=================================================+++++++++++++++++++++++++++++++")
		

		
		
		
		
		
		
		
		users.each do |user|
			
			if (user.country_id == self.country_id)
				fleet_shipments = self.is_pickup_now ? Shipment.where(state: ["ongoing", "accepted"], fleet_id: user.company_id) : []
				fleet_bid = self.is_pickup_now ? Bid.where(status: [:open], company_id: user.company_id) : []
				shipment_vehicle = VehicleType.find(self.vehicle_type_id)
				condition = (self.is_pickup_now  && fleet_shipments.none? && fleet_bid.none?)
				vehicles = Vehicle.where(company_id:user.company_id)
				hasVehicleCode = false;
				# (shipmentCompany.company_type == 'company')
				if !hasVehicleCode
					vehicles.each do |vehicle|
						vehicleType = VehicleType.find(vehicle.vehicle_type_id)
						if (vehicleType.code == shipment_vehicle.code)
							hasVehicleCode=true
						end
					end
				end
				if (hasVehicleCode && user.os != "ios")
					if (!self.is_pickup_now || condition)
						if(user.device_id)
							if (self.is_pickup_now)
							PushNotification.send_notification(
									user.os,
									user.device_id,
									"",
									self.id,
									"#{prefix}New shipment posted from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}",
									"filhall_violin",
									)
							else
								PushNotification.send_notification(
									user.os,
									user.device_id,
									"",
									self.id,
									"#{prefix}New shipment posted from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}",
									nil,
									)
								end			
								
						end
						Notification.create(notifiable_id: self.id,
																notifiable_type: "Shipment",
																body: "#{prefix}New shipment posted from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}",
																user_mentioned_id: user.id)
						ShortListedBidder.create(fleet_owner_id: user.company_id,
																		 company_id: user.company_id,
																		 shipment_id: self.id)
					end
				end
			end
		end
	end


	def shipment_updated_before_accept_notifications
		prefix = ""
		if (self.is_pickup_now)
			prefix = "Pickup Now: "
		end
		bidders = ShortListedBidder.where(shipment_id: self.id)
		user_ids = bidders.map{|b| b.fleet_owner_id}

		users = User.where(id: user_ids)
		users = users.find_all { |user| user.country_id == self.country_id }
		users.each do |user|
			fleet_shipments = self.is_pickup_now ? Shipment.where(state: ["ongoing", "accepted"], fleet_id: user.company_id) : []

			fleet_bid = self.is_pickup_now ? Bid.where(status: [:open], company_id: user.company_id) : []

			condition = (self.is_pickup_now && user.is_online && fleet_shipments.none? && fleet_bid.none?)

			if (!self.is_pickup_now || condition)
				if(user.device_id)
					PushNotification.send_notification(
							user.os,
							user.device_id,
							"",
							self.id,
							"#{prefix}Shipment updated: from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}",
							"filhall_violin",
							)
				end
				Notification.create(notifiable_id: self.id,
														notifiable_type: "Shipment",
														body: "#{prefix}Shipment updated: from #{pickup_full_address} to #{drop_full_address} for #{vehicle_type.name}",
														user_mentioned_id: user.id)
			end
		end
	end

	def destination_change_request_received_notification
		# Cargo Notification
		user = self.company.cargo_owner
		# dispathcer = SendSMS.new
		# message = dispathcer.message(user.mobile,"Admin will contact you shortly against your change in Shipment Destination")
		Notification.create(notifiable_id: self.id,
												notifiable_type: "Shipment",
												body: "Admin will contact you shortly against your change in Shipment Destination",
												user_mentioned_id: user.id)
		PushNotification.send_notification(
				user.os,
				user.device_id,
				"",
				self.id,
				"Admin will contact you shortly against your change in Shipment Destination",
				nil,
				) if user.device_id
	end

	def vehicle_status_change_request(status)
		# Cargo Notification
		user = self.company.cargo_owner
		# dispathcer = SendSMS.new
		# message = dispathcer.message(user.mobile,"Admin will contact you shortly against your change in Shipment Destination")
		Notification.create(notifiable_id: self.id,
												notifiable_type: "Shipment",
												body: "Shipment Vehicle status changed to " + status,
												user_mentioned_id: user.id)
		PushNotification.send_notification(
				user.os,
				user.device_id,
				"",
				self.id,
				"Shipment Vehicle status changed to " + status,
				nil,
				) if user.device_id && user.os != "ios"
	end

	def destination_change_notifications(old_destination)
		# Fleet Notification
		user = self.fleet.fleet_owner
		# dispathcer = SendSMS.new
		# message = dispathcer.message(user.mobile,"The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}")
		Notification.create(
				notifiable_id: self.id,
				notifiable_type: "Shipment",
				body: "The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}",
				user_mentioned_id: user.id)
		PushNotification.send_notification(
				user.os,
				user.device_id,
				"",
				self.id,
				"The destination for shipment # #{ self.id} has been changed from #{old_destination} to #{self.drop_location}",
				nil,
				) if user.device_id

		# Notification To Driver
		vehicles = self.vehicles
		vehicles.each do |vehicle|
			user = vehicle.driver
			dispathcer = SendSMS.new
			message = dispathcer.message(user.mobile,"The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}")
			Notification.create(
					notifiable_id: self.id,
					notifiable_type: "Shipment",
					body: "The destination of shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}",
					user_mentioned_id: user.id)
			PushNotification.send_notification(
					user.os,
					user.device_id,
					"",
					self.id,
					"The destination for shipment # #{ self.id } has been changed from #{old_destination} to #{self.drop_location}",
					nil,
					) if user.device_id
		end
	end

	def notify_fleet_to_assign_vehicle
		user = fleet.fleet_owner
		Notification.create(
				notifiable_id: self.id,
				notifiable_type: "Shipment",
				body: "Reminder! Please assign a vehicle to shipment # #{ self.id }",
				user_mentioned_id: user.id)
		PushNotification.send_notification(
				user.os,
				user.device_id,
				"",
				self.id,
				"Reminder! Please assign a vehicle to shipment # #{ self.id }",
				nil,
				) if user.device_id
	end

	def notify_driver_for_shipment_reminder
		vehicles = self.vehicles
		vehicles.each do |vehicle|
			user = vehicle.driver
			# dispathcer = SendSMS.new
			# message = dispathcer.message(user.mobile,"Reminder! Don't forget to pick shipment # #{ self.id } on #{self.pickup_time}")
			Notification.create(
					notifiable_id: self.id,
					notifiable_type: "Shipment",
					body: "Reminder! Don't forget to pick shipment # #{ self.id } on #{self.pickup_time}",
					user_mentioned_id: user.id)
			PushNotification.send_notification(
					user.os,
					user.device_id,
					"",
					self.id,
					"Reminder! Don't forget to pick shipment # #{ self.id } on #{self.pickup_time}",
					nil,
					) if user.device_id
		end
	end

	def calculate_distance(lat,lng)
		url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{lat.to_f},#{lng.to_f}&destinations=#{drop_lat.to_f},#{drop_lng.to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
		response = HTTParty.get(url)
		distance = response["rows"].first["elements"].first["distance"]["value"].to_f / 1000
	end

	def generate_tacking_code
		self.update_attribute(:tracking_code, SecureRandom.hex)
	end

	def actual_loading_days_cal vehicle_id
		loading = shipment_action_dates.where(state: "loading", vehicle_id: vehicle_id).last
		enroute = shipment_action_dates.where(state: "enroute", vehicle_id: vehicle_id).last
		(enroute.performed_at - loading.performed_at)/86400 rescue 0
	end

	def actual_unloading_days_cal vehicle_id
		unloading = shipment_action_dates.where(state: "unloading", vehicle_id: vehicle_id).last
		finish = shipment_action_dates.where(state: "finish", vehicle_id: vehicle_id).last
		(finish.performed_at - unloading.performed_at)/86400 rescue 0
	end

	def cancel_date
		shipment_action_dates.where(state: "cancel").last.try(:performed_at) rescue 0
	end

	def completed_date
		shipment_action_dates.where(state: "complete").last.try(:performed_at) rescue 0
	end

	def actual_load_unload_days(vehicle_id)
		(actual_loading_days_cal(vehicle_id) + actual_unloading_days_cal(vehicle_id))
	end

	def load_unload_days
		((loading_time || 0) + (unloading_time || 0))/24
	end

	def detention_days vehicle_id
		actual_load_unload_days(vehicle_id) - load_unload_days
	end

	def self.search(query)

		
		return all if query_blank?(query)
		company = nil
		company = CompanyInformation.find_by_id(query["cargo_company_id"]).company_id if query["cargo_company_id"].present?
	
		result = joins(:company).includes(:bids).join_users
		result = result.filter_by_user(company) if query["cargo_company_id"].present?
		result = result.fleet_individual_shipments(User.find_by_id(query["fleet_company_id"]).company_id) if query["fleet_owner"].present?
		result = result.filter_by_country_id(query["country_id"])  if query["country_id"].present?
		result = result.filter_by_state(query["status"])  if query["status"].present?
		result = result.where("(shipments.id::text LIKE ?)", "%#{ query["shipment_no"]}") if query["shipment_no"].present?
		
		
		result = result.filter_by_pickup_date(query["pickup_date"])  if query["pickup_date"].present? &&   query["drop_date"].blank?
		result = result.filter_by_drop_date(query["drop_date"])  if query["drop_date"].present? & query["pickup_date"].blank? 
		result = result.filter_by_pick_drop_date(query["pickup_date"],query["drop_date"])  if query["pickup_date"].present? && query["drop_date"].present?
		result = result.where('lower(invoice_comments) LIKE ? ', "%#{query["reference"].downcase}%") if query["reference"].present?
		result = result.filter_by_truck(query["truck_no"]) if query["truck_no"].present?
		if query["truck_assigned"].present?
		result = result.filter_by_truck_assigned() if query["truck_assigned"] == "Truck Assigned"
		result =result.filter_by_truck_not_assigned if query["truck_assigned"] == "Truck Not Assigned"
		end


		if query["pickup_location"].present? && query["drop_location"].present?
            
			result = result.where('lower(pickup_location) LIKE ? and lower(drop_location) LIKE ? ', "%#{query["pickup_location"].downcase}%", "%#{query["drop_location"].downcase}%")
		  
					end    
		
					if query["pickup_location"].present? && query["drop_location"].blank?
	  
						result = result.where('lower(pickup_location) LIKE ? ', "%#{query["pickup_location"].downcase}%")
							   
						# @shipment_dashboard = @shipment_dashboard.where('pickup_location LIKE ? and drop_location LIKE ? ', "%#{params[:query][:pickup_location]}%", "%#{params[:query][:drop_location]}%")
								   
					  end    
							   
											 
					   if query["pickup_location"].blank? && query["drop_location"].present?
							   
						result = result.where(' lower(drop_location) LIKE ? ', "%#{query["drop_location"].downcase}%")
										 
						   # @shipment_dashboard = @shipment_dashboard.where('pickup_location LIKE ? and drop_location LIKE ? ', "%#{params[:query][:pickup_location]}%", "%#{params[:query][:drop_location]}%")
											 
						   end                  
		 






		result
	end

	def self.query_blank?(query)
		query.values.reject(&:empty?).length == 0
	end

	def self.states
		_states = []
		Shipment.state_machine.states.map(&:name).each do |name|
			k = name.to_s
			v = k.capitalize.humanize
			_states.push([v,k])
		end
		_states
	end

	def if_expire?
		bids.count == 0 && (Date.today - pickup_date).to_i >= 20
	end

	def lorryz_share_calculation_for_given_amount amount
		((country.commision)/100) * (amount)
	end

	def freight_charges
		customer_net_amount.to_f - self.try(:amount_tax).to_f - self.try(:detention).to_f + self.try(:discount).to_f
	end

	def taxable_amount
		freight_charges + self.try(:detention) - self.try(:discount)
	end

	def pickup_full_address
		if pickup_location.include?(pickup_building_name)
			pickup_location
		else
			"#{pickup_building_name} #{pickup_location}"
		end
	end

	def drop_full_address
		if drop_location.include?(drop_building_name)
			drop_location
		else
			"#{drop_building_name} #{drop_location }"
		end
	end

	def due_date
		if completed?
			if payment_option == "credit_period"
				(invoice_date + company.credit_period.days ).strftime("%d-%m-%Y") rescue ""
			else
				(invoice_date + 3.days).strftime("%d-%m-%Y") rescue ""
			end
		elsif cancel?
			(cancel_date + 3.days).strftime("%d-%m-%Y") rescue ""
		end
	end

	def update_shipment_vehicle_status
		shipment_vehicles.each do |sv|
			sv.update_attribute(:status, 6)
		end
	end

	def validate_pickup_date_time
		if !self.is_createdby_admin
		if Date.today == self.pickup_date
			if self.is_pickup_now
				errors.add(:pickup_time, "and date cannot be back dated date and time.") if pickup_time < Time.now.utc
			else
				errors.add(:pickup_time, "and date cannot be back dated date and time. Should be minimum 1 hour after current date and time") if pickup_time < Time.now.utc + 1.hour
			end
		end
	end
	end

	def cancel_shipment_notifications
		ShipmentMailer.shipment_canceled(self.id).deliver
		if cancel_by == "cargo_owner"
			user = fleet.fleet_owner rescue nil
		elsif cancel_by == "fleet_owner"
			user = company.cargo_owner
		end

		Notification.create(
				notifiable_id: self.id,
				notifiable_type: "Shipment",
				body: "Shipment # #{ self.id } has been cancelled",
				user_mentioned_id: user.id) if user
		PushNotification.send_notification(
				user.os,
				user.device_id,
				"",
				self.id,
				"Shipment # #{ self.id } has been cancelled",
				nil,
				) if user && user.device_id
	end

	def invoice_to_fleet
		if !self.is_createdby_admin && fleet.present?
		fleet_owner = fleet.fleet_owner
		InvoiceMailer.send_in_email(fleet_owner, self.id).deliver_now 
		end
	end

	def rate_shipment_notifications
		fleet_owner = fleet.fleet_owner
		cargo_owner = company.cargo_owner

		Notification.create(
				notifiable_id: self.id,
				notifiable_type: "Shipment",
				body: "Shipment no #{ self.id } has been completed. Please rate the Cargo Owner",
				user_mentioned_id: fleet_owner.id) if fleet_owner
		PushNotification.send_notification(
				fleet_owner.os,
				fleet_owner.device_id,
				"",
				self.id,
				"Shipment no #{ self.id } has been completed. Please rate the Cargo Owner",
				nil,
				) if fleet_owner && fleet_owner.device_id

		Notification.create(
				notifiable_id: self.id,
				notifiable_type: "Shipment",
				body: "Shipment no #{ self.id } has been completed. Please rate the Fleet Owner / Driver for the service you have received",
				user_mentioned_id: cargo_owner.id) if cargo_owner
		PushNotification.send_notification(
				cargo_owner.os,
				cargo_owner.device_id,
				"",
				self.id,
				"Shipment no #{ self.id } has been completed. Please rate the Fleet Owner / Driver for the service you have received",
				nil,
				) if cargo_owner && cargo_owner.device_id
	end

	handle_asynchronously :rate_shipment_notifications, :run_at => Proc.new { 2.minutes.from_now }

	def distance_from_vehicle(vehicle_id)
		sv = vehicles.find_by(id: vehicle_id)
		distance = calculate_distance(sv.latitude,sv.longitude) rescue 0
	end

	def all_vehicles_at_destination?
		vehicle_status = shipment_vehicles.pluck(:status).compact

		vehicle_status.count { |e| 'finish'.include? e } == self.no_of_vehicles
	end

	def get_payment_option
		case payment_option
		when "before_pick_up"
			return "Pay at origin during shipment pickup"
		when "bank_remittance"
			return "Bank Remittance"
		when "credit_period"
			return self.company.credit_period.to_i.to_s + " days credit period"
		when "after_delivery"
			return "Pay at destination during shipment delivery"
		else
			return payment_option.humanize
		end
	end

	def update_errors
		self.errors.messages[:pickup_location_name] = self.errors.messages.delete :pickup_building_name if self.errors.messages[:pickup_building_name]
		self.errors.messages[:dropoff_location_name] = self.errors.messages.delete :drop_building_name if self.errors.messages[:drop_building_name]
		self.errors.messages[:number_of_vehicles] = self.errors.messages.delete :no_of_vehicles if self.errors.messages[:no_of_vehicles]
	end

	def payment_method
		if payment_option == "credit_period"
			"Credit #{company.credit_days} Days"
		else
			try(:payment_option).try(:camelcase)
		end
	end
end
