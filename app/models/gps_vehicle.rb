class GpsVehicle < ApplicationRecord
  audited
  validates :imei, presence:true, uniqueness: {case_sensitive: false}
  validates :vehicle_id, allow_nil: true, uniqueness: true, if: -> (x) { x.vehicle_id.present? }

	has_many :vehicles
  has_many :locations, dependent: :destroy
  belongs_to :country, optional: true
	scope :filter_by_country_id, -> (country_id) { where('gps_vehicles.country_id =?',country_id)}
  scope :filter_by_status ,-> {joins("INNER JOIN vehicles on gps_vehicles.vehicle_id =  vehicles.id ").merge(Vehicle.fleet_admin_gps_owners)}
  scope :filter_by_status1 ,-> {joins("INNER JOIN vehicles on gps_vehicles.vehicle_id =  vehicles.id ").merge(Vehicle.fleet_gps_owners)}

  scope :filter_by_status_false, -> (status) { where('gps_vehicles.status != true',)}


  # .merge(ShipmentFleet.shipment_active)

  def display_name
    "#{name.try(:capitalize)} "
  end
  
	def self.search(query)
		return all if query_blank?(query)
    # result =GpsVehicle.all

    if  query["status"].present?
      if  query["status"] == "Busy"
        query["status"] = true
      elsif query["status"] == "Free"
        query["status"] = false
      end 

   end
   result =GpsVehicle.all

    result = result.filter_by_country_id(query["country_id"])  if query["country_id"].present?
    result = result.filter_by_status() +  result.filter_by_status1()  if query["status"]==true
    result = result -  result.filter_by_status() -  result.filter_by_status1()  if query["status"]== false


		result
  end
  def self.query_blank?(query)

		query.values.reject(&:blank?).length == 0
	end
 
end
