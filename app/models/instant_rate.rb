class InstantRate < ApplicationRecord
    audited 
    
    belongs_to :vehicle_type , optional: true
    attr_accessor :dropoff_location
    attr_accessor :pickup_location
end
