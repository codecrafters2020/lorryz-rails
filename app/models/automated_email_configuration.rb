class AutomatedEmailConfiguration < ApplicationRecord
    audited 
    belongs_to :companies , optional: true
    attr_accessor :to_location

    attr_accessor :dropoff_location
    attr_accessor :pickup_location
end
