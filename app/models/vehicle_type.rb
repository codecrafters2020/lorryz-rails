class VehicleType < ApplicationRecord
	audited

	belongs_to :country
	has_many :shipments
	has_one :vehicle
	has_one_attached :avatar

	def formatted_name
		"#{name}  | #{country.try(:name)} "
	  end
end
