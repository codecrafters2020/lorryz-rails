class ShipmentFleet < ApplicationRecord
	audited
	belongs_to :shipment
	belongs_to :company
	validates_presence_of :shipment_id, :company_id
	# scope :shipment_active, ->{where(shipment_fleets: { status: ["booked","start","loading","enroute","unloading"]})}
	scope :shipment_active, ->{joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").merge(Shipment.shipment_active)}
	scope :shipment_inactive, ->{joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").merge(Shipment.shipment_inactive)}
	scope :fleet_owners, ->{joins("INNER JOIN users on users.company_id = shipment_fleets.company_id")}
	scope :shipment_driver, -> (shipment_id,fleet_id) { where(:company_id => fleet_id,:shipment_id => shipment_id) }
	scope :filter_by_country, ->(country_id) { joins("INNER JOIN users on users.company_id = shipment_fleets.company_id ").where( 'users.country_id=?',country_id) }
	scope :shipments_filtered, ->(startdate,enddate){joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").where(' shipments.pickup_date <=? and shipments.pickup_date >=? ',startdate,enddate)}

	scope :individual_shipment_active, ->(shipment_id) { where('shipment_fleets.shipment_id =?',shipment_id) }
	scope :fleet_individual_shipments, -> (fleet_id) { where('shipment_fleets.company_id =?',fleet_id)}
	scope :shipment_latest, ->(startdate,enddate){joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").where(' shipments.pickup_date <=? and shipments.pickup_date >=? and shipment_fleets.company_id IN (SELECT company_id FROM shipment_fleets GROUP BY shipment_fleets.company_id HAVING COUNT(company_id) > 1)',startdate,enddate)}
	

	
	# {joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").merge(Shipment.shipment_latest( Date.today() - 30.day))}

	scope :shipment_active_fleet, -> (fleet_id) {joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").merge(Shipment.shipment_active).where('shipment_fleets.company_id =?',fleet_id)}

	extend Enumerize
	enumerize :status, in: {
							:booked => 1,
							:start => 2,
							:loading => 3,
							:enroute => 4,
							:unloading => 5,
							:finish => 6 }, scope: true, predicates: true

	def status_code
		if status == "booked"
			100
		elsif status == 'start'
			200
		elsif status == 'loading'
			300
		elsif status == 'enroute'
			400
		elsif status == 'unloading'
			500
		elsif status == 'finish'
			600
		end 
	end


end
