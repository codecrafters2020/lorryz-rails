module PushNotification
	def self.send_notification(os, device_token, url, id, message,sound_name)
		if os == "ios"
			APNS.send_notification(device_token, alert: message, badge: 0, sound: 'default', :other => {id: id, path: url, count: 0})
		elsif os == "android"
			if sound_name.blank?
				options = { data: { message: message, path: url, count: 0, id: id, sound: 'default', android_channel_id: 'TestChannel1'}}
			else
				options = { data: { message: message, path: url, count: 0, id: id, sound: sound_name, android_channel_id: 'TestChannel'}}
				#sound = sound_name;
			end
			Fcm.send([device_token], options)
		end
	end
end