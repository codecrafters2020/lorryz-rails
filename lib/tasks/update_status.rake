desc 'Update vehicle cordinates '
task update_coords: :environment do

  @shipments = Shipment.all.where( state: ["accepted", "vehicle_assigned"])
  datetime = DateTime.now()
  @shipments.each do |shipment|

    tz = ISO3166::Country.new(shipment.country.short_name).timezones.zone_identifiers.first
		datetime_zone =  datetime.in_time_zone(tz) rescue  datetime
    
    if  shipment.pickup_date.present? && shipment.pickup_time.present? 
      start_time = {"date"=>  shipment.pickup_date.strftime("%d/%m/%y") , "time"=>  shipment.pickup_time.strftime("%H:%M") }
      start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
    
      start_timestamp = start_time_csv
  
       if start_timestamp <= datetime_zone

        shipment.update_attributes(state_event: "ongoing", status: "Start")
        @shipment_vehicles = ShipmentVehicle.where(shipment_id: shipment.id , status: 1).all
        @shipment_vehicles.each do |iterator|
         iterator.update_attributes(status: 2)
        
        end
           @action_date = ShipmentActionDate.create(shipment_id: shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  shipment.convert_to_shipment_country_timezone(shipment.country ,Time.now().utc).strftime("%I:%M %p"),lat: shipment.pickup_lat ,lng:shipment.pickup_lng ) 
            
       end
    end
  end
end 