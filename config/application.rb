require_relative 'boot'

require 'rails/all'
require 'geokit'

require 'active_storage/engine'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module LorryzBackend
  class Application < Rails::Application
    
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    Rails.application.config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'

        resource '*',
          headers: :any,
          methods: [:get, :post, :put, :patch, :delete, :options, :head]
      end
    end
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
    config.eager_load_paths << Rails.root.join('lib')
    config.active_job.queue_adapter = :delayed_job
  end


end

  

require "#{Rails.root}/lib/carrierwave/base64/adapter.rb"
