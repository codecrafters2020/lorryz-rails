require "application_system_test_case"

class Api::V1::CompaniesTest < ApplicationSystemTestCase
  setup do
    @api_v1_company = api_v1_companies(:one)
  end

  test "visiting the index" do
    visit api_v1_companies_url
    assert_selector "h1", text: "Api/V1/Companies"
  end

  test "creating a Company" do
    visit api_v1_companies_url
    click_on "New Api/V1/Company"

    fill_in "Category", with: @api_v1_company.category
    fill_in "Company Type", with: @api_v1_company.company_type
    fill_in "User", with: @api_v1_company.user_id
    click_on "Create Company"

    assert_text "Company was successfully created"
    click_on "Back"
  end

  test "updating a Company" do
    visit api_v1_companies_url
    click_on "Edit", match: :first

    fill_in "Category", with: @api_v1_company.category
    fill_in "Company Type", with: @api_v1_company.company_type
    fill_in "User", with: @api_v1_company.user_id
    click_on "Update Company"

    assert_text "Company was successfully updated"
    click_on "Back"
  end

  test "destroying a Company" do
    visit api_v1_companies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Company was successfully destroyed"
  end
end
