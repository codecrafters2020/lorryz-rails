require 'test_helper'

class Api::V1::RatingsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get api_v1_ratings_create_url
    assert_response :success
  end

  test "should get show" do
    get api_v1_ratings_show_url
    assert_response :success
  end

end
