class AddMaxAmountAndMaxDaysToCountry < ActiveRecord::Migration[5.2]
  def change
    add_column :countries, :cargo_max_amount, :float
    add_column :countries, :cargo_max_days, :integer
    rename_column :countries , :max_amount , :fleet_max_amount
    rename_column :countries , :max_days , :fleet_max_days
  end
end
