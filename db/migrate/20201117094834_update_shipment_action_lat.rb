class UpdateShipmentActionLat < ActiveRecord::Migration[5.2]
  def change
    add_column :shipment_action_dates, :lat, :string
    add_column :shipment_action_dates, :lng, :string

  end
end
