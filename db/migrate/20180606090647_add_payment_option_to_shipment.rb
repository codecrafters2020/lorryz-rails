class AddPaymentOptionToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :payment_option, :int
  end
end
