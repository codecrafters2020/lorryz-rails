class CreateVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.string :registration_number
      t.string :insurance_number
      t.string :vehicle_type
      t.integer :load_capacity
      t.integer :company_id

      t.timestamps
    end
    add_index :vehicles, :company_id
  end
end
