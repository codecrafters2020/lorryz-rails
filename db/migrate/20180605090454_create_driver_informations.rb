class CreateDriverInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :driver_informations do |t|
      t.string :national_id
      t.date :expirty_date
      t.string :license
      t.date :license_expirty
      t.integer :user_id

      t.timestamps
    end
  end
end
