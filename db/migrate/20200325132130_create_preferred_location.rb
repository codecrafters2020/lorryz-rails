class CreatePreferredLocation < ActiveRecord::Migration[5.2]
  def change
    create_table :preferred_locations do |t|
      t.string  :country_name, null: false
      t.string :city_name, null: false
      t.timestamps

    end
  end
end
