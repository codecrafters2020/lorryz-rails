class ChangeShipmentPaymentOptionType < ActiveRecord::Migration[5.2]
  def change
    change_column :shipments, :payment_option , :string
  end
end
