class AddIspickupNowShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :is_pickup_now, :boolean, default: false
  end
end
