class UpdateIndivualInformation < ActiveRecord::Migration[5.2]
  def change
    add_column :individual_informations, :secondary_mobile_no_1, :string
    add_column :individual_informations, :passport_no, :string
    add_column :individual_informations, :passport_no_expiry, :string
    add_column :individual_informations, :visa_no, :string
    add_column :individual_informations, :visa_no_expiry, :string
    add_column :individual_informations, :license_no, :string
    add_column :individual_informations, :license_no_expiry, :string
    add_column :individual_informations, :nationality, :string
    add_column :individual_informations, :preferred_location, :string


  end
end
