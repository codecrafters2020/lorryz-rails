class EmailSpecificCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :send_automated_emails, :boolean, :default => false

end
end