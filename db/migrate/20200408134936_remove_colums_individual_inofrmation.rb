class RemoveColumsIndividualInofrmation < ActiveRecord::Migration[5.2]
  def change
    remove_column :individual_informations, :industry, :string, default: ""
    remove_column :individual_informations, :trade_license_no, :string, default: ""
    remove_column :individual_informations, :trade_license_expiry, :string, default: ""
    remove_column :individual_informations, :trade_registration_no, :string, default: ""
    remove_column :individual_informations, :trade_registration_expiry, :string, default: ""
    remove_column :individual_informations, :company_name, :string, default: ""
    remove_column :individual_informations, :contact1_first_name, :string, default: ""
    remove_column :individual_informations, :contact1_last_name, :string, default: ""
    remove_column :individual_informations, :contact1_mobile, :string, default: ""
    remove_column :individual_informations, :contact1_email, :string, default: ""
    remove_column :individual_informations, :contact1_designation, :string, default: ""
    remove_column :individual_informations, :contact1_office_no, :string, default: ""
    remove_column :individual_informations, :contact2_first_name, :string, default: ""
    remove_column :individual_informations, :contact2_last_name, :string, default: ""
    remove_column :individual_informations, :contact2_mobile, :string, default: ""
    remove_column :individual_informations, :contact2_email, :string, default: ""
    remove_column :individual_informations, :contact2_designation, :string, default: ""
    remove_column :individual_informations, :contact2_office_no, :string, default: ""
    remove_column :individual_informations, :contact3_first_name, :string, default: ""
    remove_column :individual_informations, :contact3_last_name, :string, default: ""
    remove_column :individual_informations, :contact3_mobile, :string, default: ""
    remove_column :individual_informations, :contact3_email, :string, default: ""
    remove_column :individual_informations, :contact3_designation, :string, default: ""
    remove_column :individual_informations, :contact3_office_no, :string, default: ""

  end
end
