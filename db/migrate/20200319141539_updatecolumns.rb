class Updatecolumns < ActiveRecord::Migration[5.2]
  def change
    remove_column :gps, :status, :integer
    remove_column :gps, :country, :string
    add_column :gps, :status, :Boolean
    add_column :gps, :country_id, :integer

  end
end
