class AddCancelByToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :cancel_by, :string
    add_column :shipments, :cancel_penalty, :decimal
  end
end
