class AddColumnsShipmentsAdvanceCosting < ActiveRecord::Migration[5.2]
  def change
    remove_column :shipments, :advance_paid_to
    remove_column :shipments, :advance_voucher, :string

    add_column :shipment_fleets, :advance_paid_to,:string
    add_column :shipment_fleets, :advance_voucher, :string

  end
end
