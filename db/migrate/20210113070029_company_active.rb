class CompanyActive < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :is_active, :Boolean , default: true

  end
end
