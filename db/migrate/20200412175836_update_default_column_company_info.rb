class UpdateDefaultColumnCompanyInfo < ActiveRecord::Migration[5.2]
  def change
    change_column :company_informations, :company_id, 'integer USING CAST(company_id AS integer)'
  end
end
