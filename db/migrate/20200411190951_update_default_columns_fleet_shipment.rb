class UpdateDefaultColumnsFleetShipment < ActiveRecord::Migration[5.2]
  def change
    change_column_default :shipment_fleets, :status, from: nil, to: "booked"
  end
end
