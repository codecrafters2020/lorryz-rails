class AddLorryzComissionReceivedToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :lorryz_comission_received, :boolean, default: false
  end
end
