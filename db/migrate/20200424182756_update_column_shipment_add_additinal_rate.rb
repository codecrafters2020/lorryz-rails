class UpdateColumnShipmentAddAdditinalRate < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :additional_type, :string , default: ""
    add_column :shipments, :additional_rate1, :integer , default: ""
    add_column :shipments, :additional_rate_desc1, :string, default: ""
    add_column :shipments, :additional_type1, :string , default: ""
    add_column :shipments, :additional_rate2, :integer , default: ""
    add_column :shipments, :additional_rate_desc2, :string, default: ""
    add_column :shipments, :additional_type2, :string , default: ""
    add_column :shipments, :additional_rate3, :integer , default: ""
    add_column :shipments, :additional_rate_desc3, :string, default: ""
    add_column :shipments, :additional_type3, :string , default: ""
    add_column :shipments, :additional_rate4, :integer , default: ""
    add_column :shipments, :additional_rate_desc4, :string, default: ""
    add_column :shipments, :additional_type4, :string , default: ""
    add_column :shipments, :additional_rate5, :integer , default: ""
    add_column :shipments, :additional_rate_desc5, :string, default: ""
    add_column :shipments, :additional_type5, :string , default: ""
    add_column :shipments, :additional_rate6, :integer , default: ""
    add_column :shipments, :additional_rate_desc6, :string, default: ""
    add_column :shipments, :additional_type6, :string , default: ""

  
  end
end
