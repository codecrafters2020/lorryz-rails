class CreateTableFleetShipment < ActiveRecord::Migration[5.2]
  def change
    create_table :shipment_fleets do |t|
          t.integer   :shipment_id, null: true
          t.integer  :fleet_id, null: true
          t.integer :status, null: true
          t.timestamps
        end
      end

    
  end