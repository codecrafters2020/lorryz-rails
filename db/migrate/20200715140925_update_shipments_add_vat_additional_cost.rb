class UpdateShipmentsAddVatAdditionalCost < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :additional_cost_vat, :Boolean
    add_column :shipments, :additional_cost_1_vat, :Boolean
    add_column :shipments, :additional_cost_2_vat, :Boolean
    add_column :shipments, :additional_cost_3_vat, :Boolean
    add_column :shipments, :additional_cost_4_vat, :Boolean
    add_column :shipments, :additional_cost_5_vat, :Boolean
    add_column :shipments, :additional_cost_6_vat, :Boolean


  end
end
