class ModforInstantRates < ActiveRecord::Migration[5.2]
  def change
    rename_column :instant_rates, :from_city, :pickup_city
    rename_column :instant_rates, :from_country, :pickup_country
    add_column :instant_rates, :pickup_location, :string 
  end
end
