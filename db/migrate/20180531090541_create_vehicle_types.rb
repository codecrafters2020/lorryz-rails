class CreateVehicleTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicle_types do |t|
      t.integer :code
      t.text :name
      t.integer :country_id
      t.decimal :fleet_penalty_amount
      t.decimal :cargo_penalty_amount
      t.decimal :base_rate
      t.float :free_kms
      t.decimal :rate_per_km
      t.decimal :waiting_charges
      t.float :load_unload_free_hours

      t.timestamps
    end
  end
end
