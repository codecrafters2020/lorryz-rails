class RemoveCkeditorTable < ActiveRecord::Migration[5.2]
  def down
    drop_table :ckeditor_assets
  end
end
