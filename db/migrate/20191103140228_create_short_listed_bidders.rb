class CreateShortListedBidders < ActiveRecord::Migration[5.2]
  def change
    create_table :short_listed_bidders do |t|
      t.integer :fleet_owner_id
      t.integer :company_id
      t.integer :shipment_id
      t.timestamps
    end
  end
end
