class UpdatePrimaryVehicle < ActiveRecord::Migration[5.2]
  def change
    add_column :primary_vehicles, :image_url, :string, array: true, default: []

  end
end
