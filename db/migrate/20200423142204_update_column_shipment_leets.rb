class UpdateColumnShipmentLeets < ActiveRecord::Migration[5.2]
  def change
    remove_column :shipment_fleets, :status, :integer,  null: true
    add_column :shipment_fleets, :payment_cleared, :Boolean, default: false
    add_column :shipment_fleets, :advance_amount,:numeric, null: true


  end
end
