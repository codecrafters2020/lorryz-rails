class AddIsContractualToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :is_contractual, :boolean, default: false
  end
end
