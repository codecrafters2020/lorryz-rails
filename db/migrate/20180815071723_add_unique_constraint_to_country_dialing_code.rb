class AddUniqueConstraintToCountryDialingCode < ActiveRecord::Migration[5.2]
  def change
    add_index :countries, [:dialing_code], :unique => true
  end
end
