class SecondmodforInstantRates < ActiveRecord::Migration[5.2]
  def change
    change_column :instant_rates, :vehicle_type, 'integer USING CAST(vehicle_type AS integer)'
    rename_column :instant_rates, :vehicle_type, :vehicle_type_id
    remove_column :instant_rates, :pickup_location
  end
end
