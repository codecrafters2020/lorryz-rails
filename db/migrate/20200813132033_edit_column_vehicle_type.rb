class EditColumnVehicleType < ActiveRecord::Migration[5.2]
  def change
    remove_column :vehicle_types ,:image_url
   add_column :vehicle_types, :image_url, :string, array: true, default: []

  end
end
