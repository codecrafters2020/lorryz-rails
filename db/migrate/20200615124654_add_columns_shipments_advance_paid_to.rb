class AddColumnsShipmentsAdvancePaidTo < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :advance_paid_to, :string
    add_column :shipments, :advance_voucher, :string
    add_column :shipments, :final_paid_to, :string
    add_column :shipments, :final_voucher, :string
  end
end
