class AddFleetIdToShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :fleet_id, :integer
  end
end
