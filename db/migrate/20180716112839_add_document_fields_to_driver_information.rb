class AddDocumentFieldsToDriverInformation < ActiveRecord::Migration[5.2]
  def change
  	add_column :driver_informations, :license_documents, :string, array: true, default: []
  	add_column :driver_informations, :emirate_documents, :string, array: true, default: []
  	add_column :driver_informations, :avatar, :json
  	
  end
end
