class AddColumnsShipmentsAdvanceCostingDocumenrs < ActiveRecord::Migration[5.2]
  def change

    remove_column :shipment_fleets, :advance_voucher
    remove_column :shipments, :final_voucher

    add_column :shipment_fleets, :advance_voucher, :string, array: true, default: []
    add_column :shipments, :final_voucher, :string, array: true, default: []

  end
end
