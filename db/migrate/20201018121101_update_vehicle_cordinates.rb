class UpdateVehicleCordinates < ActiveRecord::Migration[5.2]
  def change

    add_column :vehicles, :coordinates_updated_at, :datetime

  end
end
