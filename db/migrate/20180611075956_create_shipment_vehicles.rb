class CreateShipmentVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :shipment_vehicles do |t|
      t.integer :shipment_id
      t.integer :vehicle_id

      t.timestamps
    end
  end
end
