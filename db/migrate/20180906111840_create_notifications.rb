class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.integer :notifiable_id
      t.string :notifiable_type
      t.integer :mentioned_by_id
      t.integer :user_mentioned_id
      t.boolean :viewed_flag, default: false
      t.text :body

      t.timestamps
    end
  end
end
