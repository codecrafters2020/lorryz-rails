class ChangeLatitudeToBeFloatInVehicles < ActiveRecord::Migration[5.2]
  def up
  	execute 'ALTER TABLE vehicles ALTER COLUMN latitude TYPE float USING (latitude::float)'
    execute 'ALTER TABLE vehicles ALTER COLUMN longitude TYPE float USING (longitude::float)'
  end
  def down
  	execute 'ALTER TABLE vehicles ALTER COLUMN latitude TYPE string USING (latitude::string)'
    execute 'ALTER TABLE vehicles ALTER COLUMN longitude TYPE string USING (longitude::string)'
  end
end
