class UpdateShipmentReverse < ActiveRecord::Migration[5.2]
  def change
    change_column :shipments, :reverse_trip, :boolean, :default => false
  end
end
