class UpdateShipmentsActionDatesPerformed < ActiveRecord::Migration[5.2]
  def change
    change_column :shipment_action_dates, :performed_at, :datetime

  end

end
