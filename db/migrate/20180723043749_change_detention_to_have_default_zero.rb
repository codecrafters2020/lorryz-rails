class ChangeDetentionToHaveDefaultZero < ActiveRecord::Migration[5.2]
  def change
    change_column :shipments, :detention, :decimal , default: 0
  end
end
