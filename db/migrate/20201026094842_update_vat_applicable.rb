class UpdateVatApplicable < ActiveRecord::Migration[5.2]
  def change
    change_column_default :shipments, :additional_cost_vat, false
    change_column_default :shipments, :additional_cost_1_vat, false
    change_column_default :shipments, :additional_cost_2_vat, false
    change_column_default :shipments, :additional_cost_3_vat, false
    change_column_default :shipments, :additional_cost_4_vat, false
    change_column_default :shipments, :additional_cost_5_vat, false
    change_column_default :shipments, :additional_cost_6_vat, false

  end
end
