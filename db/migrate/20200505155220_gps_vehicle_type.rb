class GpsVehicleType < ActiveRecord::Migration[5.2]
  def change
    change_column :gps_vehicles, :vehicle_id, 'integer USING CAST(vehicle_id AS integer)'

  end
end
