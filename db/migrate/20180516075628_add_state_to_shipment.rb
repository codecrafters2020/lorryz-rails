class AddStateToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :state, :string
  end
end
