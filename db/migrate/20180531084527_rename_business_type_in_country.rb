class RenameBusinessTypeInCountry < ActiveRecord::Migration[5.2]
  def change
    rename_column :countries, :business_type, :process
  end
end
