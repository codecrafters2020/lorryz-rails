class AddIsCompleteToCompanyInformation < ActiveRecord::Migration[5.2]
  def change
    add_column :company_informations, :is_complete, :boolean, default: false
  end
end
