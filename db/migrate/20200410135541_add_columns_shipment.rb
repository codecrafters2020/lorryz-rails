class AddColumnsShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :additional_rate_currency, :string, default: ""
    add_column :shipments, :additional_rate, :integer , default: ""
    add_column :shipments, :additional_rate_desc, :string, default: ""
    add_column :shipments, :is_createdby_admin, :Boolean, default: false
   

  end
end
