class VehicleTypeClassification < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_types, :domestic, :Boolean , default: true
    add_column :vehicle_types, :intra, :Boolean , default: true
    add_column :vehicle_types, :inter, :Boolean , default: true
    add_column :vehicle_types, :CrossBorder, :Boolean , default: true


  end
end
