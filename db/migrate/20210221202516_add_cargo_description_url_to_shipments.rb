class AddCargoDescriptionUrlToShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :cargo_description_url, :text
  end
end
