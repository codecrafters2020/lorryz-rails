class CreateInstantRates < ActiveRecord::Migration[5.2]
  def change
    create_table :instant_rates do |t|
      t.string   :from_city
      t.string   :from_country
      t.string   :to_city
      t.string   :to_country
      t.string   :vehicle_type
      t.integer   :amount
      t.timestamps
    end
  end
end
