class UpdateColumnFleetssShipments < ActiveRecord::Migration[5.2]
  def change
    change_column :shipment_fleets, :status, :string
  end
end
