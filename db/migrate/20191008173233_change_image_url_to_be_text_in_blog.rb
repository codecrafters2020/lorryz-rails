class ChangeImageUrlToBeTextInBlog < ActiveRecord::Migration[5.2]
  def change
    change_column :blogs, :image_url, :text
  end
end
