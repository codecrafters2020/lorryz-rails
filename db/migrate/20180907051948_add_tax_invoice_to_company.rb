class AddTaxInvoiceToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :tax_invoice, :boolean, default: false
  end
end
