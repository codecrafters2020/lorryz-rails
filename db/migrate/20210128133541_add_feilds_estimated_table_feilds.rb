class AddFeildsEstimatedTableFeilds < ActiveRecord::Migration[5.2]
  def change

    remove_column :shipmentestimations, :reverse_trip
     remove_column :shipmentestimations, :shipment_type
    add_column :shipmentestimations, :verified, :Boolean , default: false
    add_column :shipmentestimations, :shipment_type, :string
  
  end
end
