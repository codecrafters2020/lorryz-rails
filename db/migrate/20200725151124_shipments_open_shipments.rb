class ShipmentsOpenShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :is_fixed, :Boolean
  end
end
