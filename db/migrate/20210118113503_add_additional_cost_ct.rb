class AddAdditionalCostCt < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :additional_cost_ctc, :Boolean , default: true
    add_column :shipments, :additional_cost_ctf, :Boolean , default: true
    add_column :shipments, :additional_cost1_ctc, :Boolean , default: true
    add_column :shipments, :additional_cost1_ctf, :Boolean , default: true
    add_column :shipments, :additional_cost2_ctc, :Boolean , default: true
    add_column :shipments, :additional_cost2_ctf, :Boolean , default: true
    add_column :shipments, :additional_cost3_ctc, :Boolean , default: true
    add_column :shipments, :additional_cost3_ctf, :Boolean , default: true
    add_column :shipments, :additional_cost4_ctc, :Boolean , default: true
    add_column :shipments, :additional_cost4_ctf, :Boolean , default: true
    add_column :shipments, :additional_cost5_ctc, :Boolean , default: true
    add_column :shipments, :additional_cost5_ctf, :Boolean , default: true
    add_column :shipments, :additional_cost6_ctc, :Boolean , default: true
    add_column :shipments, :additional_cost6_ctf, :Boolean , default: true

  end
end
