class AddAmountToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :amount, :decimal
    add_column :shipments, :location_id, :integer
    add_column :shipments, :pickup_city, :string
    add_column :shipments, :drop_city, :string
  end
end
