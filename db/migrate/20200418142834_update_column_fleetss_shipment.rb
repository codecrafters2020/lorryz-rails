class UpdateColumnFleetssShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :vendor, :numeric, null: true
  end
end
