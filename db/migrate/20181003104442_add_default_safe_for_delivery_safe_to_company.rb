class AddDefaultSafeForDeliverySafeToCompany < ActiveRecord::Migration[5.2]
  def change
    change_column :companies, :safe_for_cash_on_delivery, :boolean , default: true
  end
end
