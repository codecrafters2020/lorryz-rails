class AddFieldsToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :vehicle_type_id, :int
    add_column :locations, :loading_time, :float, default: 0
    add_column :locations, :unloading_time, :float, default: 0
  end
end
