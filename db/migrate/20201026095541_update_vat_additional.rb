class UpdateVatAdditional < ActiveRecord::Migration[5.2]
  def change


    remove_column :shipments, :additional_cost_vat
    remove_column :shipments, :additional_cost_1_vat
    remove_column :shipments, :additional_cost_2_vat
    remove_column :shipments, :additional_cost_3_vat
    remove_column :shipments, :additional_cost_4_vat
    remove_column :shipments, :additional_cost_5_vat
    remove_column :shipments, :additional_cost_6_vat



    add_column :shipments, :additional_cost_vat, :Boolean, :default => false
    add_column :shipments, :additional_cost_1_vat, :Boolean,:default =>  false
    add_column :shipments, :additional_cost_2_vat, :Boolean, :default => false
    add_column :shipments, :additional_cost_3_vat, :Boolean,:default =>  false
    add_column :shipments, :additional_cost_4_vat, :Boolean, :default => false
    add_column :shipments, :additional_cost_5_vat, :Boolean, :default => false
    add_column :shipments, :additional_cost_6_vat, :Boolean, :default => false

  end
end
