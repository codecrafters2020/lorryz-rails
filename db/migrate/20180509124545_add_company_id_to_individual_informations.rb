class AddCompanyIdToIndividualInformations < ActiveRecord::Migration[5.2]
  def change
    add_column :individual_informations, :company_id, :integer
    add_index :individual_informations, :company_id
  end
end
