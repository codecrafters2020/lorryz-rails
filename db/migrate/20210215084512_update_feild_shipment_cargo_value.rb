class UpdateFeildShipmentCargoValue < ActiveRecord::Migration[5.2]
  def change
    remove_column :shipments, :cargo_value
    add_column :shipments, :cargo_value ,:integer

  end
end
