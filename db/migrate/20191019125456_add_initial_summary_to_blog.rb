class AddInitialSummaryToBlog < ActiveRecord::Migration[5.2]
  def change
    add_column :blogs, :initial_summary, :text
  end
end
