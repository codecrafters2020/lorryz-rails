class AutomatedEmailConfiguration < ActiveRecord::Migration[5.2]
  def change

    create_table :autoamted_email_configuration do |t|
      t.string   :pickup_city
      t.string   :pickup_country
      t.string   :drop_city
      t.string   :drop_country
      t.string   :email
      t.integer   :company_id
      t.timestamps
    end
  end
end
